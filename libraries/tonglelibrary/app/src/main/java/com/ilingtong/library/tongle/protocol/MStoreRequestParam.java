package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/15
 * Time: 16:53
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class MStoreRequestParam implements Serializable {
    public String user_id;
    public String mstore_id;
    public String forward;
    public String fetch_count;
}
