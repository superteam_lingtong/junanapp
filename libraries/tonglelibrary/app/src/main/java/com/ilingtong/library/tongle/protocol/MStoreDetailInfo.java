package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 14:52
 * Email: jqleng@isoftstone.com
 * Desc:2011接口 魔店基本信息的body
 */
public class MStoreDetailInfo implements Serializable {
    private String mstore_id;
    private String mstore_name;
    private String mstore_favorited_by_me;
    private ArrayList<ADListItem> ad_list;
    private ArrayList<ProductListItemData> prod_list;
    private String vouchers_text;       // 优惠介绍文字  add at 2016/03/29
    private String vouchers_url;       //优惠LinkUrl add at 2016/03/29
    private String vouchers_title;       //优惠券标题  add at 2016/03/29

    public String getMstore_id() {
        return mstore_id;
    }

    public String getMstore_name() {
        return mstore_name;
    }

    public String getMstore_favorited_by_me() {
        return mstore_favorited_by_me;
    }

    public ArrayList<ADListItem> getAd_list() {
        return ad_list;
    }

    public ArrayList<ProductListItemData> getProd_list() {
        return prod_list;
    }

    public void setMstore_id(String mstore_id) {
        this.mstore_id = mstore_id;
    }

    public void setMstore_name(String mstore_name) {
        this.mstore_name = mstore_name;
    }

    public void setMstore_favorited_by_me(String mstore_favorited_by_me) {
        this.mstore_favorited_by_me = mstore_favorited_by_me;
    }

    public void setAd_list(ArrayList<ADListItem> ad_list) {
        this.ad_list = ad_list;
    }

    public void setProd_list(ArrayList<ProductListItemData> prod_list) {
        this.prod_list = prod_list;
    }

    public String getVouchers_text() {
        return vouchers_text;
    }

    public void setVouchers_text(String vouchers_text) {
        this.vouchers_text = vouchers_text;
    }

    public String getVouchers_url() {
        return vouchers_url;
    }

    public void setVouchers_url(String vouchers_url) {
        this.vouchers_url = vouchers_url;
    }

    public String getVouchers_title() {
        return vouchers_title;
    }

    public void setVouchers_title(String vouchers_title) {
        this.vouchers_title = vouchers_title;
    }

    @Override
    public String toString() {
        return "mstore_id:" + mstore_id + "\r\n" +
                "mstore_name:" + mstore_name + "\r\n" +
                "mstore_favorited_by_me:" + mstore_favorited_by_me + "\r\n" +
                "ad_list:" + ad_list + "\r\n" +
                "prod_list:" + prod_list;
    }
}
