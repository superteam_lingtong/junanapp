package com.ilingtong.library.tongle.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.GroupOrderListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.CouponOrderBaseInfo;
import com.ilingtong.library.tongle.protocol.GroupOrderListResult;
import com.ilingtong.library.tongle.protocol.OrderRequestParam;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/15
 * Time: 10:21
 * Email: liuting@ilingtong.com
 * Desc:团购订单列表，分为全部、未消费和退款单
 */
public class GroupOrderListFragment extends LazyFragment implements XListView.IXListViewListener {
    private XListView mLvGroupOrder;//订单列表
    private RelativeLayout mRlyReplace;//无相关信息提示
    private LinearLayout mLlyTop;//顶部view
    private Dialog mDialog;//加载对话框

   // private GroupOrderListResult mGroupOrderListResult;//我的团购订单
    private GroupOrderListAdapter mGroupOrderListAdapter;//我的团购订单列表Adapter
    private List<CouponOrderBaseInfo> mOrderList = new ArrayList<>(); //订单列表
    private boolean isPrepared;// 标志位，标志已经初始化完成。
    private String type;//标志位，区分全部、未消费和退款单
    private OrderRequestParam mParam;//订单请求参数类

    private boolean loadAllFlag = false;  //为true时表示数据已全部加载完毕
   // public static boolean UPDATE_LIST_FLAG = false; //是否刷新列表的标准

    public static final int REQUESTCODE_TO_PAY = 10001;  //去支付requestCode
    public static final int REQUESTCODE_TO_ORDER = 10002;  //去订单详情requestCode
    public static final int REQUESTCODE_TO_EVALUATE = 10003;  //去评价requestCode

    private boolean clearFlag = true;
    public static final int REFRESH_LIST = 0;  //刷新列表
    public static final int NO_DATA_REFRESH = 1;  //无数据时刷新
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_LIST:
                    mRlyReplace.setVisibility(View.GONE);
                    mLvGroupOrder.setRefreshTime();
                    mGroupOrderListAdapter.notifyDataSetChanged();
                    break;
                case NO_DATA_REFRESH:
                    mLvGroupOrder.setVisibility(View.GONE);
                    mRlyReplace.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.xlistview_common_layout, null);
        initView(view);
        type = getArguments().getString("type", "");
        isPrepared = true;
      //  UPDATE_LIST_FLAG = true;
        lazyLoad();
        return view;
    }

    public static GroupOrderListFragment newInstance(String type) {
        Bundle args = new Bundle();
        args.putString("type", type);
        GroupOrderListFragment fragment = new GroupOrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void initView(View view) {
        mLvGroupOrder = (XListView) view.findViewById(R.id.xlistview_common_lv_list);
        mRlyReplace = (RelativeLayout) view.findViewById(R.id.xlistview_common_rly_replace);
        mLlyTop = (LinearLayout) view.findViewById(R.id.xlistview_common_lly_top);
        mLlyTop.setVisibility(View.GONE);

        mDialog = DialogUtils.createLoadingDialog(getActivity());
        mDialog.setCancelable(true);

        //mOrderList = new ArrayList<>();
        mOrderList.clear();
        mLvGroupOrder.setPullLoadEnable(false);
        mLvGroupOrder.setPullRefreshEnable(true);
        mLvGroupOrder.setXListViewListener(this, 0);
        mGroupOrderListAdapter = new GroupOrderListAdapter(getActivity(), mOrderList,GroupOrderListFragment.this);
        mLvGroupOrder.setAdapter(mGroupOrderListAdapter);
    }

    /**
     * 取得数据
     */
    public void getData(String order_no) {
        mDialog.show();
        mParam = new OrderRequestParam();
        mParam.user_id = TongleAppInstance.getInstance().getUserID();//用户ID
        mParam.order_status = type;//订单状态过滤条件
        mParam.order_date_from = "";//时间期间From
        mParam.order_date_to = "";//时间期间To
        mParam.order_no = order_no;//订单号
        mParam.forward = TongleAppConst.FORWORD_DONW;//翻页方向
        mParam.fetch_count = TongleAppConst.FETCH_COUNT;//返回条数
        ServiceManager.doMyGroupOrderListRequest(mParam, getSuccessListener(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mDialog.dismiss();
                ToastUtils.toastLong(R.string.sys_exception);
            }
        });
    }

    /**
     * 接口请求成功回调
     *
     * @return
     */
    private Response.Listener<GroupOrderListResult> getSuccessListener() {
        return new Response.Listener<GroupOrderListResult>() {
            @Override
            public void onResponse(GroupOrderListResult groupOrderListResult) {

                mDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(groupOrderListResult.getHead().getReturn_flag())) {
                    if (groupOrderListResult.getBody().getData_total_count() > 0) {
                        if (clearFlag) {
                            mOrderList.clear();
                        }
                        mOrderList.addAll(groupOrderListResult.getBody().getOrder_list());
                        loadAllFlag = (mOrderList.size() < groupOrderListResult.getBody().getData_total_count()) ? false : true;
                        mLvGroupOrder.setPullLoadEnable(!loadAllFlag);
                        handler.sendEmptyMessage(REFRESH_LIST);
                    } else {
                        handler.sendEmptyMessage(NO_DATA_REFRESH);
                    }
                } else {
                    ToastUtils.toastLong(R.string.para_exception + groupOrderListResult.getHead().getReturn_message());
                }
            }
        };
    }

    @Override
    protected void lazyLoad() {
        if (!isPrepared || !isVisible) {
            return;
        } else {
            //mOrderList.clear();
            clearFlag = true;
            getData("");
        }
    }

    @Override
    public void onRefresh(int id) {
        // mOrderList.clear();
        clearFlag = true;
        getData("");
    }

    @Override
    public void onLoadMore(int id) {
        if (loadAllFlag) {
            ToastUtils.toastShort(R.string.common_list_end);
        } else {
            clearFlag = false;
            getData(mOrderList.get(mOrderList.size() - 1).getOrder_no());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //这里认为只要返回RESULT_OK就刷新列表。不用考虑requestCode。如果刷新逻辑不同，应根据requestCode分别写逻辑。
        if (resultCode == Activity.RESULT_OK){
            onRefresh(0);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
