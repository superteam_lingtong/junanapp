package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/1
 * Time: 9:51
 * Email: jqleng@isoftstone.com
 * Desc: 注册信息类,该包含通过网络返回的数据
 */
public class RegistInfo implements Serializable {
    private String str = "";
    @Override
    public String toString() {
        return str;
    }
}
