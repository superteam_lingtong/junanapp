package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/23
 * Time: 11:23
 * Email: jqleng@isoftstone.com
 * Desc: 服务管家信息类
 */
public class AppServiceInfo implements Serializable {
    private String title_text;
    private String qa_service_url;

    public String getTitle_text() { return title_text; }
    public String getQa_service_url() {
        return qa_service_url;
    }

    @Override
    public String toString() {
        return "title_text:" + title_text + "\r\n" +
                "qa_service_url:" + qa_service_url;
    }
}
