package com.ilingtong.library.tongle.fragment;


import android.support.v4.app.Fragment;

import com.ilingtong.library.tongle.dialog.RequestCallbackDialog;
import com.ilingtong.library.tongle.listener.Task;
import com.ilingtong.library.tongle.utils.TongleUtils;
import com.lidroid.xutils.http.HttpHandler;

    public class FragmentAppBase extends Fragment {
        public HttpHandler<String> httpHandler;
        private RequestCallbackDialog requestCallbackDialog;// 请求显示dialog

         /**
         * 初始化数据增加延迟
         */
        protected void initData(final Runnable runnalbe, boolean isInit) {
            if (isInit) {
                TongleUtils.postDelayed(runnalbe, 1 * 1000);
            }
        }

        public void showRequestCallbackDialog() {
            if (null == requestCallbackDialog) {
                requestCallbackDialog = new RequestCallbackDialog(getActivity(), new Task<String>() {

                    @Override
                    public void run(String t) {
                        cancelRequestCallbackDialog();
                    }
                });
            }
            requestCallbackDialog.setCanceledOnTouchOutside(false);
            requestCallbackDialog.show();
        }

        public void cancelRequestCallbackDialog() {
            if (null != requestCallbackDialog && requestCallbackDialog.isShow()) {
                requestCallbackDialog.dismiss();
                if (null != httpHandler) {
                    httpHandler.cancel();
                }
            }
        }
    }