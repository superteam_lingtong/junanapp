//package com.ilingtong.app.tongle.activity;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.drawable.BitmapDrawable;
//import android.graphics.drawable.Drawable;
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import com.ilingtong.app.tongle.R;
//import com.ilingtong.app.tongle.TongleAppInstance;
//import com.ilingtong.app.tongle.utils.BitmapUtils;
//import com.ilingtong.app.tongle.utils.ToastUtils;
//import com.nostra13.universalimageloader.core.DisplayImageOptions;
//import com.nostra13.universalimageloader.core.ImageLoader;
//import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
//
///**
// * User:shuailei
// * Date:2015/6/17
// * Time:AM 11:16
// * Email:leishuai@isoftstone.com
// * Desc:Tongle 我的百宝箱界面
// */
//public class SettingBoxActivity extends BaseActivity implements View.OnClickListener {
//    private ImageView arrow;
//    private TextView top_title;
//    private TextView tongle_name;
//    private TextView tongle_no;
//    private TextView tongle_fans;
//    private TextView tongle_follows;
//    private ImageView head_icon_img_box;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.setting_box_layout);
//        initView();
//        assignment();
//    }
//
//    public void initView() {
//        top_title = (TextView) findViewById(R.id.top_name);
//        tongle_name = (TextView) findViewById(R.id.tongle_name);
//        tongle_no = (TextView) findViewById(R.id.tongle_no);
//        tongle_fans = (TextView) findViewById(R.id.tongle_fans);
//        tongle_follows = (TextView) findViewById(R.id.tongle_follows);
//        head_icon_img_box = (ImageView) findViewById(R.id.head_icon_img_box);
//        arrow = (ImageView) findViewById(R.id.left_arrow_btn);
//        head_icon_img_box.setOnClickListener(this);
//        arrow.setOnClickListener(this);
//        arrow.setVisibility(View.VISIBLE);
//        top_title.setVisibility(View.VISIBLE);
//        top_title.setText(getString(R.string.my_gallery));
//        //下半部分四个模块的控件（包括点击事件）
//        //我的宝贝
//        LinearLayout item_my_box = (LinearLayout) findViewById(R.id.item_my_box);
//        item_my_box.setOnClickListener(this);
//        //app下载推广
//        LinearLayout item_my_app = (LinearLayout) findViewById(R.id.item_my_app);
//        item_my_app.setOnClickListener(this);
//        //我的人脉圈
//        LinearLayout item_my_contacts = (LinearLayout) findViewById(R.id.item_my_contacts);
//        item_my_contacts.setOnClickListener(this);
//        //我的二维码
//        LinearLayout item_my_qrCode = (LinearLayout) findViewById(R.id.item_my_qrCode);
//        item_my_qrCode.setOnClickListener(this);
//    }
//
//    /**
//     * 赋值
//     */
//    public void assignment() {
//        //显示圆角头像
//        DisplayImageOptions options;
//        options = new DisplayImageOptions.Builder()
//                .showStubImage(R.drawable.default_image)            // 设置图片下载期间显示的图片
//                .showImageForEmptyUri(R.drawable.default_image)    // 设置图片Uri为空或是错误的时候显示的图片
//                .showImageOnFail(R.drawable.default_image)        // 设置图片加载或解码过程中发生错误显示的图片
//                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
//                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
//                .displayer(new RoundedBitmapDisplayer(200))    // 设置成圆角图片
//                .bitmapConfig(Bitmap.Config.RGB_565)
//                .build();
//        if (TextUtils.isEmpty(TongleAppInstance.getInstance().getUser_photo_url())) {
//            Drawable drawable = getResources().getDrawable(R.drawable.avatar_normal);
//            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
//            Bitmap bitmap = bitmapDrawable.getBitmap();
//            BitmapDrawable bbb = new BitmapDrawable(BitmapUtils.toRoundCorner(bitmap, 250));
//            head_icon_img_box.setBackgroundDrawable(bbb);
//        } else {
//            ImageLoader.getInstance().displayImage(TongleAppInstance.getInstance().getUser_photo_url(), head_icon_img_box, options);
//        }
//
//        //基本信息
//        tongle_name.setText(TongleAppInstance.getInstance().getUser_nick_name());
//        tongle_no.setText(getString(R.string.tongle_id) + TongleAppInstance.getInstance().getUserID());
//        tongle_fans.setText(getString(R.string.fans) + TongleAppInstance.getInstance().getUser_fans());
//        tongle_follows.setText(getString(R.string.attention) + TongleAppInstance.getInstance().getUser_follows());
//    }
//
//    @Override
//    public void onClick(View v) {
//        if (v.getId() == R.id.left_arrow_btn) { //返回箭头 的点击事件
//            finish();
//        } else if (v.getId() == R.id.item_my_box) {//我的宝贝 点击事件
//            Intent myBox = new Intent(SettingBoxActivity.this, SettingMyBoxBabyActivity.class);
//            startActivity(myBox);
//        } else if (v.getId() == R.id.item_my_app) {//app下载推广 点击事件
//            Intent promotion = new Intent(SettingBoxActivity.this, SettingMyBoxAPPActivity.class);
//            startActivity(promotion);
//        } else if (v.getId() == R.id.item_my_contacts) {//我的人脉圈 点击事件
//            Intent intent = new Intent(SettingBoxActivity.this, SettingBoxContactsActivity.class);
//            startActivity(intent);
//        } else if (v.getId() == R.id.item_my_qrCode) {//我的二维码 点击事件
//            if (ToastUtils.isFastClick()) {
//                return;
//            } else {
//                View view = LayoutInflater.from(SettingBoxActivity.this).inflate(R.layout.expert_popwindow_layout, null);
//                AlertDialog.Builder builder = new AlertDialog.Builder(SettingBoxActivity.this);
//                builder.setView(view);
//                AlertDialog alert = builder.create();
//                alert.setCanceledOnTouchOutside(true);
//                ImageView coverImage = (ImageView) view.findViewById(R.id.qr_user);
//                if (TongleAppInstance.getInstance().getUser_qr_code_url() == null ||TongleAppInstance.getInstance().getUser_qr_code_url().equals("")) {
//                    ToastUtils.toastShort(getString(R.string.setting_box_qrcode_null));
//                } else {
//                    ImageLoader.getInstance().displayImage(TongleAppInstance.getInstance().getUser_qr_code_url(), coverImage, TongleAppInstance.options);
//                    alert.show();
//                }
//            }
//        }
//    }
//}
