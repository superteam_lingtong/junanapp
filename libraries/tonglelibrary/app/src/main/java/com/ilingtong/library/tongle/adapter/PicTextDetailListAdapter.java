package com.ilingtong.library.tongle.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.protocol.ProductPicTextListItem;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 16:44
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class PicTextDetailListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<ProductPicTextListItem> list;
    private ProductPicTextListItem dataItem = new ProductPicTextListItem();
    int mIvWidth;
    private Context context;
    private Dialog dialog;
    private ImageView imageView;
    private int count = 0;

    public PicTextDetailListAdapter(Context context, ArrayList list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
        mIvWidth = context.getResources().getDisplayMetrics().widthPixels - context.getResources().getDimensionPixelSize(R.dimen.iv_margin) * 2;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_post_content, null);
            holder = new ViewHolder();
            holder.listItemImageView = (ImageView) view.findViewById(R.id.pic_url);
            holder.pic_memo = (TextView) view.findViewById(R.id.pic_memo);
            holder.img_play_icon = (ImageView) view.findViewById(R.id.post_content_img_cover_play);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        dataItem = list.get(position);
        //设置是否覆盖显示播放按钮图片
        holder.img_play_icon.setVisibility(TongleAppConst.PRODUCT_PIC_TYPE_VIDEO.equals(dataItem.pic_type) ? View.VISIBLE : View.GONE);
        if (TextUtils.isEmpty(dataItem.pic_url)) {
            holder.listItemImageView.setVisibility(View.GONE);
        } else {
            holder.listItemImageView.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(dataItem.pic_url, holder.listItemImageView, ImageOptionsUtils.getOptions(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bm) {
                    if (bm == null) {
                        super.onLoadingComplete(imageUri, view, bm);
                    } else {
                        float es = (float) mIvWidth / (float) bm.getWidth();
                        int height = (int) (bm.getHeight() * es);
                        ViewGroup.LayoutParams params = view.getLayoutParams();
                        params.height = height;
                        view.setLayoutParams(params);
                    }
                }
            });
        }

        if (TextUtils.isEmpty(dataItem.pic_memo)) {
            holder.pic_memo.setVisibility(View.GONE);
        } else {
            holder.pic_memo.setVisibility(View.VISIBLE);
            holder.pic_memo.setText((CharSequence) dataItem.pic_memo);
        }
        holder.listItemImageView.setDrawingCacheEnabled(true);


        //为listItemImageView添加监听事件
        holder.listItemImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TongleAppConst.PRODUCT_PIC_TYPE_LINK.equals(list.get(position).pic_type) || TongleAppConst.PRODUCT_PIC_TYPE_VIDEO.equals(list.get(position).pic_type)) {
                    //关联类型是外部网址链接
                    Uri uri = Uri.parse(list.get(position).link_url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    context.startActivity(intent);
                } else {

                    count++;
                    Handler handler = new Handler();

                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            if (count == 1) {
                                //创建dialog
                                dialog = new Dialog(context, android.R.style.Theme_NoTitleBar);
                                LinearLayout ll = (LinearLayout) LayoutInflater.from(context).inflate(
                                        R.layout.layout_show_picture, null);
                                //为dialog添加自定义布局
                                dialog.setContentView(ll);
                                imageView = (ImageView) ll.findViewById(R.id.show_picture_scaleView);
                                if (holder.listItemImageView.getDrawable() != null) {
                                    //设置图片
                                    imageView.setImageDrawable(holder.listItemImageView.getDrawable());
                                } else {
                                    imageView.setImageResource(R.drawable.default_image);
                                }
                                dialog.show();
                                count = 0;
                            }
                            if (count == 2) {
                                count = 0;
                            }
                        }
                    };
                    if (count == 1) {
                        handler.postDelayed(r, 500);
                    } else if (count == 2) {
                        handler.post(r);
                    }
                }
            }
        });


        return view;

    }


    static class ViewHolder {
        TextView pic_memo;
        ImageView listItemImageView;
        ImageView img_play_icon;
    }

}




