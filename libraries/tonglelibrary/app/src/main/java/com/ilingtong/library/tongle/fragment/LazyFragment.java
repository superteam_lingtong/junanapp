package com.ilingtong.library.tongle.fragment;

import android.util.Log;

/**
 * Created by wuqian on 2015/11/3.
 * mail: wuqian@ilingtong.com
 * Description:实现fragment的缓加载
 */
public abstract class LazyFragment extends BaseFragment {
    protected boolean isVisible;

    /**
     * 在这里实现Fragment数据的缓加载.
     *
     * @param isVisibleToUser
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.e("tag", "getUserVisibleHint=" + getUserVisibleHint());
        if (getUserVisibleHint()) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }
    }

    protected void onVisible() {
        lazyLoad();
    }

    protected abstract void lazyLoad();

    protected void onInvisible() {
    }
}