package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: syc
 * Date: 2015/6/10
 * Time: 14:18
 * Email: ycshi@isoftstone.com
 * Desc:  getUserOrdersRequest()---
 */
public class UserOrdersResult implements Serializable {

    private BaseInfo head;
    private UserOrdersInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public UserOrdersInfo getBody() {
        return body;
    }

    public void setBody(UserOrdersInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }

}
