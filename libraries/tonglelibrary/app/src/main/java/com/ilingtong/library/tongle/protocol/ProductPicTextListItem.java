package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 15:27
 * Email: jqleng@isoftstone.com
 * Dest: 图文详情
 */
public class ProductPicTextListItem implements Serializable {
    public String pic_url;
    public String pic_memo;
    public String pic_type;    //add on 2017/4/25 图片类型  0:单纯图片 1:视频超链图片  2:超链图片
    public String link_url;    //add on 2017/4/25 网址
}
