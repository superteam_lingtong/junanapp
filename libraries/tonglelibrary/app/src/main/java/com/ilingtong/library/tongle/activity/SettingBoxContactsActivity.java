package com.ilingtong.library.tongle.activity;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.fragment.MyAttentionFragment;
import com.ilingtong.library.tongle.fragment.MyFansFragment;

/**
 * User: shuailei
 * Date: 2015/6/25
 * Time: 15:21
 * Email: leishuai@isoftstone.com
 * Dest:    我的人脉圈
 */
public class SettingBoxContactsActivity extends BaseFragmentActivity implements View.OnClickListener {
    private ImageView left_arrow_btn;
    private TextView top_name;
    private TextView updateText;
    private TextView productText;
    private RelativeLayout updateFrame;
    private RelativeLayout productFrame;
    private ImageView expert_detail_left_cusor;
    private ImageView expert_detail_right_cusor;

    private MyAttentionFragment updateFragment;
    private MyFansFragment productFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_box_contacts_layout);
        initView();
    }
    public void initView(){
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(getString(R.string.my_follows));
        updateText = (TextView) findViewById(R.id.expert_update_text);
        productText = (TextView) findViewById(R.id.expert_product_text);
        updateFrame = (RelativeLayout) findViewById(R.id.update_frame);
        productFrame = (RelativeLayout) findViewById(R.id.product_frame);
        expert_detail_left_cusor = (ImageView) findViewById(R.id.expert_detail_left_cusor);
        expert_detail_right_cusor = (ImageView) findViewById(R.id.expert_detail_right_cusor);
        updateFrame.setOnClickListener(this);
        productFrame.setOnClickListener(this);
        onTabSelected("attention");
    }

    /**
     * 功能：根据tabName，切换fragment
     *
     * @param tabName
     */
    public void onTabSelected(String tabName) {
        Resources resource = (Resources) getResources();
        final ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.expert_select_color);
        final ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.expert_unselect_color);
        if (tabName == "attention") {
            if (null == updateFragment) {
                updateFragment = new MyAttentionFragment();
            }
            replaceFragment(R.id.expert_detail_fragment_container, updateFragment);
            updateText.setTextColor(selectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabName == "fans") {
            if (null == productFragment) {
                productFragment = new MyFansFragment();
            }
            replaceFragment(R.id.expert_detail_fragment_container, productFragment);
            updateText.setTextColor(unselectedTextColor);
            productText.setTextColor(selectedTextColor);
        }
    }

    /**
     * 功能：fragment切换
     *
     * @param viewId
     * @param fragment
     */
    protected void replaceFragment(int viewId, Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(viewId, fragment).commit();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn){
            finish();
        }else if(v.getId()==R.id.update_frame){
            onTabSelected("attention");
            expert_detail_left_cusor.setVisibility(View.VISIBLE);
            expert_detail_right_cusor.setVisibility(View.GONE);
        }else if(v.getId()==R.id.product_frame){
            onTabSelected("fans");
            expert_detail_left_cusor.setVisibility(View.GONE);
            expert_detail_right_cusor.setVisibility(View.VISIBLE);
        }

    }
}
