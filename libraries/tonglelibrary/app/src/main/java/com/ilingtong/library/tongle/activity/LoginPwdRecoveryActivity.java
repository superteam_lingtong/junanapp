package com.ilingtong.library.tongle.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.CodeResult;
import com.ilingtong.library.tongle.protocol.PWRecoveryResult;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.Utils;

/**
 * User: lengjiqiang
 * Date: 2015/5/4
 * Time: 14:55
 * Email: jqleng@isoftstone.com
 * Dest: 密码找回页
 */
public class LoginPwdRecoveryActivity extends BaseActivity implements View.OnClickListener {
    private EditText phoneNumberEdit;
    private EditText validateEdit; //验证码
    private Button validat_btn;
    private ImageButton pwdFind_btn;
    private ImageView left_arrow_btn;
    private TextView top_name;
    private String strPhone; //手机号
    private String validateCode;
    public static String token;
    public static LoginPwdRecoveryActivity instance = null;

    /**
     * 继承Activity的onCreate()方法，实现本类的特殊需求
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_pwd_recovery_layout);
        instance = this;
        initView();
    }

    public void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        pwdFind_btn = (ImageButton) findViewById(R.id.pwdFind_btn);
        validat_btn = (Button) findViewById(R.id.validat_btn);
        validateEdit = (EditText) findViewById(R.id.vali_Code);
        phoneNumberEdit = (EditText) findViewById(R.id.phoneNum);

        left_arrow_btn.setOnClickListener(this);
        pwdFind_btn.setOnClickListener(this);
        pwdFind_btn.setEnabled(false);
        validat_btn.setOnClickListener(this);
        validat_btn.setEnabled(false);

        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setText(R.string.password_recovery);
        top_name.setVisibility(View.VISIBLE);
        validat_btn.setOnClickListener(this);

        //密码找回
        pwdFind_btn = (ImageButton) findViewById(R.id.pwdFind_btn);
        pwdFind_btn.setOnClickListener(this);

        //做手机号的改变监听，判断底部密码找回按钮的背景
        phoneNumberEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                strPhone = phoneNumberEdit.getText().toString();
                validateCode = validateEdit.getText().toString();
                //按钮密码找回，背景图片的操作
                if (strPhone.equals("") || validateCode.equals("")) {
                    pwdFind_btn.setEnabled(false);
                } else {
                    pwdFind_btn.setEnabled(true);
                }
                //判断手机号是否为空，改变验证码的背景
                if (strPhone.equals("")) {
                    validat_btn.setEnabled(false);
                } else {
                    validat_btn.setEnabled(true);
                }

            }
        });

        //做验证码的改变监听，判断底部密码找回按钮的背景
        validateEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                strPhone = phoneNumberEdit.getText().toString();
                validateCode = validateEdit.getText().toString();
                //按钮密码找回，背景图片的操作
                if (strPhone.equals("") || validateCode.equals("")) {
                    pwdFind_btn.setEnabled(false);
                } else {
                    pwdFind_btn.setEnabled(true);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.validat_btn) {
            strPhone = phoneNumberEdit.getText().toString();
            if ("".equals(strPhone)) {
                ToastUtils.toastShort(getString(R.string.regist_phone_error));
                return;
            }
            if (!Utils.isMobileNO(strPhone) || (strPhone.length() != TongleAppConst.PHONE_NUMBER_LENGTH)) {
                ToastUtils.toastShort(getString(R.string.regist_phone_error));
                return;
            }
            if (validat_btn.isEnabled()){
                timer.start();
                ServiceManager.doCodeRequest(strPhone, codeListener(), errorListener());
            }

        } else if (v.getId() == R.id.pwdFind_btn) {
            strPhone = phoneNumberEdit.getText().toString();
            validateCode = validateEdit.getText().toString();
            TongleAppInstance.getInstance().setUser_phone(strPhone);
            //手机号验证
            if ((!Utils.isMobileNO(strPhone) || (strPhone.length() != TongleAppConst.PHONE_NUMBER_LENGTH))) {
                ToastUtils.toastShort(getString(R.string.regist_phone_error));
                return;
            }
            //验证码长度验证
            if (validateCode.length() != TongleAppConst.CODE_LENGTH) {
                ToastUtils.toastShort(getString(R.string.regist_code_error));
                return;
            }
            if (!"".equals(strPhone) && !"".equals(validateCode)) {
                ServiceManager.doPWRecoverRequest(strPhone, validateCode, recoveryListener(), errorListener());
            } else {
                ToastUtils.toastShort(getString(R.string.password_recovery_txt_null));
            }
        }
    }

    //计时器，计算多少秒后重新获取验证码
    private CountDownTimer timer = new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            validat_btn.setText((millisUntilFinished / 1000) + getString(R.string.common_validat_btn_txt_before));
            validat_btn.setEnabled(false);
        }

        @Override
        public void onFinish() {
            validat_btn.setEnabled(true);
            validat_btn.setText(getString(R.string.common_validat_btn_txt_after));
        }
    };

    /**
     * 功能：获取验证码网络响应成功，返回数据
     */
    private Response.Listener<CodeResult> codeListener() {
        return new Response.Listener<CodeResult>() {
            @Override
            public void onResponse(CodeResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.common_get_code_success));
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：密码找回网络响应成功，返回数据
     */
    private Response.Listener<PWRecoveryResult> recoveryListener() {
        return new Response.Listener<PWRecoveryResult>() {
            @Override
            public void onResponse(PWRecoveryResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    token = response.getBody().getToken();
                    //返回成功，进入密码重置界面
                    Intent intent = new Intent(LoginPwdRecoveryActivity.this, LoginResetPwdActivity.class);
                    intent.putExtra("verification_code",validateCode);
                    startActivity(intent);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception));
            }
        };
    }
}
