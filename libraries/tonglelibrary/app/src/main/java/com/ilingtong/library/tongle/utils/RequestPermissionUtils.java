package com.ilingtong.library.tongle.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Package:com.ilingtong.library.tongle.utils
 * author:liuting
 * Date:2016/12/27
 * Desc:请求权限类
 */

public class RequestPermissionUtils {
    private String[] needPermissions;//所需权限
    private IRequestPermissionsListener listener;//权限请求监听
    private Activity activity;//活动
    private static final int PERMISSION_REQUEST_CODE = 100;//权限请求码

    public RequestPermissionUtils(Activity activity,String[] needPermissions,IRequestPermissionsListener listener) {
        this.needPermissions=needPermissions;
        this.listener=listener;
        this.activity=activity;
    }

    /**
     * 获取RequestPermissionUtils
     *
     * @param activity
     * @param needPermissions         所需权限
     * @param listener                权限请求监听
     * @return RequestPermissionUtils 请求权限类
     */
    public static RequestPermissionUtils getRequestPermissionUtils(Activity activity,String[] needPermissions,IRequestPermissionsListener listener) {
        return new RequestPermissionUtils(activity,needPermissions,listener);
    }

    /**
     * 检查权限是否取得
     *
     * @param activity
     */
    public void checkPermissions(Activity activity) {
        List<String> needRequestPermissonList = findDeniedPermissions(needPermissions);
        if (null != needRequestPermissonList
                && needRequestPermissonList.size() > 0) {
            ActivityCompat.requestPermissions(activity,
                    needRequestPermissonList.toArray(
                            new String[needRequestPermissonList.size()]),
                    PERMISSION_REQUEST_CODE);
        } else {
            listener.requestSuccess();
        }
    }

    /**
     * 检查权限是否取得
     *
     * @param fragment
     */
    public void checkPermissions(Fragment fragment) {
        List<String> needRequestPermissonList = findDeniedPermissions(needPermissions);
        if (null != needRequestPermissonList
                && needRequestPermissonList.size() > 0) {
            fragment.requestPermissions(
                    needRequestPermissonList.toArray(
                            new String[needRequestPermissonList.size()]),
                    PERMISSION_REQUEST_CODE);
        } else {
            if(listener!=null) {
                listener.requestSuccess();
            }
        }
    }

    /**
     * 获取权限集中需要申请权限的列表
     *
     * @param permissions
     * @return
     *
     * checkPermissions方法是在用来判断是否app已经获取到某一个权限
     * shouldShowRequestPermissionRationale方法用来判断是否
     * 显示申请权限对话框，如果同意了或者不在询问则返回false
     */
    private List<String> findDeniedPermissions(String[] permissions) {
        List<String> needRequestPermissionList = new ArrayList<String>();
        for (String perm : permissions) {
            if (ContextCompat.checkSelfPermission(activity,
                    perm) != PackageManager.PERMISSION_GRANTED) {
                needRequestPermissionList.add(perm);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        activity, perm)) {
                    needRequestPermissionList.add(perm);
                }
            }
        }
        return needRequestPermissionList;
    }

    /**
     * 申请权限结果的回调方法
     * 拒绝则执行请求失败的操作，允许则执行请求成功的操作
     */
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (!verifyPermissions(paramArrayOfInt)) {
                if(listener!=null){
                    listener.requestFail();
                }
            } else {
                if(listener!=null){
                    listener.requestSuccess();
                }
            }
        }
    }

    /**
     * 检测是否所有的权限都已经授权
     *
     * @param grantResults 给予权限Result
     * @return boolean     true为请求成功，false为请求失败
     */
    private boolean verifyPermissions(int[] grantResults) {
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    /**
     * 权限请求监听
     */
    public interface IRequestPermissionsListener{
        void requestSuccess();//请求成功事件
        void requestFail();//请求失败事件
    }
}

