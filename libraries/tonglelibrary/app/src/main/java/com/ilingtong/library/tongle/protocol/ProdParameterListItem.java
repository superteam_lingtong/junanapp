package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 17:28
 * Email: jqleng@isoftstone.com
 * Desc: 产品规格属性类
 */
public class ProdParameterListItem implements Serializable {
    //参数模块名称
    public String module_name;

    //参数列表
    public ArrayList<ProdParamsSubListItem> parameters;
}
