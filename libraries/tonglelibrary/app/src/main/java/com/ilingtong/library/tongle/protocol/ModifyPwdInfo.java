package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/8
 * Time: 17:31
 * Email: jqleng@isoftstone.com
 * Desc: 修改登录密码信息类，包含服务器返回的数据
 */
public class ModifyPwdInfo implements Serializable {
    private String str = "";

    @Override
    public String toString() {
        return str;
    }
}
