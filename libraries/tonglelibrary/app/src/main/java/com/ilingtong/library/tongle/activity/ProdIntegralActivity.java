package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ilingtong.library.tongle.R;

/**
 * User: shuailei
 * Date: 2015/11/11
 * Time: 11:32
 * Email: leishuai@isoftstone.com
 * Desc: 商品积分详情
 */
public class ProdIntegralActivity extends BaseActivity implements View.OnClickListener{
    //标题栏
    private TextView top_name;
    private ImageView left_arrow_btn;
    private WebView webView;
    private String prod_url,myTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_comm);
        initView();
    }
    public void initView(){
        Bundle bundle = getIntent().getExtras();
        prod_url = bundle.getString("prod_url");
        myTitle = bundle.getString("myTitle");

        top_name = (TextView)findViewById(R.id.top_name);
        left_arrow_btn = (ImageView)findViewById(R.id.left_arrow_btn);
        webView = (WebView)findViewById(R.id.webview);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setText(myTitle);
        top_name.setVisibility(View.VISIBLE);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            /**
             * 加载错误
             *
             * @param view
             * @param errorCode
             * @param description
             * @param failingUrl
             */
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(ProdIntegralActivity.this, R.string.para_exception + description, Toast.LENGTH_SHORT).show();
            }
        });
        webView.loadUrl(prod_url);
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn){
            finish();
        }
    }

}