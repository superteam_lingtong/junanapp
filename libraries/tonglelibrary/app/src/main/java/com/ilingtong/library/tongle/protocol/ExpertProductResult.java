package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 9:51
 * Email: jqleng@isoftstone.com
 * Dest:use by 2019 7010
 */
public class ExpertProductResult implements Serializable {
    private BaseInfo head;
    private ExpertProductInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ExpertProductInfo getBody() {
        return body;
    }

    public void setBody(ExpertProductInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
