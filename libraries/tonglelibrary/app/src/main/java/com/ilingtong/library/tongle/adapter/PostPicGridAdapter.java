package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.PostThumbnailPicUrl;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/6/15
 * Time: 16:24
 * Email: jqleng@isoftstone.com
 * Desc: 帖子中的图片网格适配器
 */
public class PostPicGridAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<PostThumbnailPicUrl> list;
    private Context mContext;

    public PostPicGridAdapter(Context context, ArrayList list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        if (list.size() > 9) {
            return 9;
        } else {
            return list.size();
        }

    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView itemImageView;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.grid_item_post_pic, null);
            itemImageView = (ImageView) convertView.findViewById(R.id.item_image);
            PostThumbnailPicUrl url = (PostThumbnailPicUrl) getItem(position);
            ImageLoader.getInstance().displayImage(url.pic_url, itemImageView, ImageOptionsUtils.getOptions());
        }
        return convertView;
    }
}
