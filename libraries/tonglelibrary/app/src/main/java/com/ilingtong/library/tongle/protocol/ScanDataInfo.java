package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/2
 * Time: 23:32
 * Email: jqleng@isoftstone.com
 * Desc: 扫描数据类,该类包含通过网络返回的数据
 */
public class ScanDataInfo implements Serializable {
    private QRData qr_data;

    public QRData getQRData() {
        return qr_data;
    }

    @Override
    public String toString() {
        return "qr_data:" + qr_data;
    }
}
