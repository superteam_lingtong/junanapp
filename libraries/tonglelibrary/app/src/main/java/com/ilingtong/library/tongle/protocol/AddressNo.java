package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/9
 * Time: 13:05
 * Email: jqleng@isoftstone.com
 * Desc: 保存地址编号的对象，用于后台传递参数使用.address_no不是邮政编码，是后台保存地址的唯一编号
 */
public class AddressNo implements Serializable {
    public String address_no;

    @Override
    public String toString() {
        return "AddressNo{" +
                "address_no='" + address_no + '\'' +
                '}';
    }
}
