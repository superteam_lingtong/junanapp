package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 11:51
 * Email: jqleng@isoftstone.com
 * Desc: 收藏的达人
 */
public class CollectExpertInfo implements Serializable {

    private String data_total_count;
    private ArrayList<FriendListItem> friend_list;

    public String getData_total_count() {
        return data_total_count;
    }

    public ArrayList<FriendListItem> getFriend_list() {
        return friend_list;
    }

    @Override
    public String toString() {
        return "data_total_count:" + data_total_count + "\r\n" +
                "friend_list:" + friend_list;
    }
}
