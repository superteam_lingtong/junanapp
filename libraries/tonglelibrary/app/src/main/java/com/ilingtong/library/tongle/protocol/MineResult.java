package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/2
 * Time: 17:54
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class MineResult implements Serializable {
    private BaseInfo head;
    private MineInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public MineInfo getBody() {
        return body;
    }

    public void setBody(MineInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
