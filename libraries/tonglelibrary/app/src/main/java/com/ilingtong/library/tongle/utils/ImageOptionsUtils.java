package com.ilingtong.library.tongle.utils;

import android.graphics.Bitmap;

import com.ilingtong.library.tongle.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

/**
 * author: liuting
 * Date: 2016/6/3
 * Time: 16:45
 * Email: liuting@ilingtong.com
 * Desc:ImageLoader 加载图片类
 */
public class ImageOptionsUtils {

    /**
     * 图片加载 Options
     *
     * @return
     */
    public static DisplayImageOptions getOptions() {
        return new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.default_image)            // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.default_image)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.default_image)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
                //.displayer(new RoundedBitmapDisplayer(20))	// 设置成圆角图片
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    /**
     * 我的头像 圆形图片 加载Options
     *
     * @return
     */
    public static DisplayImageOptions getRoundOptions() {
        return new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.default_image)            // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.default_image)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.default_image)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
                .displayer(new RoundedBitmapDisplayer(200))    // 设置成圆角图片
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    /**
     * 会员/粉丝/关注人/发帖人头像 加载Options
     *
     * @return
     */
    public static DisplayImageOptions getHeadIconOptions() {
        return new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.head_icon_defualt)            // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.head_icon_defualt)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.head_icon_defualt)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
                //.displayer(new RoundedBitmapDisplayer(20))	// 设置成圆角图片
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }
}
