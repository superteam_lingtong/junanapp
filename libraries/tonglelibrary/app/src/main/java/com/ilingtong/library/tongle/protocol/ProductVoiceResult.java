package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/6/28.
 * mail: wuqian@ilingtong.com
 * Description: 获取音频文件接口返回body use by 2045
 */
public class ProductVoiceResult extends BaseResult implements Serializable{
    public ProductVoiceData body;
}
