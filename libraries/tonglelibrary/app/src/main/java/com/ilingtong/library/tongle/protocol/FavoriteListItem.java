package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 21:44
 * Email: jqleng@isoftstone.com
 * Desc: 添加收藏参数类型
 */
public class FavoriteListItem implements Serializable {
    //收藏类别
    public String collection_type;
    //被收藏商品/魔店/用户/帖子/宝贝商品的ID
    public String key_value;
}
