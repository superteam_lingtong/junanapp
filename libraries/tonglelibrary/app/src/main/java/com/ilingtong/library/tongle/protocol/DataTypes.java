package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/29.
 * mail: wuqian@ilingtong.com
 * Description:基础数据列表  2018接口入口参数对象化用。
 */
public class DataTypes implements Serializable{
    private List<DataTypeName> types;

    public List<DataTypeName> getTypes() {
        return types;
    }

    public void setTypes(List<DataTypeName> types) {
        this.types = types;
    }
}
