package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/7
 * Time: 16:29
 * Email: jqleng@isoftstone.com
 * Desc: 基础数据节点类型
 */
public class BaseDataNode implements Serializable {
    public String code;
    public String name;
}
