package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/14.
 * mail: wuqian@ilingtong.com
 * Description:6012接口（取的团购订单列表）返回json对应entity
 *
 */
public class GroupOrderListResult extends BaseResult implements Serializable{

    private GroupOrderListInfo body;

    public GroupOrderListInfo getBody() {
        return body;
    }

    public void setBody(GroupOrderListInfo body) {
        this.body = body;
    }
}
