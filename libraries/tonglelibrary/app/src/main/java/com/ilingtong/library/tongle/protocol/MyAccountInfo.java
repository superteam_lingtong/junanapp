package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: syc
 * Date: 2015/8/9
 * Time: 13:01
 * Email: ycshi@isoftstone.com
 * Desc: 支付宝支付
 */
public class MyAccountInfo implements Serializable {

    public String account_type_id;
    public String account_type_name;
    public String account_qty;
    public String account_detail_title;
    public String account_detail_url;


    @Override
    public String toString() {
        return "account_type_id="+ "\"" + account_type_id + "\""+ "&" +
                "account_type_name=" + "\""+ account_type_name + "\""+ "&" +
                "account_qty="+ "\"" + account_qty+ "\"" + "&" +
                "account_detail_title="+ "\"" + account_detail_title + "\""+ "&" +
                "account_detail_url="+ "\"" + account_detail_url+ "\"";
    }
}
