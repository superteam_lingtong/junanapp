package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * User: shiyuchong
 * Date: 2015/6/19
 * Time: 14:27
 * Email: ycshi@isoftstone.com
 * Desc:[2014]收益数据列表 my_income_list
 */

public class MyProceeds implements Serializable {

    //收益类型
    public String income_type;
    //区间类型
    public String period_type;
    //曲线标题名称
    public String chart_title;
    //曲线颜色代码
    public String chart_color;
    //曲线X轴字段名
    public String chart_x_field_name;
    //曲线Y轴字段名
    public String chart_y_field_name;
    //曲线Y轴最小值
    public String chart_y_min_value;
    //曲线Y轴最大值
    public String chart_y_max_value;
    //收益数据列表
    public ArrayList<IncomeListData> income_list;





}
