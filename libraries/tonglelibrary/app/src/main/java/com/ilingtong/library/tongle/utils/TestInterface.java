package com.ilingtong.library.tongle.utils;

import com.google.gson.Gson;
import com.ilingtong.library.tongle.TongleAppInstance;

import org.apache.http.util.EncodingUtils;

import java.io.InputStream;

/**
 * Created by wuqian on 2016/3/15.
 * mail: wuqian@ilingtong.com
 * Description:测试接口
 */
public class TestInterface {
    /**
     * 从resource的asset中读取文件数据
     * @param fileName
     * @return
     */
    public static  String readFile(String fileName) {
        String res = "";
        try {

            //得到资源中的asset数据流
            InputStream in = TongleAppInstance.getAppContext().getResources().getAssets().open(fileName);

            int length = in.available();
            byte[] buffer = new byte[length];

            in.read(buffer);
            in.close();
            res = EncodingUtils.getString(buffer, "UTF-8");

        } catch (Exception e) {

            e.printStackTrace();

        }
        return res;
    }

    /**
     * 解析json
     * @param clazz
     * @param fileName
     * @return
     */
    public static Object parseJson(Class clazz, String fileName){
        String json = readFile(fileName);
        Gson gson = new Gson();
        return gson.fromJson(json,clazz);
    }

}
