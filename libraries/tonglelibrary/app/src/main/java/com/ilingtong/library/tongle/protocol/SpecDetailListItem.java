package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by ljq on 2015/5/25.
 */
public class SpecDetailListItem implements Serializable {
    public String spec_detail_id;
    public String spec_detail_name;
}
