package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/15
 * Time: 12:05
 * Email: leishuai@isoftstone.com
 * Desc: 我的领域保存的类型信息
 */
public class FieldTypeInfo implements Serializable {
    public String type_id;
    public String type_name;
}
