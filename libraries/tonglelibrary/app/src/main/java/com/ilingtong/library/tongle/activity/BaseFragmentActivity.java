package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.ilingtong.library.tongle.AppManager;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by wuqian on 2016/1/25.
 * mail: wuqian@ilingtong.com
 * Description: FragmentActivity 统一父类
 */
public class BaseFragmentActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppManager.getAppManager().addActivity(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppManager.getAppManager().removeActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //友盟统计
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //友盟统计
        MobclickAgent.onPause(this);
    }

    @Override
    public void finish() {
        super.finish();
    }
}
