package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.ProdCommentListAdapter;
import com.ilingtong.library.tongle.adapter.TLPagerAdapter;
import com.ilingtong.library.tongle.external.FlowRadioGroup;
import com.ilingtong.library.tongle.fragment.CartFragment;
import com.ilingtong.library.tongle.fragment.MProductFragment;
import com.ilingtong.library.tongle.model.CartModel;
import com.ilingtong.library.tongle.model.ProductDetailModel;
import com.ilingtong.library.tongle.protocol.BillingNowParam;
import com.ilingtong.library.tongle.protocol.CartResult;
import com.ilingtong.library.tongle.protocol.CartSettlementResult;
import com.ilingtong.library.tongle.protocol.FavoriteListItem;
import com.ilingtong.library.tongle.protocol.MStoreDetailCollectResult;
import com.ilingtong.library.tongle.protocol.ProdCartItem;
import com.ilingtong.library.tongle.protocol.ProdDetailQRResult;
import com.ilingtong.library.tongle.protocol.ProdDetailRequestParam;
import com.ilingtong.library.tongle.protocol.ProdSpecListItem;
import com.ilingtong.library.tongle.protocol.ProductDetailResult;
import com.ilingtong.library.tongle.protocol.ProductRatingListItem;
import com.ilingtong.library.tongle.protocol.ProductStockListItem;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * User: lengjiqiang
 * Date: 2015/5/21
 * Time: 14:21
 * Email: jqleng@isoftstone.com
 * Desc: 产品详情页
 */
public class CollectProductDetailActivity extends BaseActivity implements View.OnClickListener {
    private TextView top_name;
    private ImageView left_arrow_btn;
    private ImageView collect_btn;
    private ImageView QR_share_btn;
    private ListView product_detail_listView;
    private Button add_to_cart_btn;//加入购物车
    private Button buy_now;
    private ImageView cart_icon;//购物车图标
    private ScrollView scroll_view;
    private RelativeLayout rl_replace;
    private TextView tv_replace;

    private View contentView;//整个布局view
    private ViewPager productViewPager;//顶部page
    private LinearLayout banner_bottom;//banner圆点
    private ImageView to_mstore_icon;//回到魔店图标
    private TextView productName;//产品名字
    private TextView productPrice;//产品价格
    private TextView moreBtn;//更多图标
    private ImageView shareBtn;//分享图标
    private LinearLayout specLinearLayout;//下边的横线
    private LinearLayout prod_rule_linear;//
    private TextView prd_num;//产品数量
    private TextView prod_service;//产品服务
    private TextView prod_notify;//产品提示
    private TextView prod_rule;//退换货
    private ImageView btnMiuns;//数量减
    private EditText productnumber;//数量输入框
    private ImageView btnAdd;//数量加
    private Button cancle, collect;//收藏弹出框-》取消，收藏
    private TextView mPreSelectedBt;//选中后

    private String prodID;//商品ID
    private TLPagerAdapter pageAdapter;
    private ArrayList<View> productPicListView;
    private CheckBox[] cb = new CheckBox[2];
    private PopupWindow popupWindow;
    private View mTouchTarget;
    private ScheduledExecutorService scheduledExecutorService;
    private int currentItem = 0;//当前页面

    private boolean bIsFavorate = false;
    private ProductDetailModel productDetailModel;
    private ProdDetailRequestParam param;
    private ProgressDialog pDlg;

    ArrayList<ProdSpecListItem> list1 = new ArrayList<ProdSpecListItem>();
    private String collectType;
    private ImageView country_pic;//进口国家标致图
    private TextView country_info;//进口信息描述
    private TextView freight;//配送费描述
    private TextView tariff_pay;//关税
    private LinearLayout home_import_prod_flag;//进口表示linearlayout
    private TextView prod_stock_qty;//剩余库存
    private ArrayList<ProductStockListItem> stock_list = new ArrayList<>();//库存列表
    private String spec_detail_id1 = "";   //选中的第一个规格明细ID
    private String spec_detail_id2 = "";   //选中的第二个规格明细ID

    private boolean buyNowEnableFlag = true;  //防止立即购重复点击标志
    private ProdCommentListAdapter commentListAdapter;  //评价列表adapter
    private ArrayList<ProductRatingListItem> commentList = new ArrayList<>();  //好评列表

    private String action;
    private String relationId;
    private String mStoreId;
    private String postId;
    private TextView mTxtLimit;//限购数量
    private TextView txt_need_pay_point;
    private TextView txt_cost_of_production;


    private int intoType = 0;  //表示从哪个页面跳转来
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    if (bIsFavorate) {
                        collect_btn.setBackgroundResource(R.drawable.discollect_button_style);
                    } else {
                        collect_btn.setBackgroundResource(R.drawable.collect_button_style);
                    }
                    break;
            }
        }
    };

    /**
     * 启动商品详情activity
     *
     * @param activity
     * @param product_id  商品id
     * @param action      商品明细入口
     *                    1 首页活动
     *                    2 帖子链接
     *                    4	扫描条码
     *                    5	购物车
     *                    6	魔店
     * @param relation_id 二维码关联id
     * @param mstore_id   魔店id
     * @param post_id     帖子id
     */
    public static void launch(Activity activity, String product_id, String action, String relation_id, String mstore_id, String post_id) {
        Intent detailIntent = new Intent(activity, CollectProductDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("product_id", product_id);
        bundle.putString("action", action);
        bundle.putString("relationId", relation_id);
        bundle.putString("mstore_id", mstore_id);
        bundle.putString("mypostId", post_id);
        detailIntent.putExtras(bundle);
        activity.startActivity(detailIntent);
    }

    /**
     * 启动商品详情activity
     *
     * @param activity
     * @param product_id  商品id
     * @param action      商品明细入口
     *                    1 首页活动
     *                    2 帖子链接
     *                    4	扫描条码
     *                    5	购物车
     *                    6	魔店
     * @param relation_id 二维码关联id
     * @param mstore_id   魔店id
     * @param post_id     帖子id
     * @param intoType
     */
    public static void launch(Activity activity, String product_id, String action, String relation_id, String mstore_id, String post_id, int intoType) {
        Intent detailIntent = new Intent(activity, CollectProductDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("product_id", product_id);
        bundle.putString("action", action);
        bundle.putString("relationId", relation_id);
        bundle.putString("mstore_id", mstore_id);
        bundle.putString("mypostId", post_id);
        bundle.putInt(TongleAppConst.INTO_TYPE, intoType);
        detailIntent.putExtras(bundle);
        activity.startActivity(detailIntent);
    }

    /**
     * 启动商品详情activityForResult
     *
     * @param activity
     * @param product_id  商品id
     * @param action      商品明细入口
     *                    1 首页活动
     *                    2 帖子链接
     *                    4	扫描条码
     *                    5	购物车
     *                    6	魔店
     * @param relation_id 二维码关联id
     * @param mstore_id   魔店id
     * @param post_id     帖子id
     * @param intoType
     */
    public static void launchForResult(Activity activity, String product_id, String action, String relation_id, String mstore_id, String post_id, int intoType, int requestCode) {
        Intent detailIntent = new Intent(activity, CollectProductDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("product_id", product_id);
        bundle.putString("action", action);
        bundle.putString("relationId", relation_id);
        bundle.putString("mstore_id", mstore_id);
        bundle.putString("mypostId", post_id);
        bundle.putInt(TongleAppConst.INTO_TYPE, intoType);
        detailIntent.putExtras(bundle);
        activity.startActivityForResult(detailIntent, requestCode);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collect_product_detail);
        getData();
        initView();
        doRequest();
    }

    public void getData() {
        //       cartflag = getIntent().getExtras().getString("cartlist");
        prodID = getIntent().getExtras().getString("product_id", "");
        mStoreId = getIntent().getExtras().getString("mstore_id", "");
        action = getIntent().getExtras().getString("action", "");
        relationId = getIntent().getExtras().getString("relationId", "");
        postId = getIntent().getExtras().getString("mypostId", "");
        intoType = getIntent().getIntExtra(TongleAppConst.INTO_TYPE, 0);
        if (TongleAppConst.ACTIONID_MY_BABY.equals(action)) {
            collectType = "baby";
        }

        intoType = getIntent().getIntExtra(TongleAppConst.INTO_TYPE, 0);
    }

    public void initView() {
        //创建并弹出“加载中Dialog”
        pDlg = new ProgressDialog(this);
        pDlg.setCancelable(false);
        pDlg.setMessage(getString(R.string.loading));
        pDlg.show();

        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        collect_btn = (ImageView) findViewById(R.id.collect_btn);
        QR_share_btn = (ImageView) findViewById(R.id.QR_share_btn);
        product_detail_listView = (ListView) findViewById(R.id.product_detail_listView);
        add_to_cart_btn = (Button) findViewById(R.id.add_to_cart_btn);
        buy_now = (Button) findViewById(R.id.buy_now);
        cart_icon = (ImageView) findViewById(R.id.cart_icon);
        scroll_view = (ScrollView) findViewById(R.id.scroll_view);
        rl_replace = (RelativeLayout) findViewById(R.id.rl_replace);
        tv_replace = (TextView) findViewById(R.id.tv_replace);
        left_arrow_btn.setOnClickListener(this);
        collect_btn.setOnClickListener(this);
        QR_share_btn.setOnClickListener(this);
        add_to_cart_btn.setOnClickListener(this);
        buy_now.setOnClickListener(this);
        cart_icon.setOnClickListener(this);

        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(R.string.product_detail);
        rl_replace.setVisibility(View.VISIBLE);
        tv_replace.setVisibility(View.GONE);

        contentView = LayoutInflater.from(this).inflate(R.layout.product_detail_head_view, null);
        //设置图片比例为4:3
        productViewPager = (ViewPager) contentView.findViewById(R.id.product_pic_viewpager);
        ViewGroup.LayoutParams params1 = productViewPager.getLayoutParams();
        params1.height = (DipUtils.getScreenWidth(this) * 3 / 4);
        productViewPager.setLayoutParams(params1);

        banner_bottom = (LinearLayout) contentView.findViewById(R.id.banner_bottom);
        to_mstore_icon = (ImageView) contentView.findViewById(R.id.to_mstore_icon);
        to_mstore_icon.setOnClickListener(this);
        productName = (TextView) contentView.findViewById(R.id.product_name);
        productPrice = (TextView) contentView.findViewById(R.id.product_price);
        txt_need_pay_point = (TextView) contentView.findViewById(R.id.product_point);
        txt_cost_of_production = (TextView) contentView.findViewById(R.id.product_cost_of_production);
        moreBtn = (TextView) contentView.findViewById(R.id.product_detail_right_arrow);
        moreBtn.setOnClickListener(this);
        shareBtn = (ImageView) contentView.findViewById(R.id.share_btn);
        shareBtn.setOnClickListener(this);
        specLinearLayout = (LinearLayout) contentView.findViewById(R.id.spec_liner);
        prd_num = (TextView) contentView.findViewById(R.id.prd_num);
        btnMiuns = (ImageView) contentView.findViewById(R.id.product_numMiuns);
        btnMiuns.setOnClickListener(this);
        productnumber = (EditText) contentView.findViewById(R.id.product_number);
        //productnumber.setEnabled(false);
        prod_service = (TextView) contentView.findViewById(R.id.prod_service);
        prod_notify = (TextView) contentView.findViewById(R.id.prod_notify);
        prod_rule = (TextView) contentView.findViewById(R.id.prod_rule);
        prod_rule_linear = (LinearLayout) contentView.findViewById(R.id.prod_rule_linear);
        prod_rule_linear.setOnClickListener(this);
        btnAdd = (ImageView) contentView.findViewById(R.id.product_numAdd);
        btnAdd.setOnClickListener(this);

        country_pic = (ImageView) contentView.findViewById(R.id.country_pic);
        country_info = (TextView) contentView.findViewById(R.id.country_info);
        freight = (TextView) contentView.findViewById(R.id.freight);
        tariff_pay = (TextView) contentView.findViewById(R.id.tariff_pay);
        home_import_prod_flag = (LinearLayout) contentView.findViewById(R.id.home_import_prod_flag);
        prod_stock_qty = (TextView) contentView.findViewById(R.id.product_stock_qty);//剩余库存
        mTxtLimit = (TextView) contentView.findViewById(R.id.product_txt_limit);//限购数量

        product_detail_listView.addHeaderView(contentView);
        product_detail_listView.setAdapter(null);

        productPicListView = new ArrayList<View>();
        pageAdapter = new TLPagerAdapter(productPicListView);


        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.default_image)            // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.moshop_icon)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.default_image)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
                .displayer(new RoundedBitmapDisplayer(90))    // 设置成圆角图片
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        ImageLoader.getInstance().displayImage(null, to_mstore_icon, options);


        //商品价格和名称设置边距
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params2.setMargins(30, 0, 20, 5);
        productName.setLayoutParams(params2);
        productPrice.setLayoutParams(params2);

        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new ViewPagerTask(), 1, 3, TimeUnit.SECONDS);
        if (productDetailModel == null)
            productDetailModel = new ProductDetailModel();
    }

    public void doRequest() {
        param = new ProdDetailRequestParam();
        param.user_id = TongleAppInstance.getInstance().getUserID();
        param.product_id = prodID;
        param.mstore_id = mStoreId;
        param.post_id = postId;
        param.action = action;
        param.relation_id = relationId;
        ServiceManager.doProductDetailRequest(param, successListener(), errorListener());
    }

    /**
     * 创建规格
     */
    public void createSpecView() {

        //根据规格列表数量来创建规格一栏
        for (int i = 0; i < productDetailModel.productInfo.prod_spec_list.size(); i++) {
            //加入购物车时需要的参数。有几个规格条目则创建几个ProdSpecListItem。并依次添加到list1.
            final ProdSpecListItem psl = new ProdSpecListItem();
            psl.prod_spec_id = productDetailModel.productInfo.prod_spec_list.get(i).prod_spec_id;
            psl.prod_spec_name = productDetailModel.productInfo.prod_spec_list.get(i).prod_spec_name;
            //加入购物车参数中的列表
            list1.add(psl);

            //根据返回数据size动态创建规格，linearlayout，textview
            LinearLayout linear = new LinearLayout(this);
            TextView tv = new TextView(this);
            tv.setTextSize(12);
            FlowRadioGroup radioGroup = new FlowRadioGroup(this);
            RadioGroup.LayoutParams lp = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            radioGroup.setItemPadding(10, 10);
            //根据规格列表明细的数量来创建各个规格明细item
            for (int j = 0; j < productDetailModel.productInfo.prod_spec_list.get(i).spec_detail_list.size(); j++) {
                final RadioButton radioButton = new RadioButton(this);
                radioButton.setBackgroundResource(R.drawable.product_spec_item_bg_seletor);
                radioButton.setButtonDrawable(new ColorDrawable(Color.TRANSPARENT));
                radioButton.setPadding(30, 10, 30, 10);
                radioButton.setTag(R.id.spec_tag_second, productDetailModel.productInfo.prod_spec_list.get(i).spec_detail_list.get(j).spec_detail_id);
                radioButton.setTag(R.id.spec_tag_first, i);
                radioButton.setTag(R.id.spec_tag_spec_name, productDetailModel.productInfo.prod_spec_list.get(i).prod_spec_name);
                radioButton.setId(100*i+j);
                radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {   //选中时记住当前的规格
                            int spec = (int) buttonView.getTag(R.id.spec_tag_first);
                            String spec_detail_id = buttonView.getTag(R.id.spec_tag_second).toString();
                            String spec_name = buttonView.getTag(R.id.spec_tag_spec_name).toString();
                            //用于记录当前选中的规格明细ID以及与之对应的规格名称。在加入购物车时通过判断spec_detail_id是否为空来提醒用户选择规格
                            for (int i = 0; i < list1.size(); i++) {
                                if (spec_name.equals(list1.get(i).prod_spec_name)) {
                                    list1.get(i).spec_detail_id = spec_detail_id;
                                }
                            }
                            //spec 为0表示为第一个规格条目，spec为1表示为第二个规格条目
                            if (spec == 0) {
                                spec_detail_id1 = spec_detail_id;
                            } else if (spec == 1) {
                                spec_detail_id2 = spec_detail_id;
                            }
                            refreshPriceAndStock(spec_detail_id1, spec_detail_id2);
                            Log.e("tag","点击了:"+radioButton.getId());
                        }
                    }
                });
                radioButton.setText(productDetailModel.productInfo.prod_spec_list.get(i).spec_detail_list.get(j).spec_detail_name);
                radioGroup.addView(radioButton, lp);
            }
            radioGroup.check(100*i+0);

            //linearlayout设置边距
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 15, 18, 15);//(左上右下)
            linear.setLayoutParams(params);

            //textview设置边距
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params1.setMargins(30, 10, 20, 0);
            tv.setLayoutParams(params1);
            prd_num.setLayoutParams(params1);

            //将textview和gridview添加到linearlayout中
            linear.addView(tv);
            linear.addView(radioGroup);
            //textview赋值
            String name = productDetailModel.productInfo.prod_spec_list.get(i).prod_spec_name;
            tv.setText(name);

            specLinearLayout.addView(linear);
        }
    }

    /**
     * 选中规格后刷新库存和价格
     *
     * @param spec_detail_id1
     * @param spec_detail_id2
     */
    private void refreshPriceAndStock(String spec_detail_id1, String spec_detail_id2) {
        for (int i = 0; i < stock_list.size(); i++) {
            if (spec_detail_id1.equals(stock_list.get(i).spec_detail_id1) && spec_detail_id2.equals(stock_list.get(i).spec_detail_id2)) {
                //productPrice.setText(getString(R.string.RMB) + stock_list.get(i).spec_price);
                productPrice.setText(FontUtils.getMoneyType(stock_list.get(i).spec_price));
                prod_stock_qty.setText(stock_list.get(i).spec_stock_qty + "");
                if (Integer.parseInt(stock_list.get(i).spec_point) > 0) {
                    txt_need_pay_point.setVisibility(View.VISIBLE);
                    txt_need_pay_point.setText("+ " + stock_list.get(i).spec_point + "积分");
                } else {
                    txt_need_pay_point.setVisibility(View.GONE);
                }
                return;
            }
        }
    }

    public void addProductPicView() {
        if ((productPicListView == null) || (productDetailModel.productInfo.prod_pic_url_list.size() == 0))
            return;

        productPicListView.clear();
        try {
            for (int i = 0; i < productDetailModel.productInfo.prod_pic_url_list.size(); i++) {
                String url = productDetailModel.productInfo.prod_pic_url_list.get(i).pic_url;
                ImageView aPieceOfView = (ImageView) LayoutInflater.from(this).inflate(R.layout.banner_cell_layout, null);
                ImageLoader.getInstance().displayImage(url, aPieceOfView, ImageOptionsUtils.getOptions());
                productPicListView.add(aPieceOfView);
            }
            banner_bottom.removeAllViews();
            //定义viewpager下的圆点
            for (int i = 0; i < productPicListView.size(); i++) {
                TextView bt = new TextView(this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(16, 16);
                params.setMargins(10, 0, 0, 0);
                bt.setLayoutParams(params);
                bt.setBackgroundResource(R.color.main_bgcolor);
                banner_bottom.addView(bt);
                pageAdapter.notifyDataSetChanged();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        productViewPager.setAdapter(pageAdapter);
        productViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int mPreviousState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (mPreSelectedBt != null) {
                    mPreSelectedBt.setBackgroundResource(R.color.main_bgcolor);
                }

                TextView currentBt = (TextView) banner_bottom.getChildAt(position);
                currentBt.setBackgroundResource(R.color.topview_bgcolor);
                mPreSelectedBt = currentBt;

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // All of this is to inhibit any scrollable container from consuming our touch events as the user is changing pages
                if (mPreviousState == ViewPager.SCROLL_STATE_IDLE) {
                    if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                        mTouchTarget = productViewPager;
                    }
                } else {
                    if (state == ViewPager.SCROLL_STATE_IDLE || state == ViewPager.SCROLL_STATE_SETTLING) {
                        mTouchTarget = null;
                    }
                }

                mPreviousState = state;
            }
        });
    }


    /**
     * 获取到库存总量
     *
     * @param ProductStockListItem
     * @return int 库存总量
     */
    public long getSumStock(ArrayList<ProductStockListItem> ProductStockListItem) {
        long sumStock = 0;
        for (int i = 0; i < ProductStockListItem.size(); i++) {
            Log.i("spec_stock_qty", ProductStockListItem.get(i).spec_stock_qty);
            sumStock = sumStock + Long.parseLong(ProductStockListItem.get(i).spec_stock_qty);
        }
        return sumStock;
    }

    @Override
    protected void onPause() {
        super.onPause();
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new ViewPagerTask(), 1, 3, TimeUnit.SECONDS);
        MobclickAgent.onPageStart("productDetail");
        MobclickAgent.onPause(getApplicationContext());
    }

    /**
     * 从图文详情返回后刷新顶部收藏图标
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10001) {
            if (resultCode == RESULT_OK) {
                boolean flag = data.getBooleanExtra("bIsFavorate", false);
                //如果传到图文详情的图标和传回的标识不同，说明在图文详情页做了收藏或者取消收藏的操作。需要改变头部收藏图标
                if (flag != bIsFavorate) {
                    bIsFavorate = flag;
                    //如果在图文详情页面取消了收藏，回到收藏/宝贝列表页都需要刷新列表
                    if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                        MProductFragment.UPDATE_LIST_FLAG = !MProductFragment.UPDATE_LIST_FLAG;
                    } else if (intoType == TongleAppConst.MYBABY_INTO) {
                        SettingMyBoxBabyActivity.UPDATE_LIST_FLAG = !SettingMyBoxBabyActivity.UPDATE_LIST_FLAG;
                    } else {
                        MProductFragment.MPRODUCTFRAGMENT_UPDATE_FLAG = true;
                    }
                    mHandler.sendEmptyMessage(0);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        int i1 = v.getId();
        if (i1 == R.id.product_detail_right_arrow) { //去图文详情
            if (!TextUtils.isEmpty(prodID)) {
                Intent moreIntent = new Intent(this, CollectProductMoreActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("product_id", prodID);
                bundle.putBoolean("prod_favorited_by_me", bIsFavorate);
                bundle.putInt(TongleAppConst.INTO_TYPE, intoType);
                bundle.putString("rela_id", productDetailModel.productInfo.relation_id);
                bundle.putString("type", "");
                moreIntent.putExtras(bundle);
                startActivityForResult(moreIntent, 10001);
            }

        } else if (i1 == R.id.share_btn) {  //弹出分享dialog
            ShareDialogActivity.launch(CollectProductDetailActivity.this, ShareDialogActivity.TYPE_SHARE_PRODUCT, prodID, productDetailModel.productInfo.relation_id);
        } else if (i1 == R.id.left_arrow_btn) {
//            if (cartflag != null)
//                goToCart();
            finish();

        } else if (i1 == R.id.product_numAdd) {
            String edit1 = productnumber.getText().toString();
            int i = Integer.valueOf(edit1).intValue();
            if (i >= Long.parseLong(prod_stock_qty.getText().toString()) || (Integer.parseInt(productDetailModel.productInfo.buy_limit_qty) > 0 && i >= Integer.parseInt(productDetailModel.productInfo.buy_limit_qty))) {
                //商品限购同时数量大于了限购数或者达到库存量后不能再加了
                ToastUtils.toastShort(getString(R.string.common_number_max));
            } else {
                i += 1;
                productnumber.setText(i + "");
            }

        } else if (i1 == R.id.product_numMiuns) {
            String edit2 = productnumber.getText().toString();
            int j = Integer.valueOf(edit2).intValue();
            if (j > 1) {
                j -= 1;
                productnumber.setText(j + "");
            } else {
                ToastUtils.toastShort(getString(R.string.common_number_min));
            }

        } else if (i1 == R.id.QR_share_btn) {
            ServiceManager.doProdDetailQRRequest(TongleAppInstance.getInstance().getUserID(), prodID, productDetailModel.productInfo.relation_id, QRListener(), errorListener());

        } else if (i1 == R.id.collect_btn) {
            if (ToastUtils.isFastClick()) {
                return;
            } else {
                if (bIsFavorate == false) {
                    showFavDlg(v);
                } else {
                    if (collectType != null) {//取消收藏判断，如果是“我的宝贝”进入则取消我的宝贝
                        ServiceManager.doMStoreCancelCollectRequest(TongleAppInstance.getInstance().getUserID(), prodID, TongleAppConst.MYPRODUCT, collectListener(), errorListener());
                    } else {
                        ServiceManager.doMStoreCancelCollectRequest(TongleAppInstance.getInstance().getUserID(), prodID, TongleAppConst.PRODUCT, collectListener(), errorListener());
                    }
                }
            }

        } else if (i1 == R.id.cart_icon) {
            goToCart();

        } else if (i1 == R.id.add_to_cart_btn) {
            addToCart();

        } else if (i1 == R.id.buy_now) {
            if (buyNowEnableFlag) {
                buyNow();
            }

        } else if (i1 == R.id.to_mstore_icon) {
            if (!TextUtils.isEmpty(productDetailModel.productInfo.mstore_id)) {
                Intent intent = new Intent(CollectProductDetailActivity.this, CollectMStoreDetailActivity.class);
                Bundle bundle2 = new Bundle();
                bundle2.putString("mstore_id", productDetailModel.productInfo.mstore_id);
                intent.putExtras(bundle2);
                startActivity(intent);
            }

        } else if (i1 == R.id.prod_rule_linear) {//拼接url
            String myurl = productDetailModel.productInfo.point_rule_url + "?user_id=" + TongleAppInstance.getInstance().getUserID() + "&user_token=" + TongleAppInstance.getInstance().getToken() + "&product_id=" + productDetailModel.productInfo.prod_id + "&mstore_id=" + productDetailModel.productInfo.mstore_id + "&app_inner_no=" + TongleAppInstance.getInstance().getApp_inner_no();
            Intent expertIntent = new Intent(CollectProductDetailActivity.this, ProdIntegralActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("prod_url", myurl);
            bundle.putString("myTitle", productDetailModel.productInfo.point_rule_title);
            expertIntent.putExtras(bundle);
            startActivity(expertIntent);

        }
    }

    /**
     * 功能：前往购物车
     */

    private void goToCart() {
        finish();
        Intent intent = new Intent(CollectProductDetailActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_NAME, TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_CART);
        startActivity(intent);
    }

    /**
     * 功能：加入购物车
     */
    private void addToCart() {
        //商品限购同时数量大于了限购数，则提示超出了限购数量
        if (Integer.parseInt(productDetailModel.productInfo.buy_limit_qty) > 0 && Integer.parseInt(productnumber.getText().toString()) > Integer.parseInt(productDetailModel.productInfo.buy_limit_qty)) {
            ToastUtils.toastShort(getString(R.string.common_buy_limit_error));
            return;
        }
        //数量大于了库存数，则提示库存不足，无论商品是否限购
        if (Integer.parseInt(productnumber.getText().toString()) > Long.parseLong(prod_stock_qty.getText().toString())) {
            ToastUtils.toastShort(R.string.product_detail_buy_error);
            return;
        }
        ProdCartItem cartItem = new ProdCartItem();
        cartItem.product_id = prodID;
        cartItem.prod_spec_list = list1;
        cartItem.order_qty = productnumber.getText().toString();
        for (int k = 0; k < productDetailModel.productInfo.prod_spec_list.size(); k++) {
            if (list1.get(k).spec_detail_id == null) {
                ToastUtils.toastShort(getString(R.string.collect_product_detail_select) + list1.get(k).prod_spec_name);
                return;
            }
        }
        if (cartItem.order_qty == null || cartItem.order_qty.equals("") || cartItem.order_qty.equals("0")) {
            ToastUtils.toastShort(getString(R.string.collect_product_detail_confirm_number));
            return;
        }
        pDlg.show();
        ServiceManager.addToCart(TongleAppInstance.getInstance().getUserID(), productDetailModel.postInfo.post_id, cartItem, productDetailModel.productInfo.mstore_id, productDetailModel.productInfo.relation_id, AddToCartListener(), errorListener());
        Map<String, String> map_ekv = new HashMap<String, String>();
        map_ekv.put("product", productName.getText().toString());
        MobclickAgent.onEvent(getApplicationContext(), "addCartclick", map_ekv);
    }

    /**
     * 功能：立即购买（得先加入购物车请求到返回数据，根据返回数据得到商品信息）
     */

    private void buyNow() {
        //商品限购同时数量大于了限购数，则提示超出了限购数量
        if (Integer.parseInt(productDetailModel.productInfo.buy_limit_qty) > 0 && Integer.parseInt(productnumber.getText().toString()) > Integer.parseInt(productDetailModel.productInfo.buy_limit_qty)) {
            ToastUtils.toastShort(getString(R.string.common_buy_limit_error));
            return;
        }
        //数量大于了库存数，则提示库存不足，无论商品是否限购
        if (Integer.parseInt(productnumber.getText().toString()) > Long.parseLong(prod_stock_qty.getText().toString())) {
            ToastUtils.toastShort(R.string.product_detail_buy_error);
            return;
        }
        ProdCartItem cartItem = new ProdCartItem();
        cartItem.product_id = prodID;
        cartItem.prod_spec_list = list1;
        cartItem.order_qty = productnumber.getText().toString();
        for (int k = 0; k < productDetailModel.productInfo.prod_spec_list.size(); k++) {
            if (list1.get(k).spec_detail_id == null) {
                ToastUtils.toastShort(getString(R.string.collect_product_detail_select) + list1.get(k).prod_spec_name);
                return;
            }
        }
        if (cartItem.order_qty == null || cartItem.order_qty.equals("") || cartItem.order_qty.equals("0")) {
            ToastUtils.toastShort(getString(R.string.collect_product_detail_confirm_number));
            return;
        }
        String mstore_id;//魔店id，全部都有魔店id
        if (productDetailModel.productInfo.mstore_id != null) {
            mstore_id = productDetailModel.productInfo.mstore_id;
        } else {
            mstore_id = "";
        }

        //ServiceManager.addToCart(TongleAppInstance.getInstance().getUserID(), productDetailModel.postInfo.post_id, cartItem, mstore_id, productDetailModel.productInfo.relation_id, BuyNowListener(), errorListener());

        BillingNowParam billingNowParam = new BillingNowParam(prodID,productnumber.getText().toString(),list1);
        Gson gson = new Gson();
        Map params = new HashMap();
        params.put("product_info",billingNowParam);
        ServiceManager.balanceNow(gson.toJson(params),productDetailModel.postInfo.post_id,productDetailModel.productInfo.relation_id,"",CartSuccessListener(),errorListener());
        buyNowEnableFlag = false;
    }


    /**
     * 功能：加入购物监听
     */
    private Response.Listener AddToCartListener() {
        return new Response.Listener<CartResult>() {
            @Override
            public void onResponse(CartResult response) {
                pDlg.dismiss();
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.collect_product_detail_add_success));
                    CartFragment.CARTFRAGMENT_UPDATE_FLAG = true;
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：立即购买
     */
//    private Response.Listener BuyNowListener() {
//        return new Response.Listener<CartResult>() {
//            @Override
//            public void onResponse(CartResult response) {
//                buyNowEnableFlag = true;
//                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
//                    CartModel.checkList = response.getBody().getThis_shopping_cart();
//
//                    if (CartModel.checkList.size() != 0) {
//                        ArrayList obj = new ArrayList();
//                        for (int i = 0; i < CartModel.checkList.size(); i++) {
//                            //立即购买现在所调用的是购物车结算接口，所以这里需要判断商品限购同时购物车中的数量大于限购数，则提示超出限购数量
//                            if (Integer.parseInt(CartModel.checkList.get(i).buy_limit_qty) > 0 && Integer.parseInt(CartModel.checkList.get(i).order_qty) > Integer.parseInt(CartModel.checkList.get(i).buy_limit_qty)) {
//                                ToastUtils.toastShort(getString(R.string.cart_limit_buy_error));
//                                return;
//                            }
//                            obj.add(CartModel.checkList.get(i).seq_no);
//                        }
//                        CartFragment.CARTFRAGMENT_UPDATE_FLAG = true;
//                        ServiceManager.addCartRequest(TongleAppInstance.getInstance().getUserID(), obj, CartSuccessListener(), errorListener());
//                        buyNowEnableFlag = false;
//
//                    }
//                } else {
//                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
//                }
//            }
//        };
//    }


    /**
     * 用来完成图片切换的任务
     */
    private class ViewPagerTask implements Runnable {
        public void run() {
            //实现我们的操作
            //改变当前页面
            currentItem = (currentItem + 1) % productPicListView.size();
            //Handler来实现图片切换
            handler.obtainMessage().sendToTarget();
        }
    }

    private Handler handler = new Handler() {

        public void handleMessage(Message msg) {
            //设定viewPager当前页面
            productViewPager.setCurrentItem(currentItem);
        }
    };

    @Override
    protected void onStop() {
        //停止图片切换
        scheduledExecutorService.shutdown();
        super.onStop();
    }

    /**
     * 功能：商品二维码网络响应成功，返回数据
     */
    private Response.Listener QRListener() {
        return new Response.Listener<ProdDetailQRResult>() {
            @Override
            public void onResponse(ProdDetailQRResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    productDetailModel.prod_qr_code_url = response.getBody().getProd_qr_code_url();
                    productDetailModel.latest_relation_id = response.getBody().getLatest_relation_id();
//                    ShowQRCodeDialog dialog = new ShowQRCodeDialog(CollectProductDetailActivity.this, productDetailModel.prod_qr_code_url, productDetailModel.latest_relation_id, TongleAppConst.STRRELATE_PRODUCT);
//                    dialog.show();
                    ShowQRCodeActivity.launcher(CollectProductDetailActivity.this,productDetailModel.prod_qr_code_url,productDetailModel.latest_relation_id,TongleAppConst.STRRELATE_PRODUCT);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：商品详情网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<ProductDetailResult>() {
            @Override
            public void onResponse(ProductDetailResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    rl_replace.setVisibility(View.GONE);
                    scroll_view.setVisibility(View.VISIBLE);
                    collect_btn.setVisibility(View.VISIBLE);
                    QR_share_btn.setVisibility(View.VISIBLE);
                    scroll_view.setVisibility(View.VISIBLE);

                    productDetailModel.productInfo = response.getBody().getProd_info();
                    productDetailModel.postInfo = response.getBody().getPost_info();
                    //保存库存列表
                    stock_list.clear();  //防止重复添加
                    stock_list.addAll(response.getBody().getProd_info().stock_list);
                    //关税信息
                    ImageLoader.getInstance().displayImage(productDetailModel.productInfo.country_pic_url, country_pic, ImageOptionsUtils.getOptions());
                    country_info.setText(productDetailModel.productInfo.import_info_desc);
                    freight.setText(productDetailModel.productInfo.transfer_fee_desc);
                    tariff_pay.setText(productDetailModel.productInfo.tariff_desc);
                    if (productDetailModel.productInfo.import_goods_flag.equals("0")) {
                        home_import_prod_flag.setVisibility(View.VISIBLE);
                    } else {
                        home_import_prod_flag.setVisibility(View.GONE);
                    }
                    addProductPicView();
                    productName.setText(productDetailModel.productInfo.prod_name);
                    txt_cost_of_production.setText(FontUtils.getMoneyType(productDetailModel.productInfo.show_price));
                    txt_cost_of_production.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //中划线
                    //规格数等于1，显示价格
                    productPrice.setText(getString(R.string.RMB) + productDetailModel.productInfo.price_interval);

                    //需要积分时显示积分数量
                    if (productDetailModel.productInfo.need_point > 0 || TongleAppConst.PRODUCT_TYPE_SPECIAL_CARE.equals(productDetailModel.productInfo.goods_type)) {
                        txt_need_pay_point.setVisibility(View.VISIBLE);
                        txt_need_pay_point.setText("+ " + productDetailModel.productInfo.need_point + "积分");
                        add_to_cart_btn.setVisibility(View.INVISIBLE);
                    } else {
                        txt_need_pay_point.setVisibility(View.GONE);
                        add_to_cart_btn.setVisibility(View.VISIBLE);
                    }
                    //Log.i("stock_list",productDetailModel.productInfo.stock_list.toString());
                    //获取库存数量
                    prod_stock_qty.setText(getSumStock(productDetailModel.productInfo.stock_list) + "");
                    prod_service.setText(productDetailModel.productInfo.sender);
                    prod_notify.setText(productDetailModel.productInfo.goods_return_info);

                    if (!TextUtils.isEmpty(collectType) && productDetailModel.productInfo.trade_prod_favorited_by_me.equals("0")) {//我的宝贝进入，并且我的宝贝收藏了
                        //0已收藏
                        collect_btn.setBackgroundResource(R.drawable.discollect_button_style);
                        bIsFavorate = true;
                    } else if (productDetailModel.productInfo.prod_favorited_by_me.equals("0")) {//商品已收藏
                        collect_btn.setBackgroundResource(R.drawable.discollect_button_style);
                        bIsFavorate = true;
                    }
                    createSpecView();

                    if (Integer.parseInt(productDetailModel.productInfo.buy_limit_qty) == 0) {//不限购
                        mTxtLimit.setVisibility(View.GONE);
                    } else {//显示限购数量
                        mTxtLimit.setVisibility(View.VISIBLE);
                        mTxtLimit.setText(String.format(getString(R.string.limit_buy), productDetailModel.productInfo.buy_limit_qty));
                    }

                } else {
                    scroll_view.setVisibility(View.GONE);
                    collect_btn.setVisibility(View.GONE);
                    QR_share_btn.setVisibility(View.GONE);
                    tv_replace.setVisibility(View.VISIBLE);
                    tv_replace.setText(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                pDlg.dismiss();
            }
        };
    }

    /**
     * 功能：请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                buyNowEnableFlag = true;
                ToastUtils.toastLong(getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }

    public void showFavDlg(View v) {
        View view = LayoutInflater.from(CollectProductDetailActivity.this).inflate(R.layout.prod_collect_layout, null);
        // 创建PopupWindow实例
        popupWindow = new PopupWindow(view, WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT, false);
        //设置可以获取焦点
        popupWindow.setFocusable(true);
        //防止弹出菜单获取焦点之后，点击activity的其他组件没有响应
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //防止虚拟软键盘被弹出菜单遮住
        popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //在底部显示
        popupWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);

        // 点击其他地方消失
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                    popupWindow = null;
                }
                return false;
            }
        });

        //弹出dialog里面的控件
        cb[0] = (CheckBox) view.findViewById(R.id.my_favorite);
        cb[1] = (CheckBox) view.findViewById(R.id.my_product);
        cancle = (Button) view.findViewById(R.id.cancle);
        collect = (Button) view.findViewById(R.id.collect);
        //复选框点击以及确定取消点击请求的数据
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();

            }
        });
        collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<FavoriteListItem> favList = new ArrayList<FavoriteListItem>();
                for (int i = 0; i < 2; i++) {
                    if (cb[i].isChecked()) {
                        FavoriteListItem item = new FavoriteListItem();
                        if (i == 0) {
                            item.collection_type = TongleAppConst.PRODUCT;// 0 商品
                        } else {
                            item.collection_type = TongleAppConst.MYPRODUCT;// 4 宝贝
                        }
                        item.key_value = prodID;
                        favList.add(item);
                    }
                }
                if (favList.isEmpty())
                    ToastUtils.toastShort(getString(R.string.no_favorite_select));
                else {
                    ServiceManager.doMStoreCollectRequest(TongleAppInstance.getInstance().getUserID(), favList, productDetailModel.productInfo.relation_id, FavListener(), errorListener());
                }
            }
        });
    }

    /**
     * 功能：购物车结算网络响应成功，返回数据
     */
    private Response.Listener<CartSettlementResult> CartSuccessListener() {
        return new Response.Listener<CartSettlementResult>() {
            @Override
            public void onResponse(CartSettlementResult response) {
                buyNowEnableFlag = true;
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {

                    CartModel.checkList = response.getBody().my_shopping_cart;
                    OrderConfirmActivity.launcher(CollectProductDetailActivity.this,response,OrderConfirmActivity.TYPE_BUYNOW,productDetailModel.productInfo.goods_type);

                    //重新加载数据，刷新界面
                } else {
                    ToastUtils.toastLong(response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：取消收藏请求网络响应成功，返回数据
     */
    private Response.Listener collectListener() {
        return new Response.Listener<MStoreDetailCollectResult>() {
            @Override
            public void onResponse(MStoreDetailCollectResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    collect_btn.setBackgroundResource(R.drawable.collect_button_style);
                    ToastUtils.toastShort(getString(R.string.favorite_cancel));
                    bIsFavorate = false;
                    if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                        MProductFragment.UPDATE_LIST_FLAG = true;  //从收藏页进来的，表示刚刚取消了收藏。回收藏页时需要刷新
                    } else if (intoType == TongleAppConst.MYBABY_INTO) {
                        SettingMyBoxBabyActivity.UPDATE_LIST_FLAG = true;  //从我的宝贝页面进入，表示刚刚取消了收藏。回宝贝列表页时需刷新
                    } else {
                        MProductFragment.MPRODUCTFRAGMENT_UPDATE_FLAG = true;   //如果是从发现等页面进入，重新进入收藏页面时必须更新M品列表
                    }
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：收藏网络响应成功，返回数据
     */
    private Response.Listener FavListener() {
        return new Response.Listener<MStoreDetailCollectResult>() {
            @Override
            public void onResponse(MStoreDetailCollectResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    collect_btn.setBackgroundResource(R.drawable.discollect_button_style);
                    ToastUtils.toastShort(getString(R.string.favorite_success));
                    bIsFavorate = true;
                    if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                        MProductFragment.UPDATE_LIST_FLAG = false;  //从收藏页进来的，表示刚刚取消了收藏之后又重新收藏。回收藏页时不需要刷新
                    } else if (intoType == TongleAppConst.MYBABY_INTO) {
                        SettingMyBoxBabyActivity.UPDATE_LIST_FLAG = false;  //从我的宝贝页面进入，表示刚刚取消了收藏之后又重新收藏。回宝贝列表页时不需刷新
                    } else {
                        MProductFragment.MPRODUCTFRAGMENT_UPDATE_FLAG = true;   //如果是从发现等页面进入，重新进入收藏页面时必须更新M品列表
                    }
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                popupWindow.dismiss();
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (pDlg != null) {
            pDlg.dismiss();
        }
        MobclickAgent.onPageStart("productDetail");
        MobclickAgent.onResume(getApplicationContext());
    }
}



