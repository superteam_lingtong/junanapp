package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.activity.SettingMyFieldSubActivity;
import com.ilingtong.library.tongle.model.MyFieldModel;
import com.ilingtong.library.tongle.protocol.FieldTypeInfo;
import com.ilingtong.library.tongle.protocol.MyFieldTypeListInfo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * User: shuailei
 * Date: 2015/6/19
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:  领域界面适配器
 */
public class MyFieldListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<MyFieldTypeListInfo> list;
    MyFieldTypeListInfo dataItem = new MyFieldTypeListInfo();
    private Context mContext;
    public MyFieldListAdapter(Context context, ArrayList list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            //如果不存在view则初始化
            view = inflater.inflate(R.layout.list_item_mfield, null);
            holder = new ViewHolder();
            holder.mfieldName = (TextView) view.findViewById(R.id.mfield_name);
            holder.mfield_imgbtn = (ImageButton) view.findViewById(R.id.mfield_imgbtn);
            holder.mfield_choose = (ImageView) view.findViewById(R.id.mfield_choose);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        dataItem = list.get(position);
        holder.mfieldName.setText((CharSequence) dataItem.getType_name());
        if(dataItem.getSub_list().size()>0) {
            //有子选项，点击跳转到子选项
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList arrlist = list.get(position).getSub_list();
                    Intent intent = new Intent(mContext, SettingMyFieldSubActivity.class);
                    intent.putCharSequenceArrayListExtra("arrlist", arrlist);
                    intent.putExtra("name", list.get(position).getType_name());
                    mContext.startActivity(intent);
                }
            });
        }else {
            //没有子选项，右侧箭头消失，做是否选中的图标操作并添加到sava_list
            holder.mfield_imgbtn.setVisibility(View.GONE);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //点击事件，首先初始化，得到当前选中的view的id和name
                    FieldTypeInfo typeInfo = new FieldTypeInfo();
                    typeInfo.type_id = list.get(position).getType_id();
                    typeInfo.type_name = list.get(position).getType_name();
                    if ( holder.mfield_choose.getVisibility() == View.VISIBLE ) {
                        //选中状态下的点击操作
                        holder.mfield_choose.setVisibility(View.GONE);
                        Iterator iter = MyFieldModel.checkList.iterator();
                        while (iter.hasNext()) {
                            FieldTypeInfo item = new FieldTypeInfo();
                            item = (FieldTypeInfo)iter.next();
                            if(item.type_id.equals(typeInfo.type_id) && item.type_name.equals(typeInfo.type_name)) {
                                iter.remove();
                            }
                        }
                    } else {
                        //未选中状态下的点击操作
                        holder.mfield_choose.setVisibility(View.VISIBLE);
                        MyFieldModel.checkList.add(typeInfo);
                    }
                }
            });
        }

        //二次进入时，根据选中状态判断右侧图标状态
        if(MyFieldModel.checkList != null) {
            for (int i = 0; i < MyFieldModel.checkList.size(); i++) {
                if (MyFieldModel.checkList.get(i).type_id.equals(list.get(position).getType_id())) {
                    holder.mfield_choose.setVisibility(View.VISIBLE);
                }
            }
        }
        return view;
    }

    static class ViewHolder {
        TextView mfieldName;
        ImageButton mfield_imgbtn;
        ImageView mfield_choose;
    }
}
