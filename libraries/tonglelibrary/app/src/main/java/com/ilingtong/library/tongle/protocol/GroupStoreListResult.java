package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:6008接口返回json 对应entity
 */
public class GroupStoreListResult extends BaseResult implements Serializable {
   private GroupStoreInfo body;

    public GroupStoreInfo getBody() {
        return body;
    }

    public void setBody(GroupStoreInfo body) {
        this.body = body;
    }
}
