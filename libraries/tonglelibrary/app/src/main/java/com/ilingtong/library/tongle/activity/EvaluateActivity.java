package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.CommentResult;
import com.ilingtong.library.tongle.protocol.OrderDetailEvaluteInfo;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * Created by wuqian on 2016/4/13.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class EvaluateActivity extends BaseActivity implements View.OnClickListener {
    private RatingBar ratingBar;  //评分
    private EditText evaluateorderEdit;  //评价内容
    private Button evaluateorderCancle;  //取消评价按钮
    private Button evaluateorderSure;    //确认评价按钮
    private String selectRating;   //分数
    private String evaluateContent;  //评价内容
    private OrderDetailEvaluteInfo orderDetailEvaluteInfo = new OrderDetailEvaluteInfo();

    /**
     * @param activity
     * @param orderNo         订单编号
     * @param order_detail_no 订单明细编号
     * @param product_id      商品编号
     */
    public static void luanchForResult(Fragment activity, String orderNo, String order_detail_no, String product_id, int requestCode) {
        Intent intent = new Intent(activity.getActivity(), EvaluateActivity.class);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("order_detail_no", order_detail_no);
        intent.putExtra("product_id", product_id);
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * @param activity
     * @param orderNo         订单编号
     * @param order_detail_no 订单明细编号
     * @param product_id      商品编号
     */
    public static void luanchForResult(Activity activity, String orderNo, String order_detail_no, String product_id, int requestCode) {
        Intent intent = new Intent(activity, EvaluateActivity.class);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("order_detail_no", order_detail_no);
        intent.putExtra("product_id", product_id);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.orderdetail_evaluate_layout);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);
        getData();
        init();
    }

    private void getData() {
        orderDetailEvaluteInfo.setOrder_no(getIntent().getStringExtra("orderNo"));
        orderDetailEvaluteInfo.setOrder_detail_no(getIntent().getStringExtra("order_detail_no"));
        orderDetailEvaluteInfo.setProduct_id(getIntent().getStringExtra("product_id"));
    }

    private void init() {
        evaluateorderEdit = (EditText) findViewById(R.id.evaluateorder_edit);
        evaluateorderCancle = (Button) findViewById(R.id.evaluateorder_cancle);
        evaluateorderSure = (Button) findViewById(R.id.evaluateorder_sure);
        evaluateorderCancle.setOnClickListener(this);
        evaluateorderSure.setOnClickListener(this);

        ratingBar = (RatingBar) findViewById(R.id.evaluate_ratingbar_level);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                selectRating = String.valueOf(ratingBar.getRating());
                Log.e("tag", "starNum" + rating);
                Log.e("tag", "selectRating" + selectRating);
            }
        });

    }

    @Override
    public void onClick(View v) {

//        switch (v.getId()){
//            case R.id.evaluateorder_cancle:
//                this.finish();
//                break;
//            case R.id.evaluateorder_sure:
//                evaluateContent = evaluateorderEdit.getText().toString();
//                if (TextUtils.isEmpty(selectRating)){
//                    ToastUtils.toastShort(getString(R.string.adapter_order_detail_evaluate_list_rating_null));
//                }else if (TextUtils.isEmpty(evaluateContent)){
//                    ToastUtils.toastShort(getString(R.string.adapter_order_detail_evaluate_list_evaluate_null));
//                }else {
//                    orderDetailEvaluteInfo.setLevel(selectRating);
//                    orderDetailEvaluteInfo.setMemo(evaluateContent);
//                    ArrayList<OrderDetailEvaluteInfo> commentList = new ArrayList<>();
//                    commentList.add(orderDetailEvaluteInfo);
//                    ServiceManager.doOrderDetailEvaluateRequest(TongleAppInstance.getInstance().getUserID(), commentList, successEvaluateListener(), errorListener());
//                }
//                break;
//        }

        if (v.getId() == R.id.evaluateorder_cancle) {
            this.finish();
        } else if (v.getId() == R.id.evaluateorder_sure) {
            evaluateContent = evaluateorderEdit.getText().toString();
            if (TextUtils.isEmpty(selectRating)) {
                ToastUtils.toastShort(getString(R.string.adapter_order_detail_evaluate_list_rating_null));
            } else if (TextUtils.isEmpty(evaluateContent)) {
                ToastUtils.toastShort(getString(R.string.adapter_order_detail_evaluate_list_evaluate_null));
            } else {
                orderDetailEvaluteInfo.setLevel(selectRating);
                orderDetailEvaluteInfo.setMemo(evaluateContent);
                ArrayList<OrderDetailEvaluteInfo> commentList = new ArrayList<>();
                commentList.add(orderDetailEvaluteInfo);
                ServiceManager.doOrderDetailEvaluateRequest(TongleAppInstance.getInstance().getUserID(), commentList, successEvaluateListener(), errorListener());
            }
        }
    }

    /**
     * 功能：订单详情网络响应成功，返回数据
     */
    private Response.Listener successEvaluateListener() {
        return new Response.Listener<CommentResult>() {
            @Override
            public void onResponse(CommentResult response) {
                if (response.getHead().getReturn_flag().equals(TongleAppConst.SUCCESS)) {
                    ToastUtils.toastShort(getString(R.string.common_evaluate_success));
                    setResult(RESULT_OK);
                    EvaluateActivity.this.finish();
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + response.getHead().getReturn_message());

                }
            }
        };
    }

    /**
     * 功能：订单详情请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(R.string.sys_exception) + volleyError.toString());
            }
        };
    }
}
