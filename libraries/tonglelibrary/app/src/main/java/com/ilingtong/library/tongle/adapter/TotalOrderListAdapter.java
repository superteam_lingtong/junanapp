package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.OrderDetailActivity;
import com.ilingtong.library.tongle.activity.OrderDetailProdEvaluateActivity;
import com.ilingtong.library.tongle.activity.ProdIntegralActivity;
import com.ilingtong.library.tongle.activity.SelectPayTypeActivity;
import com.ilingtong.library.tongle.external.MyListView;
import com.ilingtong.library.tongle.fragment.OrderListFragment;
import com.ilingtong.library.tongle.protocol.OrderDetailResult;
import com.ilingtong.library.tongle.protocol.OrderListItemData;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * User: syc
 * Date: 2015/6/10
 * Time: 10:45
 * Email: ycshi@isoftstone.com
 * Dest:全部/待付款/待收货/待评价 订单列表的adapter
 */
public class TotalOrderListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<OrderListItemData> list = new ArrayList<>();
    //    private TextView order_date;
//    private TextView order_count;
//    private TextView total_pri;
//    private TextView order_fee;
//    private Button desc;
//    private Button pay;
//    private View bootom;
//    private MyListView lv;
    //private ArrayList<ProdDetailListItem> prodlist;
    private OrderFormListItemListAdapter adapter;
    Fragment adapterContext;
    private int operatePosition;
    private Handler handler;

    public int getOperatePosition() {
        return operatePosition;
    }

    public TotalOrderListAdapter(Fragment context, List<OrderListItemData> orderlist, Handler handler) {
        this.inflater = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = orderlist;
        this.handler = handler;
        adapterContext = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.fragment_orderform_item_layout, null);
            holder.orderformItemLv = (MyListView) convertView.findViewById(R.id.orderform_item_lv);
            holder.orderDate = (TextView) convertView.findViewById(R.id.order_date);
            holder.orderCount = (TextView) convertView.findViewById(R.id.order_count);
            holder.orderFee = (TextView) convertView.findViewById(R.id.order_fee);
            holder.totalPri = (TextView) convertView.findViewById(R.id.total_pri);
            holder.orderDet = (Button) convertView.findViewById(R.id.order_det);
            holder.orderPay = (Button) convertView.findViewById(R.id.order_pay);
            holder.orderAfterSale = (Button) convertView.findViewById(R.id.order_after_sale);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.orderDate.setText(list.get(position).order_time);
        int productCountSum = 0;
        for (int i = 0; i < list.get(position).prod_detail.size(); i++) {
            productCountSum += Integer.parseInt(list.get(position).prod_detail.get(i).quantity);
        }
        holder.orderCount.setText(String.format(adapterContext.getString(R.string.adapter_total_order_list_count), productCountSum));

        if (Double.parseDouble(list.get(position).fee_amount) < 0) {
            holder.orderFee.setText(adapterContext.getString(R.string.common_order_fee_transportation) + list.get(position).fee_amount);
        } else {
            holder.orderFee.setVisibility(View.GONE);
        }
//        holder.totalPri.setText(adapterContext.getString(R.string.RMB) + list.get(position).amount);
        holder.totalPri.setText(FontUtils.getMoneyType(list.get(position).amount));
        adapter = new OrderFormListItemListAdapter(adapterContext.getActivity(), list.get(position).prod_detail);
        holder.orderformItemLv.setAdapter(adapter);
        /**********************订单详情***************************/
        holder.orderDet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operatePosition = position;
                Intent intent = new Intent(adapterContext.getActivity(), OrderDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("order_no", list.get(position).order_no);
                intent.putExtras(bundle);
                adapterContext.startActivityForResult(intent, OrderListFragment.REQUESTCODE_TO_DETALI);
            }
        });
        /******************************立即支付/确认收货/立即评价按钮处理****************/
        switch (list.get(position).status) {
            case TongleAppConst.ORDER_NO_PAY:    //订单状态：待付款。按钮显示“立即支付",点击调起支付宝或者微信
                holder.orderPay.setText(adapterContext.getString(R.string.common_order_pay_now));
                holder.orderPay.setVisibility(View.VISIBLE);
                holder.orderPay.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                operatePosition = position;
                                SelectPayTypeActivity.launcherForResult(adapterContext.getActivity(), list.get(position).order_no, TongleAppConst.PAY_MODE, OrderListFragment.REQUESTCODE_TO_PAY);
                            }
                        }
                );
                break;
            case TongleAppConst.ORDER_SENDING:  //订单状态：待收货。按钮显示“确认收货".
                holder.orderPay.setText(adapterContext.getString(R.string.common_order_confirm_receive));
                holder.orderPay.setVisibility(View.VISIBLE);
                holder.orderPay.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ServiceManager.OrderConfirmRequest(TongleAppInstance.getInstance().getUserID(), list.get(position).order_no, confirmsuccessListener(), errorListener());
                                operatePosition = position;
                            }
                        }
                );
                break;
            case TongleAppConst.ORDER_NO_COMMENTS:  //订单状态：待评价。按钮显示“立即评价”
                holder.orderPay.setText(adapterContext.getString(R.string.common_order_evaluate_txt));
                holder.orderPay.setVisibility(View.VISIBLE);
                holder.orderPay.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                operatePosition = position;
                                Intent i = new Intent(adapterContext.getActivity(), OrderDetailProdEvaluateActivity.class);
                                i.putExtra("order_no", list.get(position).order_no);
                                i.putExtra("MyOrderActivity", "yes");
                                adapterContext.startActivityForResult(i, OrderListFragment.REQUESTCODE_TO_EVALUATE);
                            }
                        }
                );
                break;
            default:
                holder.orderPay.setVisibility(View.GONE);
                break;
        }

        if (TongleAppConst.ORDER_NO_COMMENTS.equals(list.get(position).status) || TongleAppConst.ORDER_HAVE_COMMENTS.equals(list.get(position).status)){
            holder.orderAfterSale.setVisibility(View.VISIBLE);
            holder.orderAfterSale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //拼接url
                    String finalUrl = list.get(position).goods_return_url + "?user_id=" + TongleAppInstance.getInstance().getUserID() + "&user_token=" + TongleAppInstance.getInstance().getToken() + "&order_no=" + list.get(position).order_no+"&app_inner_no="+TongleAppInstance.getInstance().getApp_inner_no();
                    Intent expertIntent = new Intent(adapterContext.getContext(), ProdIntegralActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("prod_url", finalUrl);
                    bundle.putString("myTitle", list.get(position).goods_return_title);
                    expertIntent.putExtras(bundle);
                    adapterContext.getActivity().startActivity(expertIntent);
                }
            });
        }else {
            holder.orderAfterSale.setVisibility(View.GONE);
        }
//        OrderListItemData itemData = list.get(position);
//        convertView = inflater.inflate(R.layout.fragment_orderform_item_layout, null);
//        lv = (MyListView) convertView.findViewById(R.id.orderform_item_lv);
//        bootom = convertView.findViewById(R.id.bottom);
//        order_date = (TextView) bootom.findViewById(R.id.order_date);
//        order_count = (TextView) bootom.findViewById(R.id.order_count);
//        total_pri = (TextView) bootom.findViewById(R.id.total_pri);
//        order_fee = (TextView) bootom.findViewById(R.id.order_fee);
//        desc = (Button) bootom.findViewById(R.id.order_det);  //订单详情
//        int sum = 0;
//        for (int i = 0; i < itemData.prod_detail.size(); i++) {
//            sum = Integer.parseInt(itemData.prod_detail.get(i).quantity) + sum;
//        }
//        order_count.setText(String.format(adapterContext.getString(R.string.adapter_total_order_list_count), sum));
//        try {
//            if (Integer.parseInt(itemData.fee_amount) != 0) {
//                order_fee.setText(adapterContext.getString(R.string.common_order_fee_transportation) + itemData.fee_amount);
//            } else {
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        order_date.setText(itemData.order_time);
//        total_pri.setText(adapterContext.getString(R.string.RMB) + itemData.amount);
//
//        prodlist = itemData.prod_detail;
//        adapter = new OrderFormListItemListAdapter(adapterContext, prodlist);
//
//        setBottomButton(itemData, position);
//        //订单详情
//        desc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(adapterContext, OrderDetailActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("order_no", list.get(position).order_no);
//                intent.putExtras(bundle);
//                adapterContext.startActivity(intent);
//            }
//        });
//        lv.setAdapter(adapter);
        return convertView;
    }

    /**
     * boom 按钮设置
     */
//    public void setBottomButton(final OrderListItemData itemData, final int position) {
//        desc = (Button) bootom.findViewById(R.id.order_det);
//        pay = (Button) bootom.findViewById(R.id.order_pay);
//        switch (itemData.status) {
//            case TongleAppConst.ORDER_NO_PAY:    //订单状态：待付款。按钮显示“立即支付",点击调起支付宝或者微信
//                pay.setText(adapterContext.getString(R.string.common_order_pay_now));
//                pay.setOnClickListener(
//                        new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                SelectPayTypeActivity.launcher(adapterContext, list.get(position).order_no, TongleAppConst.PAY_MODE);
//                            }
//                        }
//                );
//                break;
//            case TongleAppConst.ORDER_SENDING:
//                pay.setText(adapterContext.getString(R.string.common_order_confirm_receive));
//                pay.setOnClickListener(
//                        new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                ServiceManager.OrderConfirmRequest(TongleAppInstance.getInstance().getUserID(), itemData.order_no, confirmsuccessListener(), errorListener());
//                                operatePosition = position;
//                            }
//                        }
//                );
//                break;
//            case TongleAppConst.ORDER_NO_COMMENTS:
//                pay.setText(adapterContext.getString(R.string.common_order_evaluate_txt));
//                pay.setOnClickListener(
//                        new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                Intent i = new Intent(adapterContext, OrderDetailProdEvaluateActivity.class);
//                                i.putExtra("order_no", itemData.order_no);
//                                i.putExtra("MyOrderActivity", "yes");
//                                adapterContext.startActivity(i);
//                            }
//                        }
//                );
//                break;
//            default:
//                pay.setVisibility(View.GONE);
//        }
//    }


    /**
     * 功能：确认收货网络响应成功，返回数据
     */
    private Response.Listener confirmsuccessListener() {
        return new Response.Listener<OrderDetailResult>() {
            @Override
            public void onResponse(OrderDetailResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastLong(adapterContext.getString(R.string.common_order_confirm_receive_success));
                    Message message = new Message();
                    message.arg1 = operatePosition;
                    message.what = OrderListFragment.CONFIRM_RECEIVE_SUCCESS;
                    handler.sendMessage(message);
                } else {
                    ToastUtils.toastLong(adapterContext.getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(adapterContext.getString(R.string.sys_exception) + volleyError.toString());
            }
        };
    }

    class ViewHolder {
        private MyListView orderformItemLv;  //商品列表
        private TextView orderDate;  //订单日期
        private TextView orderCount;  //商品数量
        private TextView orderFee;   //运费
        private TextView totalPri;    //订单金额
        private Button orderDet;    //订单详情按钮
        private Button orderPay;    //立即支付/确认收货/立即评价 按钮
        private Button orderAfterSale;   //退货返修按钮
    }
}
