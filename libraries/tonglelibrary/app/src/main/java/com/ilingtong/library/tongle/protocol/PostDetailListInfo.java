package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/18
 * Time: 22:05
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class PostDetailListInfo implements Serializable {

    private UserPostInfo user_post_info;

    public UserPostInfo getUser_post_info() {
        return user_post_info;
    }

    @Override
    public String toString() {
        return  "user_post_info:" + user_post_info;
    }
}
