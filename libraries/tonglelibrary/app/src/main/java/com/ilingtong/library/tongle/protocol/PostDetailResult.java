package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/18
 * Time: 22:05
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class PostDetailResult implements Serializable {
    private BaseInfo head;
    private PostDetailListInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public PostDetailListInfo getBody() {
        return body;
    }

    public void setBody(PostDetailListInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
