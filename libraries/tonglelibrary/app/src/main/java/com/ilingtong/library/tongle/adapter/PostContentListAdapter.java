package com.ilingtong.library.tongle.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.CollectExpertDetailActivity;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.activity.CollectMStoreDetailActivity;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.activity.ProductTicketDetailActivity;
import com.ilingtong.library.tongle.protocol.PostContent;
import com.ilingtong.library.tongle.protocol.UserPostInfo;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/5/20
 * Time: 16:39
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class PostContentListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private UserPostInfo postInfo = new UserPostInfo();
    private Activity context;
    public ViewHolder holder;

    public PostContentListAdapter(Activity context, UserPostInfo postInfo, List<Bitmap> bitmaplist) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.postInfo = postInfo;
        this.context = context;
    }


    @Override
    public int getCount() {
        return postInfo.post_content.size();
    }

    @Override
    public Object getItem(int position) {
        return postInfo.post_content.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = inflater.inflate(R.layout.list_item_post_content, null);
            holder = new ViewHolder();
            holder.listItemImageView = (ImageView) view.findViewById(R.id.pic_url);
            holder.pic_memo = (TextView) view.findViewById(R.id.pic_memo);
            holder.img_play_icon = (ImageView) view.findViewById(R.id.post_content_img_cover_play);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        //设置是否覆盖显示播放按钮
        holder.img_play_icon.setVisibility(TongleAppConst.ACTIVITY_BROWSER_VIDEO.equals(postInfo.post_content.get(position).related_type) ? View.VISIBLE : View.GONE);
        PostContent dataItem = postInfo.post_content.get(position);
        //如果图片没有文字说明，隐藏textView
        if (TextUtils.isEmpty(dataItem.pic_memo)) {
            holder.pic_memo.setVisibility(View.GONE);
        } else {
            holder.pic_memo.setVisibility(View.VISIBLE);
            holder.pic_memo.setText(dataItem.pic_memo);
        }
        //如果图片地址为空，隐藏imageView
        holder.listItemImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        if (TextUtils.isEmpty(postInfo.post_thumbnail_pic_url.get(position).pic_url)) {
            holder.listItemImageView.setVisibility(View.GONE);
        } else {
            holder.listItemImageView.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(postInfo.post_thumbnail_pic_url.get(position).pic_url, holder.listItemImageView, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bm) {
                    float es = (float) (DipUtils.getScreenWidth(context) - DipUtils.dip2px(context, 12 * 2)) / (float) bm.getWidth();
                    int height = (int) (bm.getHeight() * es);
                    ViewGroup.LayoutParams params = view.getLayoutParams();
                    params.height = height;
                    view.setLayoutParams(params);
                }
            });
        }
        holder.listItemImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //关联类型是商品
                if (postInfo.post_content.get(position).related_type.equals(TongleAppConst.ACTIVITY_PRODUCT)) {
                    if (TongleAppConst.YES.equals(postInfo.post_content.get(position).coupon_flag)) {
                        ProductTicketDetailActivity.launch(context, postInfo.post_content.get(position).object_id, TongleAppConst.ACTIONID_POST, postInfo.post_content.get(position).relation_id, "", postInfo.post_id);
                    } else {
                        CollectProductDetailActivity.launch(context, postInfo.post_content.get(position).object_id, TongleAppConst.ACTIONID_POST, postInfo.post_content.get(position).relation_id, "", postInfo.post_id);
                    }
                    //关联类型是会员
                } else if (postInfo.post_content.get(position).related_type.equals(TongleAppConst.ACTIVITY_MEMBER)) {
                    Intent i = new Intent(context, CollectExpertDetailActivity.class);
                    i.putExtra("user_id", postInfo.post_content.get(position).object_id);
                    context.startActivity(i);
                    //关联类型是魔店
                } else if (postInfo.post_content.get(position).related_type.equals(TongleAppConst.ACTIVITY_MSTORE)) {
                    Intent i = new Intent(context, CollectMStoreDetailActivity.class);
                    i.putExtra("mstore_id", postInfo.post_content.get(position).object_id);
                    context.startActivity(i);
                    //关联类型是帖子
                } else if (postInfo.post_content.get(position).related_type.equals(TongleAppConst.ACTIVITY_FORUM)) {
                    Intent i = new Intent(context, CollectForumDetailActivity.class);
                    i.putExtra("post_id", postInfo.post_content.get(position).object_id);
                    context.startActivity(i);
                } else if (postInfo.post_content.get(position).related_type.equals(TongleAppConst.ACTIVITY_BROWSER) || postInfo.post_content.get(position).related_type.equals(TongleAppConst.ACTIVITY_BROWSER_VIDEO)) {
                    //关联类型是外部网址链接
                    Uri uri = Uri.parse(postInfo.post_content.get(position).link_url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    context.startActivity(intent);
                }

            }
        });

        return view;
    }

    static class ViewHolder {
        TextView pic_memo;
        ImageView listItemImageView;
        ImageView img_play_icon;   //播放按钮背景
    }
}
