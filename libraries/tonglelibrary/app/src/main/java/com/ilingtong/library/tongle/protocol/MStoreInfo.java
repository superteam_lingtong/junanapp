package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/2
 * Time: 17:54
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class MStoreInfo implements Serializable {
    private String data_total_count;
    private ArrayList<MStoreListItem> mstore_list;

    public String getData_total_count() {
        return data_total_count;
    }

    public ArrayList<MStoreListItem> getMStoreList() {
        return mstore_list;
    }

    @Override
    public String toString() {
        return "data_total_count:" + data_total_count + "\r\n" +
                "mstore_list:" + mstore_list;
    }
}
