package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/3/18
 * Time: 11:03
 * Email: liuting@ilingtong.com
 * Desc我的可用优惠券实体类
 */
public class MyCouponAvailableBaseInfo implements Serializable {
    private MyCouponBaseInfo voucher_base;  //我的可用优惠券

    public MyCouponBaseInfo getVoucher_base() {
        return voucher_base;
    }

    public void setVoucher_base(MyCouponBaseInfo voucher_base) {
        this.voucher_base = voucher_base;
    }
}
