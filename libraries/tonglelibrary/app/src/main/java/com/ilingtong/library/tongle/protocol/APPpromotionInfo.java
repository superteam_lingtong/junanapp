package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: syc
 * Date: 2015/6/26
 * Time: 14:18
 * Email: ycshi@isoftstone.com
 * Desc: 【2015】
 */
public class APPpromotionInfo implements Serializable {

    private String app_qr_code_url;
    private String wx_pic_url;
    private String wx_title;
    private String wx_memo;
    private String wx_link_url;

    public String getWx_pic_url() {
        return wx_pic_url;
    }

    public String getWx_link_url() {
        return wx_link_url;
    }

    public String getWx_memo() {
        return wx_memo;
    }

    public String getWx_title() {
        return wx_title;
    }

    public String getApp_qr_code_url() {
        return app_qr_code_url;
    }


    @Override
    public String toString() {
        return "app_qr_code_url:" + app_qr_code_url;
    }
}
