package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:6010接口返回json 对应entity
 */
public class CouponListResult extends BaseResult implements Serializable{
    private CouponListInfo body;

    public CouponListInfo getBody() {
        return body;
    }

    public void setBody(CouponListInfo body) {
        this.body = body;
    }
}
