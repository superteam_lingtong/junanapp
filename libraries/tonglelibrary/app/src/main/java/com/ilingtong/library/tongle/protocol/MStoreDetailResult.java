package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 14:49
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class MStoreDetailResult implements Serializable {
    private BaseInfo head;
    private MStoreDetailInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public MStoreDetailInfo getBody() {
        return body;
    }

    public void setBody(MStoreDetailInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
