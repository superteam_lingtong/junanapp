//package com.ilingtong.library.tongle.protocol;
//
//import java.io.Serializable;
//
///**
// * User: syc
// * Date: 2015/8/9
// * Time: 13:01
// * Email: ycshi@isoftstone.com
// * Desc: 支付宝支付
// */
//public class AliPayInfo implements Serializable {
//
//    public String service;
//    public String partner;
//    public String input_charset;
//    public String sign_type;
//    public String sign;
//    public String notify_url;
//    public String app_id;
//    public String out_trade_no;
//    public String subject;
//    public String payment_type;
//    public String seller_id;
//    public String total_fee;
//    public String body;
//    public String it_b_pay;
//    public String extern_token;
//
//
//    @Override
//    public String toString() {
//        return "service="+ "\"" + service + "\""+ "&" +
//                "partner=" + "\""+ partner + "\""+ "&" +
//                "input_charset="+ "\"" + input_charset+ "\"" + "&" +
//                "sign_type="+ "\"" + sign_type + "\""+ "&" +
//                "sign=" + "\""+ sign+ "\"" + "&" +
//                "notify_url="+ "\"" + notify_url+ "\"" + "&" +
//                "app_id=" + "\""+ app_id+ "\"" + "&" +
//                "out_trade_no="+ "\"" + out_trade_no+ "\"" +
//                "subject="+ "\"" + subject+ "\"" + "&" +
//                "payment_type="+ "\"" + payment_type+ "\"" + "&" +
//                "seller_id="+ "\"" + seller_id+ "\"" + "&" +
//                "total_fee="+ "\"" + total_fee + "\""+ "&" +
//                "body="+ "\"" + body+ "\"" + "&" +
//                "it_b_pay="+ "\"" + it_b_pay+ "\"" + "&" +
//                "extern_token="+ "\"" + extern_token+ "\"";
//    }
//}
