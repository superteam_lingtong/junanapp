package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/17
 * Time: 14:41
 * Email: jqleng@isoftstone.com
 * Desc:user by 2040
 */
public class CodeBaseResult implements Serializable {
    private BaseInfo head;
    private CodeUserData body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public CodeUserData getBody() {
        return body;
    }

    public void setBody(CodeUserData body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
