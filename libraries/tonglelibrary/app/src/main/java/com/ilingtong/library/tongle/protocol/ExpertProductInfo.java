package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 9:52
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ExpertProductInfo implements Serializable {
    private String data_total_count;
    private ArrayList<ProductListItemData> my_prod_list;

    public String getData_total_count() {
        return data_total_count;
    }

    public ArrayList<ProductListItemData> getMy_prod_list() {
        return my_prod_list;
    }

    @Override
    public String toString() {
        return "data_total_count:" + data_total_count + "\r\n" +
                "my_prod_list:" + my_prod_list;
    }
}
