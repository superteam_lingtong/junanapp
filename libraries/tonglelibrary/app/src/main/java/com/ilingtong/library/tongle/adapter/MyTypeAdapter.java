package com.ilingtong.library.tongle.adapter;

import android.content.Intent;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.LoginActivity;
import com.ilingtong.library.tongle.protocol.BaseInfo;

import java.io.IOException;

/**
 * Created by wuqian on 2016/1/25.
 * mail: wuqian@ilingtong.com
 * Description: json解析拦截。
 */
public class MyTypeAdapter extends TypeAdapter {
    @Override
    public void write(JsonWriter out, Object value) throws IOException {

    }

    @Override
    public Object read(JsonReader reader) throws IOException {
        BaseInfo baseInfo = new BaseInfo();
        reader.beginObject();
        while (reader.hasNext()){
            String name = reader.nextName();
            if (name.equals("function_id")){
                baseInfo.setFunction_id(reader.nextString());
            }else if (name.equals("return_flag")){
                baseInfo.setReturn_flag(reader.nextString());
            }else if (name.equals("return_message")){
                baseInfo.setReturn_message(reader.nextString());
            }else {
                reader.skipValue();
            }
        }
        reader.endObject();

        if (baseInfo.getReturn_flag().equals("2")){
            Intent intent = new Intent(TongleAppInstance.getAppContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_NAME,TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_MAIN);
            TongleAppInstance.getAppContext().startActivity(intent);
        }
        return baseInfo;
    }
}
