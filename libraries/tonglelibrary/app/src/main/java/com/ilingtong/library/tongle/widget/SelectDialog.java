package com.ilingtong.library.tongle.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.utils.DipUtils;

/**
 * Package:com.ilingtong.library.tongle.widget
 * author:liuting
 * Date:2016/12/27
 * Desc:选择dialog，主要用于权限申请提示dialog
 */

public class SelectDialog extends Dialog {
    private TextView txtMsg;//提示文本
    private Button btnSure;//确定按钮
    private String strMessage;//提示内容
    private View.OnClickListener clickListener;//确定按钮监听事件

    public SelectDialog(Context context) {
        super(context);
    }

    public SelectDialog(Context context,String message) {
        super(context, R.style.select_dialog_style);
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.CENTER;
        params.width= DipUtils.px2dip(280);
        window.setAttributes(params);
        this.strMessage=message;
    }

    public SelectDialog(Context context,String message,View.OnClickListener clickListener) {
        super(context, R.style.select_dialog_style);
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.CENTER;
        params.width= DipUtils.px2dip(280);
        window.setAttributes(params);
        this.strMessage=message;
        this.clickListener=clickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_layout);
        initView();
    }

    /**
     * 初始化控件
     */
    private void initView() {
        txtMsg = (TextView) findViewById(R.id.select_txt_msg);
        btnSure = (Button) findViewById(R.id.select_btn_sure);
        txtMsg.setText(strMessage);
        if(this.clickListener!=null){
            btnSure.setOnClickListener(clickListener);
        }else{
            btnSure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isShowing()){
                        dismiss();
                    }
                }
            });
        }
    }
}

