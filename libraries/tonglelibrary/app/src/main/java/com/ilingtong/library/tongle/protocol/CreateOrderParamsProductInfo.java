package com.ilingtong.library.tongle.protocol;

import java.util.ArrayList;

/**
 * Created by wuqian on 2016/8/29.
 * mail: wuqian@ilingtong.com
 * Description: 12003入口参数product_info实体
 */
public class CreateOrderParamsProductInfo {
    private String product_id;
    private String order_qty;
    private ArrayList<ProdSpecListItem> prod_spec_list;
    private String need_point;

    public CreateOrderParamsProductInfo(String product_id, String order_qty, ArrayList<ProdSpecListItem> prod_spec_list, String need_point) {
        this.product_id = product_id;
        this.order_qty = order_qty;
        this.prod_spec_list = prod_spec_list;
        this.need_point = need_point;
    }

}
