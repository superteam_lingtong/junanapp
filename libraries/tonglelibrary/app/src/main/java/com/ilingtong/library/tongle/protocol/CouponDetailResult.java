package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:6011接口返回json对应 entity
 */
public class CouponDetailResult extends BaseResult implements Serializable {
   private CouponDetailInfo body;

    public CouponDetailInfo getBody() {
        return body;
    }

    public void setBody(CouponDetailInfo body) {
        this.body = body;
    }
}
