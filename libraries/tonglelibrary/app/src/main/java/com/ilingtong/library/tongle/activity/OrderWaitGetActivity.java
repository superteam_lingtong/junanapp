//package com.ilingtong.app.tongle.activity;
//
//import android.os.Bundle;
//import android.view.View;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.ilingtong.app.tongle.R;
//import com.ilingtong.app.tongle.ServiceManager;
//import com.ilingtong.app.tongle.TongleAppConst;
//import com.ilingtong.app.tongle.TongleAppInstance;
//import com.ilingtong.app.tongle.adapter.TotalOrderListAdapter;
//import com.ilingtong.app.tongle.external.NewActivity;
//import com.ilingtong.app.tongle.external.maxwin.view.XListView;
//import com.ilingtong.app.tongle.model.UserOrdersModel;
//import com.ilingtong.app.tongle.protocol.OrderListItemData;
//import com.ilingtong.app.tongle.protocol.OrderRequestParam;
//import com.ilingtong.app.tongle.protocol.UserOrdersResult;
//import com.ilingtong.app.tongle.utils.ToastUtils;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * User: lengjiqiang
// * Date: 2015/6/7
// * Time: 22:41
// * Email: jqleng@isoftstone.com
// * Desc: 我的订单 -> 待收货 页
// */
//public class OrderWaitGetActivity extends NewActivity {
//    private XListView listView;
//    private RelativeLayout rl_replace;
//    private LinearLayout topview_layout;
//    private TotalOrderListAdapter totalOrderListAdapter;
//    private UserOrdersModel userOrdersModel;
//    private OrderRequestParam param;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.xlistview_comm_layout);
//        initView();
//        doRequest();
//    }
//    public void initView(){
//        listView = (XListView) findViewById(R.id.xlistview);
//        rl_replace = (RelativeLayout) findViewById(R.id.rl_replace);
//        topview_layout = (LinearLayout) findViewById(R.id.topview_layout);
//        topview_layout.setVisibility(View.GONE);
//        if (userOrdersModel == null)
//            userOrdersModel = new UserOrdersModel();
//    }
//    public void doRequest() {
//        param = new OrderRequestParam();
//        param.user_id = TongleAppInstance.getInstance().getUserID();
//        param.order_status = TongleAppConst.ORDER_FILTER_PENGDING_DELIVERY;
//        param.order_date_from = "";
//        param.order_date_to = "";
//        param.order_no = "";
//        param.forward = "";
//        param.fetch_count = "100";
//        ServiceManager.getUserOrdersRequest(param, successListener(), errorListener());
//    }
//
//
//    @Override
//    public void onRefresh(int id) {
//        doRequest();
//    }
//
//    @Override
//    public void onLoadMore(int id) {
//
//    }
//
//    /**
//     * 功能：网络响应成功，返回数据
//     */
//    private Response.Listener successListener() {
//        return new Response.Listener<UserOrdersResult>() {
//            @Override
//            public void onResponse(UserOrdersResult userOrdersResult) {
//
//                if (TongleAppConst.SUCCESS.equals(userOrdersResult.getHead().getReturn_flag())) {
//                    userOrdersModel.data_total_count = userOrdersResult.getBody().getData_total_count();
//                    userOrdersModel.order_list = userOrdersResult.getBody().getOrder_list();
//                    List list = new ArrayList<OrderListItemData>();
//                    for(int x =0;x<userOrdersModel.order_list.size();x++){
//                        if(userOrdersModel.order_list.get(x).status.equals("3")){
//                            list.add(userOrdersModel.order_list.get(x));
//                        }
//                    }
//                    if (list.size()==0){
//                        listView.setVisibility(View.GONE);
//                        rl_replace.setVisibility(View.VISIBLE);
//                    }
//                    updateListView((ArrayList<OrderListItemData>) list);
//                } else {
//                    ToastUtils.toastShort(getResources().getString(R.string.para_exception) +userOrdersResult.getHead().getReturn_message());
//                }
//            }
//
//        };
//    }
//    /**
//     * 功能：网络响应失败
//     */
//    private Response.ErrorListener errorListener() {
//        return new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                ToastUtils.toastShort(getString(R.string.sys_exception));
//            }
//        };
//    }
//    public void updateListView(ArrayList<OrderListItemData> list) {
//        totalOrderListAdapter = new TotalOrderListAdapter(this,list);
//        listView.setPullLoadEnable(false);
//        listView.setPullRefreshEnable(true);
//        listView.setXListViewListener(this, 0);
//        listView.setRefreshTime();
//        listView.setAdapter(totalOrderListAdapter);
//    }
//}
