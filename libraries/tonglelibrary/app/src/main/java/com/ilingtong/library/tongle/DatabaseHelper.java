package com.ilingtong.library.tongle;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * User: lengjiqiang
 * Date: 2015/6/29
 * Time: 23:04
 * Email: jqleng@isoftstone.com
 * Desc: 数据库辅助类
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "tongle.db"; //定义数据库名称
    private static final int DATABASE_VERSION = 1;           //定义数据库版本
    public static final String TABLE_NAME = "Address_table";     //定义数据表名称
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * 没有数据库时调用onCreate()方法创建数据库
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            // 创建数据库后，对数据库的操作
            String strSQL = "create table "+ TABLE_NAME + "(tid integer primary key autoincrement," +
                    "address_no varchar(8)," +
                    "name varchar(20)," +
                    "phone varchar(11)," +
                    "province_id varchar(8)," +
                    "province_name text," +
                    "area_id varchar(8)," +
                    "city_id varchar(8)," +
                    "city_name text," +
                    "area_name text," +
                    "address text," +
                    "post_code varchar(8)," +
                    "default_flag varchar(1))";
            db.execSQL(strSQL);
        } catch ( Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 数据库版本更新时调用onUpgrade()
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // 更改数据库版本的操作
    }

    /**
     * 打开数据库时调用onOpen()
     * @param db
     */
    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        // 打开数据库后首先被执行
    }
}