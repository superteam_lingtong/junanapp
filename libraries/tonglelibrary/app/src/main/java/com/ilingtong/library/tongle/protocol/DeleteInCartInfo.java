package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/11
 * Time: 15:24
 * Email: jqleng@isoftstone.com
 * Desc: 删除购物车中的商品信息类
 */
public class DeleteInCartInfo implements Serializable {
    private String str = "";

    @Override
    public String toString() {
        return str;
    }
}
