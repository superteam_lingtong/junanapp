package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/14.
 * mail: wuqian@ilingtong.com
 * Description:6013的body
 */
public class GroupOrderDetailInfo implements Serializable{

    private CouponOrderBaseInfo order_base;   //订单基本信息
    private CouponCodeurlInfo   coupon_codeurl_info;   //订单识别码信息
    private List<CouponInfo> unused_coupon_list;    //未消费团购券信息
    private List<CouponInfo> refund_coupon_list;    //退款团购券信息
    private List<CouponInfo> used_coupon_list;     //已消费团购券信息
    private List<CouponInfo> expire_coupon_list;    //过期团购券信息
    private List<CouponInfo> refunding_coupon_list;    //退款中团购券信息  ADD 2016/04/14

    public CouponOrderBaseInfo getOrder_base() {
        return order_base;
    }

    public void setOrder_base(CouponOrderBaseInfo order_base) {
        this.order_base = order_base;
    }

    public CouponCodeurlInfo getCoupon_codeurl_info() {
        return coupon_codeurl_info;
    }

    public void setCoupon_codeurl_info(CouponCodeurlInfo coupon_codeurl_info) {
        this.coupon_codeurl_info = coupon_codeurl_info;
    }

    public List<CouponInfo> getUnused_coupon_list() {
        return unused_coupon_list;
    }

    public void setUnused_coupon_list(List<CouponInfo> unused_coupon_list) {
        this.unused_coupon_list = unused_coupon_list;
    }

    public List<CouponInfo> getRefund_coupon_list() {
        return refund_coupon_list;
    }

    public void setRefund_coupon_list(List<CouponInfo> refund_coupon_list) {
        this.refund_coupon_list = refund_coupon_list;
    }

    public List<CouponInfo> getUsed_coupon_list() {
        return used_coupon_list;
    }

    public void setUsed_coupon_list(List<CouponInfo> used_coupon_list) {
        this.used_coupon_list = used_coupon_list;
    }

    public List<CouponInfo> getExpire_coupon_list() {
        return expire_coupon_list;
    }

    public void setExpire_coupon_list(List<CouponInfo> expire_coupon_list) {
        this.expire_coupon_list = expire_coupon_list;
    }

    public List<CouponInfo> getRefunding_coupon_list() {
        return refunding_coupon_list;
    }

    public void setRefunding_coupon_list(List<CouponInfo> refunding_coupon_list) {
        this.refunding_coupon_list = refunding_coupon_list;
    }
}
