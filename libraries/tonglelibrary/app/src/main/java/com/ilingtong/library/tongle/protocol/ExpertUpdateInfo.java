package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: ljq
 * Date: 2015/6/3
 * Time: 22:19
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ExpertUpdateInfo implements Serializable {
    private ArrayList<UserFollowPostList> user_post_list;
    private String data_total_count;

    public String getData_total_count() {
        return data_total_count;
    }

    public ArrayList<UserFollowPostList> getUser_post_list() {
        return user_post_list;
    }

    @Override
    public String toString() {
        return "data_total_count:" + data_total_count + "\r\n" +
                "user_post_list:" + user_post_list;
    }
}
