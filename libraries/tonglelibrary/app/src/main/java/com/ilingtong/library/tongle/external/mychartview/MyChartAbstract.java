package com.ilingtong.library.tongle.external.mychartview;

import android.content.Context;
import android.view.View;

/**
 * User: syc
 * Date: 2015/6/18
 * Time: 17:28
 * Email: ycshi@isoftstone.com
 * Desc:  统计图，chart接口
 */

public interface MyChartAbstract {

    /**
     * 获取一个当前类型图标的Intent实例
     */
    public View getView(Context context);
}

