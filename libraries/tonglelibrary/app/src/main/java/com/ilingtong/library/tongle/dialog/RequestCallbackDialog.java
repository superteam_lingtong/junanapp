package com.ilingtong.library.tongle.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.listener.Task;
import com.ilingtong.library.tongle.widget.LoadingLayout;

/**
 * 加载请求对话框,全局对话框
 *
 * @author GaiQS E-mail:gaiqs@sina.com
 * @Date 2015年6月2日
 * @Time 下午4:17:27
 */
public class RequestCallbackDialog extends Dialog implements DialogInterface.OnKeyListener  {
	private Task<String> task;
	private LoadingLayout ll_load;

	public RequestCallbackDialog(Context context, Task<String> task) {
		super(context, R.style.Dialog_FS);
		this.task = task;
		this.setOnKeyListener(this);// 增加返回监听
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_request_callback);
		// // 设置为系统dialog，注意添加权限
		// getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		initView();
	}

	private void initView() {
		ll_load = (LoadingLayout) findViewById(R.id.ll_load);
	}

	public void show() {
        super.show();
		ll_load.startCountdown();
	}

	public void dismiss() {
		ll_load.cancelCountdown();
		super.dismiss();
	}

	public Boolean isShow() {
		return super.isShowing();
	}

	@Override
	public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
			if (this.isShowing()) {
				if (null != task) {
					task.run(getContext().getString(R.string.dialog_callback_cancel));
				}
			}
		}
		return true;
	}
}
