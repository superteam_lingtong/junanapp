package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shiyuchong
 * Date: 2015/6/19
 * Time: 14:27
 * Email: ycshi@isoftstone.com
 * Desc:[2014] 收益数据列表
 */
public class IncomeListData implements Serializable {


    //日期
    public String date;
    //数量
    public String amount;

}
