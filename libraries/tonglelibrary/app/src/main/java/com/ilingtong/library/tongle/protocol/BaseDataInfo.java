package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 10:58
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class BaseDataInfo implements Serializable {
    private ArrayList<BaseDataListItem> base_data_list;

    public ArrayList<BaseDataListItem> getBase_data_list() {
        return base_data_list;
    }

    @Override
    public String toString() {
        return "base_data_list:" + base_data_list;
    }
}
