package com.ilingtong.library.tongle.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.demo.Base64;
import com.ilingtong.library.tongle.protocol.ProductVoiceResult;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.AudioTrackPlayThread;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * 显示二维码图片的dialog
 *
 * @author qian
 */
public class ShowQRCodeDialog extends Dialog {

    private String url;    //图片地址
    private String code_type;   //二维码类别
    private String object_id;   //魔店id，会员id，商品id，组织id
    private TextView txt_play_pcm;
    private CheckBox checkBox_paly_pcm;
    byte[] data = null;
    Thread mThread = null;
    final int EVENT_PLAY_OVER = 0x100;

    Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            if (message.what == EVENT_PLAY_OVER) {
                checkBox_paly_pcm.setChecked(false);
            }
        }
    };

    public ShowQRCodeDialog(Context context, String url) {
        super(context, R.style.Dialog_FS);
        this.url = url;
    }

    public ShowQRCodeDialog(Context context, String url, String object_id,String code_type) {
        super(context, R.style.Dialog_FS);
        this.url = url;
        this.object_id = object_id;
        this.code_type = code_type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expert_popwindow_layout);
        ImageView coverImage = (ImageView) findViewById(R.id.qr_user);
        if (TextUtils.isEmpty(object_id)) {
            findViewById(R.id.relativelayout_play_pcm).setVisibility(View.GONE);
        }

        txt_play_pcm = (TextView) findViewById(R.id.txt_play_pcm);
        checkBox_paly_pcm = (CheckBox) findViewById(R.id.checkbox_play_pcm);
        checkBox_paly_pcm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    play();
                } else {
                    txt_play_pcm.setText(getContext().getString(R.string.play_prod_pcm));
                    stop();
                }
            }
        });
        ImageLoader.getInstance().displayImage(url, coverImage, ImageOptionsUtils.getOptions());
    }

    /**
     * 从接口获取音频文件流
     */
    private void getPcmData() {
        ServiceManager.getProductVoice(code_type,object_id, new Response.Listener<ProductVoiceResult>() {
            @Override
            public void onResponse(ProductVoiceResult productVoiceResult) {
                if (TongleAppConst.SUCCESS.equals(productVoiceResult.getHead().getReturn_flag())) {
                    data = Base64.decode(productVoiceResult.body.product_voice_file);
                    play();
                } else {
                    ToastUtils.toastLong(getContext().getString(R.string.para_exception) + productVoiceResult.getHead().getReturn_message());
                    checkBox_paly_pcm.setChecked(false);
                }
                checkBox_paly_pcm.setEnabled(true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getContext().getString(R.string.sys_exception));
                checkBox_paly_pcm.setEnabled(true);
                checkBox_paly_pcm.setChecked(false);
            }
        });
    }

    /**
     * 开始播放
     */
    private void play() {
        if (data == null) {
            ToastUtils.toastShort("正在获取音频，请稍候");
            txt_play_pcm.setText("正在获取音频");
            checkBox_paly_pcm.setEnabled(false);
            getPcmData();
            return;
        }

        if (mThread == null && isShowing()) {
            mThread = new Thread(new AudioTrackPlayThread(data, mHandler, 100));
            mThread.start();
            txt_play_pcm.setText(getContext().getString(R.string.stop_prod_pcm));
        }

    }

    /**
     * 停止播放
     */
    private void stop() {
        if (mThread != null) {
            AudioTrackPlayThread.flag = false;
            mThread = null;
        }
    }

    @Override
    public void dismiss() {
        stop();
        super.dismiss();
    }
}
