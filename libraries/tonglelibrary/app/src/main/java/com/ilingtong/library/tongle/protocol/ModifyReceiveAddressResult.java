package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: syc
 * Date: 2015/6/15
 * Time: 17:51
 * Email: ycshi@isoftstone.com
 * Dest: on 2015/6/15 0015.
 * [1032]
 */
public class ModifyReceiveAddressResult implements Serializable {

    private BaseInfo head;
    private AddressInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public AddressInfo getBody() {
        return body;
    }

    public void setBody(AddressInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }



}
