package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/7/15
 * Time: 16:26
 * Email: jqleng@isoftstone.com
 * Desc: 商品评价类，包含网络返回的数据
 */
public class MyPointsInfo implements Serializable {
    public String user_id;
    public ArrayList<MyAccountInfo> account_list;
    public String withdraw_url;
    public String withdraw_title;

    @Override
    public String toString() {
        return "user_id:" + user_id + "\r\n" +
                "account_list:" + account_list + "\r\n" +
                "withdraw_url:" + withdraw_url + "\r\n" +
                "withdraw_title:" + withdraw_title;
    }
}
