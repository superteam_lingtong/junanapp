package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 22:03
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class CartSettlementResult implements Serializable {
    private BaseInfo head;
    private CatrReturnInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public CatrReturnInfo getBody() {
        return body;
    }

    public void setBody(CatrReturnInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
