package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:团购券详细信息；注：团购券是按照订单封装列表。可以理解为订单信息。
 */
public class CouponOrderInfo implements Serializable {
    private String order_no;             //订单编号
    private String prod_name;   //商品名称
    private String mstore_id;  //魔店ID
    private String mstore_name;      //魔店名称
    private String overdue_date_from;  //商品有效期 from
    private String overdue_date_to;     //商品有效期to
    private String out_of_date;           //过期/未消费标志
    private CouponCodeurlInfo coupon_codeurl_info;           //订单识别码信息
    private List<CouponCode> coupon_code_list;         //团购券号列表

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public String getMstore_id() {
        return mstore_id;
    }

    public void setMstore_id(String mstore_id) {
        this.mstore_id = mstore_id;
    }

    public String getMstore_name() {
        return mstore_name;
    }

    public void setMstore_name(String mstore_name) {
        this.mstore_name = mstore_name;
    }

    public String getOverdue_date_from() {
        return overdue_date_from;
    }

    public void setOverdue_date_from(String overdue_date_from) {
        this.overdue_date_from = overdue_date_from;
    }

    public String getOverdue_date_to() {
        return overdue_date_to;
    }

    public void setOverdue_date_to(String overdue_date_to) {
        this.overdue_date_to = overdue_date_to;
    }

    public String getOut_of_date() {
        return out_of_date;
    }

    public void setOut_of_date(String out_of_date) {
        this.out_of_date = out_of_date;
    }

    public CouponCodeurlInfo getCoupon_codeurl_info() {
        return coupon_codeurl_info;
    }

    public void setCoupon_codeurl_info(CouponCodeurlInfo coupon_codeurl_info) {
        this.coupon_codeurl_info = coupon_codeurl_info;
    }

    public List<CouponCode> getCoupon_code_list() {
        return coupon_code_list;
    }

    public void setCoupon_code_list(List<CouponCode> coupon_code_list) {
        this.coupon_code_list = coupon_code_list;
    }
}
