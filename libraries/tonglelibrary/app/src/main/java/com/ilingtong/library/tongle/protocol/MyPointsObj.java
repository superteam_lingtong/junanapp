package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/15
 * Time: 16:26
 * Email: jqleng@isoftstone.com
 * Desc: 商品评价类，包含网络返回的数据
 */
public class MyPointsObj implements Serializable {
    public  MyPointsInfo  user_account_info;

    @Override
    public String toString() {
        return "MyPointsObj{" +
                "user_account_info=" + user_account_info +
                '}';
    }
}
