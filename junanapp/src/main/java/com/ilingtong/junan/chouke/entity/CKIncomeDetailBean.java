package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 10:32
 * Email: liuting@ilingtong.com
 * Desc:used by 7012接口(取得组织业绩组织酬金明细) 业绩列表
 */
public class CKIncomeDetailBean {
    private CKIncomeOrgInfo org_info;//组织信息
    private CKIncomeUserInfo user_info;//用户信息
    private CKIncomeDetailInfo income_info;//获利信息

    public CKIncomeOrgInfo getOrg_info() {
        return org_info;
    }

    public void setOrg_info(CKIncomeOrgInfo org_info) {
        this.org_info = org_info;
    }

    public CKIncomeUserInfo getUser_info() {
        return user_info;
    }

    public void setUser_info(CKIncomeUserInfo user_info) {
        this.user_info = user_info;
    }

    public CKIncomeDetailInfo getIncome_info() {
        return income_info;
    }

    public void setIncome_info(CKIncomeDetailInfo income_info) {
        this.income_info = income_info;
    }
}
