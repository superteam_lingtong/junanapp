package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 9:33
 * Email: liuting@ilingtong.com
 * Desc:used by 7008接口(取得我的业绩总览) 我的业绩
 */
public class CKMyIncomeInfoBean {
    private double organization_income;//组织业绩
    private double organization_payoff;//组织酬金
    private double personal_payoff;//个人酬金

    public double getOrganization_income() {
        return organization_income;
    }

    public void setOrganization_income(double organization_income) {
        this.organization_income = organization_income;
    }

    public double getOrganization_payoff() {
        return organization_payoff;
    }

    public void setOrganization_payoff(double organization_payoff) {
        this.organization_payoff = organization_payoff;
    }

    public double getPersonal_payoff() {
        return personal_payoff;
    }

    public void setPersonal_payoff(double personal_payoff) {
        this.personal_payoff = personal_payoff;
    }
}
