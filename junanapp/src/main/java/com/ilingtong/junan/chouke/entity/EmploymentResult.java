package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/4/26.
 * mail: wuqian@ilingtong.com
 * Description:创业就业中心接口返回json body
 * use by 7017接口
 */
public class EmploymentResult extends BaseResult implements Serializable {
    public EmploymentListInfo body;
}
