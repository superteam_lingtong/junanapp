package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 9:45
 * Email: liuting@ilingtong.com
 * Desc:7009接口(取得组织业绩组织酬金列表)返回json结构
 */
public class CKOrgIncomeResult extends BaseResult implements Serializable {
    private CKOrgIncomeInfo body;//组织业绩组织酬金列表信息

    public CKOrgIncomeInfo getBody() {
        return body;
    }

    public void setBody(CKOrgIncomeInfo body) {
        this.body = body;
    }
}
