package com.ilingtong.junan.chouke.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ScrollView;

/**
 * Created by wuqian on 2016/5/27.
 * mail: wuqian@ilingtong.com
 * Description:滑动悬停scrollview
 */
public class CanMonitorScrollView extends ScrollView{
    private onScrollListener mListener = null;

    public CanMonitorScrollView(Context context) {
        super(context);
    }

    public CanMonitorScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CanMonitorScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        Log.e("TAG","onScrollChanged L="+l+",t="+t+",oldL="+oldl+",oldt="+oldt);
        Log.e("TAG","onScrollChanged getMeasureHegiht="+getChildAt(0).getMeasuredHeight()+",getHeight="+getHeight());
        super.onScrollChanged(l, t, oldl, oldt);
        if(null != mListener){
            if(t == 0){
                mListener.isScrollTop();
            }else if(t + getHeight() >= getChildAt(0).getMeasuredHeight()){
                mListener.isScrollBottom();
            }else{
                mListener.onScroll(l,t,oldl,oldt);
            }
        }
    }

    public void setScrollListener(onScrollListener listener){
        this.mListener = listener;
    }

    public interface onScrollListener{
        public void onScroll(int left, int top, int oldLeft, int oldTop);

        public void isScrollTop();

        public void isScrollBottom();
    }
}
