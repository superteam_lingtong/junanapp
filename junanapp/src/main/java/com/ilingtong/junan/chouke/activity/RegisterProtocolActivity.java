package com.ilingtong.junan.chouke.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.junan.chouke.CKConstant;
import com.ilingtong.junan.chouke.CKServiceManager;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.fragment.CKLoginFragment;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.BaseActivity;
import com.ilingtong.library.tongle.protocol.BaseResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

/**
 * author: liuting
 * Date: 2016/5/30
 * Time: 10:17
 * Email: liuting@ilingtong.com
 * Desc:酬客平台加入协议
 */
public class RegisterProtocolActivity extends BaseActivity implements View.OnClickListener{
    private ImageView mImgBack;//返回
    private TextView mTxtTitle;//标题
    private TextView mTxtAgree;//同意
    private WebView mWvProtocol;//协议H5页面
    private String type;//区分类型：0代表注册页进入，1代表登录进入
    private String mURL;//路径

    /**
     * @param context
     * @param request_code
     */
    public static void launcher(Activity context,String url,String type,int request_code){
        Intent intent = new Intent(context,RegisterProtocolActivity.class);
        intent.putExtra("url",url);
        intent.putExtra("type",type);
        context.startActivityForResult(intent, request_code);
    }

    /**
     * @param context
     * @param request_code
     */
    public static void launcher(Fragment context, String url, String type, int request_code){
        Intent intent = new Intent(context.getActivity(),RegisterProtocolActivity.class);
        intent.putExtra("url",url);
        intent.putExtra("type",type);
        context.startActivityForResult(intent, request_code);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_comm);

        initView();
    }

    /**
     * 初始化控件
     */
    private void initView() {
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mTxtAgree = (TextView) findViewById(R.id.top_btn_text);
        mImgBack.setVisibility(View.VISIBLE);
        mTxtTitle.setVisibility(View.VISIBLE);
        mTxtAgree.setVisibility(View.VISIBLE);
        mImgBack.setOnClickListener(this);
        mTxtTitle.setText(R.string.org_detail_join_protocol_title);
        mTxtAgree.setText(R.string.register_protocol_title_txt);
        mImgBack.setOnClickListener(this);
        mTxtAgree.setOnClickListener(this);
        mTxtAgree.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16); //16SP

        mWvProtocol = (WebView) findViewById(R.id.webview);
        type=getIntent().getExtras().getString("type");
        mURL=getIntent().getExtras().getString("url","");

        mWvProtocol.setWebViewClient(new WebViewClient() {

            /**
             * 加载错误
             *
             * @param view
             * @param errorCode
             * @param description
             * @param failingUrl
             */
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(RegisterProtocolActivity.this, R.string.para_exception + description, Toast.LENGTH_SHORT).show();
            }
        });

        mWvProtocol.getSettings().setJavaScriptEnabled(true);
        mWvProtocol.loadUrl(mURL);
    }

    /**
     * 同意协议请求成功
     *
     * @return
     */
    private Response.Listener<BaseResult> agreeSuccessListener() {
        return new Response.Listener<BaseResult>() {
            @Override
            public void onResponse(BaseResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    //操作成功，结束登录页和当前页，缓存数据，进入首页
                    setResult(RESULT_OK);
                    if(CKLoginFragment.instance!=null){
                        CKLoginFragment.instance.finish();
                    }
                    finish();
                    Intent mainIntent = new Intent(RegisterProtocolActivity.this, com.ilingtong.library.tongle.activity.MainActivity.class);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mainIntent.putExtra(TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_NAME,"");
                    startActivity(mainIntent);
                } else {
                    ToastUtils.toastLong(getResources().getString(com.ilingtong.library.tongle.R.string.para_exception)+ response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 请求失败
     *
     * @return
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getResources().getString(com.ilingtong.library.tongle.R.string.sys_exception));
            }
        };
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.left_arrow_btn) {//返回
            finish();
        } else if (i == R.id.top_btn_text) {//同意协议
            if(type.equals(CKConstant.REGISTER_INTO_TYPE)) {//从注册页进入
                setResult(RESULT_OK);
                finish();
            }else if(type.equals(CKConstant.LOGIN_INTO_TYPE)) {//登录进入
                CKServiceManager.agreeProtocol(agreeSuccessListener(),errorListener());
            }
        }
    }
}
