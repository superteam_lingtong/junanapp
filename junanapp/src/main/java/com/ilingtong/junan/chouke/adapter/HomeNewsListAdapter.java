package com.ilingtong.junan.chouke.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.junan.chouke.R;
import com.ilingtong.library.tongle.protocol.UserFollowPostList;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by wuqian on 2016/8/30.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class HomeNewsListAdapter extends BaseAdapter {
    private Context context;
    private List<UserFollowPostList> list;

    public HomeNewsListAdapter(Context context, List<UserFollowPostList> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        viewHolder holder;
        if (convertView == null) {
            holder = new viewHolder();

            convertView = LayoutInflater.from(context).inflate(R.layout.ck_home_news_list_item, null);
            holder.img_pic = (ImageView) convertView.findViewById(R.id.home_news_list_item_img_pic);
            holder.txt_title = (TextView) convertView.findViewById(R.id.home_news_list_item_txt_title);
            holder.txt_date = (TextView) convertView.findViewById(R.id.home_news_list_item_txt_date);
            convertView.setTag(holder);
        }else {
            holder = (viewHolder) convertView.getTag();
        }
        if (list.get(position).post_thumbnail_pic_url != null && list.get(position).post_thumbnail_pic_url.size()>0){
            ImageLoader.getInstance().displayImage(list.get(position).post_thumbnail_pic_url.get(0).pic_url,holder.img_pic, ImageOptionsUtils.getOptions());
        }else {
            holder.img_pic.setImageResource(R.drawable.default_image);
        }
        holder.txt_title.setText(list.get(position).post_title+"");
        holder.txt_date.setText(list.get(position).post_time);
        return convertView;
    }

    class viewHolder {
        ImageView img_pic;
        TextView txt_title;
        TextView txt_date;
    }
}

