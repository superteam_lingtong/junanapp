package com.ilingtong.junan.chouke.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;
import com.github.lzyzsd.jsbridge.DefaultHandler;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.BaseActivity;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.activity.ProductTicketDetailActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

//import com.github.lzyzsd.jsbridge.BridgeHandler;
//import com.github.lzyzsd.jsbridge.BridgeWebView;
//import com.github.lzyzsd.jsbridge.CallBackFunction;
//import com.github.lzyzsd.jsbridge.DefaultHandler;

/**
 * author: liuting
 * Date: 2016/5/27
 * Time: 14:00
 * Email: liuting@ilingtong.com
 * Desc:组织推荐商品列表
 */
public class OrgProdListActivity extends BaseActivity implements View.OnClickListener {
    private TextView top_name;//标题栏
    private ImageView left_arrow_btn;//返回
//    private WebView webView;//html页面
    private BridgeWebView webView;//html页面
    private String prod_url;//加载的url
    private String myTitle;//标题

    public static void launcher(Activity context, String prod_url,String title) {
        Intent intent = new Intent(context, OrgProdListActivity.class);
        intent.putExtra("prod_url", (Serializable) prod_url);
        intent.putExtra("myTitle", (Serializable) title);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(com.ilingtong.library.tongle.R.layout.webview_comm);
        setContentView(R.layout.ck_activity_org_prod);
        initView();
    }

    /**
     * 初始化控件
     */
    public void initView() {
        Bundle bundle = getIntent().getExtras();
        prod_url = bundle.getString("prod_url");
        myTitle = bundle.getString("myTitle");

        top_name = (TextView) findViewById(com.ilingtong.library.tongle.R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(com.ilingtong.library.tongle.R.id.left_arrow_btn);
        webView = (BridgeWebView) findViewById(R.id.org_prod_web_main);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setText(myTitle);
        top_name.setVisibility(View.VISIBLE);

//        webView.setWebViewClient(new WebViewClient() {
//
//            /**
//             * 加载错误
//             *
//             * @param view
//             * @param errorCode
//             * @param description
//             * @param failingUrl
//             */
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(OrgProdListActivity.this, R.string.para_exception + description, Toast.LENGTH_SHORT).show();
//            }
//        });
        webView.setWebChromeClient(new WebChromeClient() {

        });
        webView.setDefaultHandler(new DefaultHandler());
        Log.e("TAG","run--->default");
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.addJavascriptInterface(new JavaScriptInterface(OrgProdListActivity.this), "nativeMethod");
        webView.loadUrl(prod_url);
        Log.e("TAG","run--->load");
        webView.registerHandler("submitFromWeb", new BridgeHandler() {

            @Override
            public void handler(String data, CallBackFunction function) {
                Log.e("TAG","run--->register");
                Log.e("TAG",data);
                try {
                    JSONObject product = new JSONObject(data);
                    String coupon_flag = product.getString("coupon_flag");
                    String prod_id = product.getString("prod_id");
                    String relation_id = product.getString("relation_id");
                    Log.e("TAG", "coupon_flag =" + coupon_flag + " prod_id =" + prod_id + " relation_id =" + relation_id);
                    if (coupon_flag.equals(TongleAppConst.YES)) {//团购商品
                        ProductTicketDetailActivity.launch(OrgProdListActivity.this, prod_id, TongleAppConst.ACTIONID_ORG_PRODUCT, relation_id, "", "");
                    } else {
                        //普通商品
                        CollectProductDetailActivity.launch(OrgProdListActivity.this, prod_id, TongleAppConst.ACTIONID_ORG_PRODUCT, relation_id, "", "");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
    }

//    public class JavaScriptInterface {
//        Activity mContext;
//
//        JavaScriptInterface(Activity c) {
//            mContext = c;
//            Log.e("tag","create success");
//        }
//
//        /**
//         * @param prod_id 商品ID
//         * @param coupon_flag  团购券标志
//         * @param relation_id  关联ID
//         */
//        @JavascriptInterface
//        public void toActivity(String prod_id, String coupon_flag, String relation_id) {
//            //此处应该定义常量对应，同时提供给web页面编写者
//            Log.e("tag",prod_id+coupon_flag+relation_id);
//            if (coupon_flag.equals(TongleAppConst.YES)) {//团购商品
//                ProductTicketDetailActivity.launch(mContext, prod_id, TongleAppConst.ACTIONID_ORG_PRODUCT, relation_id, "", "");
//            } else {
//                //普通商品
//                CollectProductDetailActivity.launch(mContext, prod_id, TongleAppConst.ACTIONID_ORG_PRODUCT, relation_id, "", "");
//            }
//        }
//    }

    @Override
    public void onClick(View v) {
        if (v.getId() == com.ilingtong.library.tongle.R.id.left_arrow_btn) {
            finish();
        }
    }
}
