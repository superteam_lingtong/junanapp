package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 9:20
 * Email: liuting@ilingtong.com
 * Desc:7007接口(获取组织邀请二维码)返回json结构
 */
public class CKOrgQrCodeResult  extends BaseResult implements Serializable {
    private CKOrgQrCodeInfo body;//组织邀请二维码信息

    public CKOrgQrCodeInfo getBody() {
        return body;
    }

    public void setBody(CKOrgQrCodeInfo body) {
        this.body = body;
    }
}
