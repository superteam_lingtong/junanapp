package com.ilingtong.junan.chouke.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * author: liuting
 * Date: 2016/5/24
 * Time: 17:52
 * Email: liuting@ilingtong.com
 * Desc:自定义dialog
 */
public class MyDialog extends Dialog {
    private static int mWidth=0;//宽度
    private static int mHeight=0;//高度

    public MyDialog(Context context,View layout, int style) {
        this(context,mWidth, mHeight, layout, style);
    }

    public MyDialog(Context context, int width, int height, View layout, int style) {
        super(context, style);
        setContentView(layout);
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.CENTER;
        params.width=width;
        window.setAttributes(params);
    }

}
