package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/5/16.
 * mail: wuqian@ilingtong.com
 * Description:7001接口(获取我的基本信息)返回json结构
 */
public class CKPersonalInfoResult extends BaseResult implements Serializable {
    private CKPersonalInfo body;//用户信息

    public CKPersonalInfo getBody() {
        return body;
    }

    public void setBody(CKPersonalInfo body) {
        this.body = body;
    }
}
