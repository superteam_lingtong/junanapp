package com.ilingtong.junan.chouke.entity;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 16:04
 * Email: liuting@ilingtong.com
 * Desc:used by 7003接口(获取指定组织的详情) 组织内部发帖信息
 */
public class CKPostInfo {
    private int data_total_count;//内部帖子总数量
    private List<CKUserPostList> user_post_list;//帖子列表

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<CKUserPostList> getUser_post_list() {
        return user_post_list;
    }

    public void setUser_post_list(List<CKUserPostList> user_post_list) {
        this.user_post_list = user_post_list;
    }
}
