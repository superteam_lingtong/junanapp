package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 9:27
 * Email: liuting@ilingtong.com
 * Desc:7008接口(取得我的业绩总览)返回json结构
 */
public class CKMyIncomeResult extends BaseResult implements Serializable {
    private CKMyIncomeInfo body;//我的业绩信息

    public CKMyIncomeInfo getBody() {
        return body;
    }

    public void setBody(CKMyIncomeInfo body) {
        this.body = body;
    }
}
