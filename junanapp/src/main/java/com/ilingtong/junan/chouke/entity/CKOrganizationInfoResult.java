package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 15:12
 * Email: liuting@ilingtong.com
 * Desc:7002接口(获取我的关注组织列表) 返回json结构
 */
public class CKOrganizationInfoResult extends BaseResult implements Serializable {
    private CKOrganizationInfo body;//组织信息

    public CKOrganizationInfo getBody() {
        return body;
    }

    public void setBody(CKOrganizationInfo body) {
        this.body = body;
    }

}
