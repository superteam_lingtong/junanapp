package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 9:28
 * Email: liuting@ilingtong.com
 * Desc:7008接口(取得我的业绩总览)的body 我的业绩信息
 */
public class CKMyIncomeInfo {
    private CKMyIncomeInfoBean income_info;//我的业绩

    public CKMyIncomeInfoBean getIncome_info() {
        return income_info;
    }

    public void setIncome_info(CKMyIncomeInfoBean income_info) {
        this.income_info = income_info;
    }
}
