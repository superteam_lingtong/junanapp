package com.ilingtong.junan.chouke.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.junan.chouke.CKConstant;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.activity.JARegisterActivity;
import com.ilingtong.junan.chouke.activity.JARegisterIdCardInfoActivity;
import com.ilingtong.junan.chouke.activity.RegisterProtocolActivity;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.LoginPwdRecoveryActivity;
import com.ilingtong.library.tongle.activity.MainActivity;
import com.ilingtong.library.tongle.fragment.BaseFragment;
import com.ilingtong.library.tongle.protocol.LoginResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

/**
 * Created by wuqian on 2016/5/30.
 * mail: wuqian@ilingtong.com
 * Description:酬客登录页面
 */
public class CKLoginFragment extends BaseFragment implements View.OnClickListener{
    private EditText mEdtPhone;//账号或手机号
    private EditText mEdtPassword;//密码
    private Button mBtnLogin;//登录
    private TextView mTxtForget;//忘记密码
    private TextView mTxtRegister;//立即注册

    private Dialog mDialog;//加载对话框
    private String mPhone;//手机号
    private String mPassword;//密码
    public SharedPreferences mLoginSp, mUserSp;
    public static final int AGREE_PROTOCOL_REQUEST = 3001;//获取协议请求码
    public static Activity instance;
    private LoginResult mLoginResult;//返回结果
    private CheckBox mCbRemember;//记住账号  Add 2016/8/25

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ck_activity_login,null);
        initView(view);
        isLogin();
        return view;
    }

    /**
     * 初始化控件
     *
     * @param view
     */
    public void initView(View view){
        mEdtPhone = (EditText) view.findViewById(R.id.login_edt_phone);
        mEdtPassword = (EditText) view.findViewById(R.id.login_edt_password);
        mBtnLogin = (Button) view.findViewById(R.id.login_btn_login);
        mTxtForget = (TextView) view.findViewById(R.id.login_tv_forget);
        mTxtRegister = (TextView) view.findViewById(R.id.login_tv_register);
        mBtnLogin.setOnClickListener(this);
        mTxtForget.setOnClickListener(this);
        mTxtRegister.setOnClickListener(this);

        mDialog = DialogUtils.createLoadingDialog(getActivity());
        mDialog.setCancelable(true);
        instance=getActivity();

        mLoginSp = TongleAppInstance.getAppContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        mUserSp = TongleAppInstance.getAppContext().getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        //做手机号的改变监听，判断底部登录按钮的背景
        mEdtPhone.addTextChangedListener(textChange);

        //做密码的改变监听，判断底部登录按钮的背景
        mEdtPassword.addTextChangedListener(textChange);

        mCbRemember=(CheckBox)view.findViewById(R.id.login_cb_remember_password);
    }

    //文本框的改变监听，判断底部登录按钮的背景
    private TextWatcher textChange=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            mPhone = mEdtPhone.getText().toString();
            mPassword = mEdtPassword.getText().toString();

            if (TextUtils.isEmpty(mPhone)||TextUtils.isEmpty(mPassword)){
                mBtnLogin.setEnabled(false);
            }else{
                mBtnLogin.setEnabled(true);
            }
        }
    };

    //判断是否自动登录
    public void isLogin() {
        //是否自动输入号码及密码
        if (mLoginSp.getBoolean(TongleAppInstance.getInstance().getIfRemberKeyName(), true)) {
            mEdtPhone.setText(mLoginSp.getString(TongleAppInstance.getInstance().getLoginUseridKeyName(), ""));
            mEdtPassword.setText(mLoginSp.getString(TongleAppInstance.getInstance().getLoginPwdKeyName(), ""));
        } else {
            mEdtPassword.setText("");
        }
        mEdtPhone.setSelection(mEdtPhone.getText().length());
        mEdtPassword.setSelection(mEdtPassword.getText().length());
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.login_btn_login) {//登录
            mPhone=mEdtPhone.getText().toString();
            mPassword=mEdtPassword.getText().toString();
            if(TextUtils.isEmpty(mPhone)||TextUtils.isEmpty(mPassword)){
                ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.common_userid_pwd_null));
            }else {
                TongleAppInstance.getInstance().setUser_phone(mPhone);
                mDialog.show();
                ServiceManager.doLogin(mPhone, mPassword, successListener(), errorListener());
            }

        }else if(i == R.id.login_tv_forget){//忘记密码
            startActivity(new Intent(getActivity(), LoginPwdRecoveryActivity.class));

        }else if(i == R.id.login_tv_register){//注册
           startActivity(new Intent(getActivity(), JARegisterActivity.class));
        }
    }

    private Response.Listener<LoginResult> successListener() {
        return new Response.Listener<LoginResult>() {
            @Override
            public void onResponse(LoginResult response) {
                mDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mLoginResult=response;
                    TongleAppInstance.getInstance().setUserID(mLoginResult.getBody().getUserID());
                    TongleAppInstance.getInstance().setToken(mLoginResult.getBody().getToken());

                    if (TongleAppConst.REGISTER_STATUES_NO_DATA.equals(response.getBody().getCheck_status())){
                        //未提交资料，提示提交资料
                        ToastUtils.toastShort("用户未审核，暂时不能登录，请提交资料");
                        JARegisterIdCardInfoActivity.launch(getActivity(),mPhone,response.getBody().getUserID());
                        return;
                    }
//                    if (TongleAppConst.REGISTER_STATUES_DATA_WAIT_CHECK.equals(response.getBody().getCheck_status())){
//                        //提示资料正在审核
//                        ToastUtils.toastShort("正在审核用户资料，暂时不能登录，请耐心等待");
//                        return;
//                    }
                    if (TongleAppConst.REGISTER_STATUES_DATA_REFUSE.equals(response.getBody().getCheck_status())){
                        //提示审核未通过，需重新提交资料
                        ToastUtils.toastShort("用户审核失败，暂时不能登录，请重新提交资料");
                        JARegisterIdCardInfoActivity.launch(getActivity(),mPhone,response.getBody().getUserID());
                        return;
                    }
                    //缓存user_id和token
                    mUserSp.edit().putString("token", mLoginResult.getBody().getToken()).commit();
                    mUserSp.edit().putString("user_id", mLoginResult.getBody().getUserID()).commit();// 用户ID

                    if(response.getBody().getIs_protocol_show().equals(TongleAppConst.YES)){//需要显示协议，加载协议页面
                        String url=response.getBody().getPayoff_protocol_url()+ "?user_id=" + TongleAppInstance.getInstance().getUserID() + "&user_token=" + TongleAppInstance.getInstance().getToken()+"&app_inner_no="+TongleAppInstance.getInstance().getApp_inner_no();
                        RegisterProtocolActivity.launcher(CKLoginFragment.this,url, CKConstant.LOGIN_INTO_TYPE,AGREE_PROTOCOL_REQUEST);
                    }else{//结束当前页，直接进入首页
                        initData();//获取数据
                        Intent mainIntent = new Intent(getActivity(), MainActivity.class);
                        mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mainIntent.putExtra(TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_NAME,"");
                        startActivity(mainIntent);
                        getActivity().finish();
                    }

                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
                mDialog.dismiss();
            }
        };
    }

    /**
     * 获取数据
     */
    public void initData(){
        TongleAppInstance.getInstance().setUser_photo_url(mLoginResult.getBody().getUser_head_photo_url());

        //记录登录名与密码
        mLoginSp.edit().putString(TongleAppInstance.getInstance().getLoginUseridKeyName(), mPhone).commit();
        if(mCbRemember.isChecked()) {//如果选中则记住密码
            mLoginSp.edit().putString(TongleAppInstance.getInstance().getLoginPwdKeyName(), mPassword).commit();
            mLoginSp.edit().putBoolean(TongleAppInstance.getInstance().getIfRemberKeyName(),true).commit();
            mLoginSp.edit().putBoolean(TongleAppInstance.getInstance().getIfAutoLoginKeyName(),true).commit();
        }else {
            mLoginSp.edit().putBoolean(TongleAppInstance.getInstance().getIfRemberKeyName(),false).commit();
            mLoginSp.edit().putBoolean(TongleAppInstance.getInstance().getIfAutoLoginKeyName(),false).commit();
        }

        //缓存重要数据
        mUserSp.edit().putString("user_photo_url", mLoginResult.getBody().getUser_head_photo_url()).commit();// 头像URL
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==AGREE_PROTOCOL_REQUEST){
            if (resultCode==Activity.RESULT_OK){
                //同意协议，成功进入首页后才能缓存数据
                initData();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
