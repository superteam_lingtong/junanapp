package com.ilingtong.junan.chouke.utils;

import android.graphics.Bitmap;

import com.ilingtong.junan.chouke.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

/**
 * Created by wuqian on 2016/5/27.
 * mail: wuqian@ilingtong.com
 * Description: 加载图片的options
 */
public class ImageLoaderOptionsUtils {
    /**
     * 我的酬客/直粉/通粉 头像options
     *
     * @return
     */
    public static DisplayImageOptions fansHeadOption() {
        return new DisplayImageOptions.Builder()
                .showStubImage(R.mipmap.ck_fans_list_item_defualt_head)
                .showImageOnLoading(R.mipmap.ck_fans_list_item_defualt_head)// 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.ck_fans_list_item_defualt_head)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.ck_fans_list_item_defualt_head)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
                .displayer(new RoundedBitmapDisplayer(4))    // 设置成圆角图片
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    /**
     * 首页 会员圆形头像
     *
     * @return
     */
    public static DisplayImageOptions homeCircleHeadOption() {
        return new DisplayImageOptions.Builder()
                .showStubImage(R.mipmap.chouke_head_defualt)
                .showImageOnLoading(R.mipmap.chouke_head_defualt)// 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.chouke_head_defualt)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.chouke_head_defualt)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
                .displayer(new RoundedBitmapDisplayer(90))    // 设置成圆角图片
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    /**
     *组织帖子头像
     *
     * @return
     */
    public static DisplayImageOptions orgPostOptions(){
        return new DisplayImageOptions.Builder()
                .showStubImage(R.mipmap.ck_post_head)            // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.ck_post_head)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.ck_post_head)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
                .displayer(new RoundedBitmapDisplayer(20))    // 设置成圆角图片
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    /**
     *组织默认头像
     *
     * @return
     */
    public static DisplayImageOptions orgIconOptions(){
        return new DisplayImageOptions.Builder()
                .showStubImage(R.mipmap.ck_default_organization_head)            // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.ck_default_organization_head)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.ck_default_organization_head)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
                .displayer(new RoundedBitmapDisplayer(20))    // 设置成圆角图片
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }


    /**
     * 推荐人头像
     *
     * @return
     */
    public static DisplayImageOptions recommendHeadOptions(){
        return new DisplayImageOptions.Builder()
                .showStubImage(R.mipmap.ck_ic_default_userhead)            // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.ck_ic_default_userhead)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.ck_ic_default_userhead)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
                .displayer(new RoundedBitmapDisplayer(90))    // 设置成圆角图片
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    /**
     * @author: liuting
     * @date: 2017/4/26 16:05
     * @Title: employmentCenterLogoOptions
     * @Description: 加载就业创业中心logo
     * @param:
     * @return: com.nostra13.universalimageloader.core.DisplayImageOptions
     * @throws
     */
    public static DisplayImageOptions employmentCenterLogoOptions(){
        return new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.default_image)            // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.default_image)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.default_image)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }
}
