package com.ilingtong.junan.chouke.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.entity.CKUserPostList;
import com.ilingtong.junan.chouke.utils.ImageLoaderOptionsUtils;
import com.ilingtong.junan.chouke.widget.ninegridimageview.NineGridImageView;
import com.ilingtong.junan.chouke.widget.ninegridimageview.NineGridImageViewAdapter;
import com.ilingtong.library.tongle.protocol.PostPicUrlInfo;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/23
 * Time: 15:32
 * Email: liuting@ilingtong.com
 * Desc:指定组织详情的帖子列表Adapter
 */
public class OrgPostListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<CKUserPostList> mLvPost;
    private Context mContext;

    public OrgPostListAdapter(Context context, List<CKUserPostList> postList) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvPost = postList;
    }

    @Override
    public int getCount() {
        return mLvPost.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvPost.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;

        if (view == null) {
            view = mInflater.inflate(R.layout.ck_org_post_list_item, null);
            holder = new ViewHolder();
            holder.mOrgPostImgIcon = (ImageView) view.findViewById(R.id.org_post_img_icon);
            holder.mOrgPostTxtTime = (TextView) view.findViewById(R.id.org_post_txt_time);
            holder.mOrgPostTxtName = (TextView) view.findViewById(R.id.org_post_txt_name);
            holder.mOrgPostTxtComment = (TextView) view.findViewById(R.id.org_post_txt_comment);
            holder.mOrgPostImgPhoto = (NineGridImageView) view.findViewById(R.id.org_post_img_photo);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

//      填充数据
        final CKUserPostList postItem = (CKUserPostList) getItem(position);
        holder.mOrgPostTxtTime.setText(postItem.getPost_time());
        holder.mOrgPostTxtName.setText(postItem.getUser_nick_name());
        holder.mOrgPostTxtComment.setText(postItem.getPost_comment());
        ImageLoader.getInstance().displayImage(postItem.getUser_head_photo_url(), holder.mOrgPostImgIcon, ImageLoaderOptionsUtils.orgPostOptions());

        //帖子图片组Adapter
        NineGridImageViewAdapter<PostPicUrlInfo> mAdapter = new NineGridImageViewAdapter<PostPicUrlInfo>() {
            @Override
            protected void onDisplayImage(Context context, ImageView imageView, PostPicUrlInfo s) {
                imageView.setClickable(false);
                ImageLoader.getInstance().displayImage(s.pic_url, imageView, ImageOptionsUtils.getOptions());
            }
        };

        holder.mOrgPostImgPhoto.setAdapter(mAdapter);
        holder.mOrgPostImgPhoto.setImagesData(postItem.getFirst_post_thumbnail_pic_url());
        holder.mOrgPostImgPhoto.setClickable(false);
        return view;
    }

    static class ViewHolder {
        ImageView mOrgPostImgIcon;//发帖人头像
        TextView mOrgPostTxtTime;//发帖时间
        TextView mOrgPostTxtName;//发帖人昵称
        TextView mOrgPostTxtComment;//发帖内容
        NineGridImageView mOrgPostImgPhoto;//帖子图片列表
    }
}
