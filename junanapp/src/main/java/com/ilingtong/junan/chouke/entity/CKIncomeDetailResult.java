package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 10:34
 * Email: liuting@ilingtong.com
 * Desc:7012接口(取得组织业绩组织酬金明细) 返回json结构
 */
public class CKIncomeDetailResult extends BaseResult implements Serializable {
    private CKIncomeDetailList body;//酬金明细信息

    public CKIncomeDetailList getBody() {
        return body;
    }

    public void setBody(CKIncomeDetailList body) {
        this.body = body;
    }
}
