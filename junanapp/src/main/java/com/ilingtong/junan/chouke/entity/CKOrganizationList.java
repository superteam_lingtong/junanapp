package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 15:01
 * Email: liuting@ilingtong.com
 * Desc:use by 7001(获取我的基本信息)、7002接口(获取我的关注组织列表) 组织信息
 */
public class CKOrganizationList {
    private String org_id;//当前默认组织ID
    private String org_name;//当前默认组织名称
    private String org_summary;//组织简介
    private String org_photo_url;//组织图片
    private String latest_post_info;//最近发帖内容
    private String post_update_time;//帖子最新更新时间
    private String org_favorited_by_me;//是否被当前用户加入
    private String is_org_head;//是否是组织创建者  0:是 1:否

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getOrg_summary() {
        return org_summary;
    }

    public void setOrg_summary(String org_summary) {
        this.org_summary = org_summary;
    }

    public String getOrg_photo_url() {
        return org_photo_url;
    }

    public void setOrg_photo_url(String org_photo_url) {
        this.org_photo_url = org_photo_url;
    }

    public String getLatest_post_info() {
        return latest_post_info;
    }

    public void setLatest_post_info(String latest_post_info) {
        this.latest_post_info = latest_post_info;
    }

    public String getPost_update_time() {
        return post_update_time;
    }

    public void setPost_update_time(String post_update_time) {
        this.post_update_time = post_update_time;
    }

    public String getOrg_favorited_by_me() {
        return org_favorited_by_me;
    }

    public void setOrg_favorited_by_me(String org_favorited_by_me) {
        this.org_favorited_by_me = org_favorited_by_me;
    }

    public String getIs_org_head() {
        return is_org_head;
    }

    public void setIs_org_head(String is_org_head) {
        this.is_org_head = is_org_head;
    }
}
