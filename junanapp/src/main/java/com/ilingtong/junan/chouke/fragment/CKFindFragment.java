package com.ilingtong.junan.chouke.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.Intents;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.activity.OrganizationDetailActivity;
import com.ilingtong.junan.chouke.adapter.SearchOrgListAdapter;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectExpertDetailActivity;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.activity.CollectMStoreDetailActivity;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.activity.ProductTicketDetailActivity;
import com.ilingtong.library.tongle.fragment.BaseFragment;
import com.ilingtong.library.tongle.protocol.ExpertBaseResult;
import com.ilingtong.library.tongle.protocol.OrganizationListItem;
import com.ilingtong.library.tongle.protocol.QRData;
import com.ilingtong.library.tongle.protocol.ScanDataResult;
import com.ilingtong.library.tongle.protocol.SearchResult;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * author: liuting
 * Date: 2016/5/25
 * Time: 17:47
 * Email: liuting@ilingtong.com
 * Desc:发现
 */
public class CKFindFragment extends BaseFragment implements View.OnClickListener {
    private ImageView mFindImgQr;//扫码图标
    private SearchView mCkFindSvSearch;//搜索框
    private ListView mFindLvOrg;//搜索结果列表

    private SearchOrgListAdapter mSearchOrgListAdapter;//搜索组织Adapter
    private ProgressDialog mPDialog;//提示框

    public static final String SEARCH_ORG_TYPE = "4";//搜索类型为组织
    private ArrayList<OrganizationListItem> org_list;//组织列表
    final int FIND_SCAN_CODE = 0;//发现扫码请求码
    private final int REQUEST_LIST = 0;//请求数据
    private final int NO_DATA_REQUEST = 1;//无请求数据
    private String mFindKey;//搜索关键字
    private TextView mTxtSearch;//搜索框文本
    private String[] needPermissions = {Manifest.permission.CAMERA};//相机权限，扫码所需
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REQUEST_LIST://有数据时显示列表
                    mFindLvOrg.setVisibility(View.VISIBLE);
                    mSearchOrgListAdapter.notifyDataSetChanged();
                    break;
                case NO_DATA_REQUEST://无数据时隐藏列表，提示无相关信息
                    mFindLvOrg.setVisibility(View.GONE);
                    ToastUtils.toastShort(getActivity().getResources().getString(R.string.layout_replace_txt));
                    break;
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ck_fragment_find, null);
        initView(view);
        return view;
    }

    /**
     * 初始化控件
     *
     * @param view
     */
    public void initView(View view) {
        org_list = new ArrayList<>();
        org_list.clear();

        mFindImgQr = (ImageView) view.findViewById(R.id.find_img_qr);
        mCkFindSvSearch = (SearchView) view.findViewById(R.id.ck_find_sv_search);
        mFindLvOrg = (ListView) view.findViewById(R.id.find_lv_org);
        mFindImgQr.setOnClickListener(this);
        if(mCkFindSvSearch==null){
            return;
        }
        else{
            int id=mCkFindSvSearch.getContext().getResources().getIdentifier("android:id/search_src_text",null,null);
            mTxtSearch = (TextView) mCkFindSvSearch.findViewById(id);
            mTxtSearch.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);//14sp
            mTxtSearch.setTextColor(getActivity().getResources().getColor(R.color.ticket_detail_refund_pre_txt_color));
            mTxtSearch.setHintTextColor(getActivity().getResources().getColor(R.color.ck_find_search_hint_color));
            mTxtSearch.addTextChangedListener(textChange);

            int imgId=mCkFindSvSearch.getContext().getResources().getIdentifier("android:id/search_mag_icon",null,null);
            ImageView searchButton = (ImageView)mCkFindSvSearch.findViewById(imgId);
            searchButton.setImageResource(R.mipmap.ck_icon_search);
            mCkFindSvSearch.setIconifiedByDefault(false);
        }
        mCkFindSvSearch.setOnQueryTextListener(queryTextListener);

        setSearchViewBackground(mCkFindSvSearch);

        mPDialog = new ProgressDialog(getActivity());
        mFindLvOrg.setVisibility(View.GONE);//默认隐藏列表
        mSearchOrgListAdapter = new SearchOrgListAdapter(getActivity(), org_list);
        mFindLvOrg.setAdapter(mSearchOrgListAdapter);
        mFindLvOrg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                OrganizationDetailActivity.launcher(CKFindFragment.this, org_list.get(position).org_id);
            }
        });
    }

    //搜索本文改变监听
    private TextWatcher textChange=new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            //搜索框无内容时，清空列表
            if (TextUtils.isEmpty(mTxtSearch.getText().toString())) {
                org_list.clear();
                mSearchOrgListAdapter.notifyDataSetChanged();
            }
        }
    };

    //搜索框请求监听
    private SearchView.OnQueryTextListener queryTextListener= new SearchView.OnQueryTextListener() {

        public boolean onQueryTextSubmit(String query) {
            mFindKey = query;
            doSearchRequest(mFindKey);//搜索请求
            return true;
        }

        public boolean onQueryTextChange(String newText) {
            return true;
        }
    };

    /**
     * android4.0 SearchView去掉（修改）搜索框的背景 修改光标
     */
    public void setSearchViewBackground(SearchView search) {
        try {
            Class<?> argClass = search.getClass();
            // 指定某个私有属性
            Field ownField = argClass.getDeclaredField("mSearchPlate"); // 注意mSearchPlate的背景是stateListDrawable(不同状态不同的图片)
            // 所以不能用BitmapDrawable
            // setAccessible 它是用来设置是否有权限访问反射类中的私有属性的，只有设置为true时才可以访问，默认为false
            ownField.setAccessible(true);
            View mView = (View) ownField.get(search);
            mView.setBackgroundDrawable(getResources().getDrawable(
                    com.ilingtong.library.tongle.R.drawable.input_bk));

            // 指定某个私有属性
            Field mQueryTextView = argClass.getDeclaredField("mQueryTextView");
            mQueryTextView.setAccessible(true);
            Class<?> mTextViewClass = mQueryTextView.get(search).getClass()
                    .getSuperclass().getSuperclass().getSuperclass();

            // mCursorDrawableRes光标图片Id的属性
            // 这个属性是TextView的属性，所以要用mQueryTextView（SearchAutoComplete）的父类（AutoCompleteTextView）的父
            // 类( EditText）的父类(TextView)
            Field mCursorDrawableRes = mTextViewClass
                    .getDeclaredField("mCursorDrawableRes");

            // setAccessible 它是用来设置是否有权限访问反射类中的私有属性的，只有设置为true时才可以访问，默认为false
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(mQueryTextView.get(search),
                    com.ilingtong.library.tongle.R.drawable.input_line);// 注意第一个参数持有这个属性(mQueryTextView)的对象(mSearchView)
            // 光标必须是一张图片不能是颜色，因为光标有两张图片，一张是第一次获得焦点的时候的闪烁的图片，一张是后边有内容时候的图片，如果用颜色填充的话，就会失去闪烁的那张图片，颜色填充的会缩短文字和光标的距离（某些字母会背光标覆盖一部分）。
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 调用接口，请求数据
     *
     * @param find_key
     */
    private void doSearchRequest(String find_key) {
        org_list.clear();
        //提示正在检索
        mPDialog.setTitle(getString(com.ilingtong.library.tongle.R.string.search_dialog_title));
        mPDialog.setMessage(getString(com.ilingtong.library.tongle.R.string.search_dialog_message));
        mPDialog.show();

        ServiceManager.searchRequest(TongleAppInstance.getInstance().getUserID(), find_key, SEARCH_ORG_TYPE, searchSuccessListener(), errorListener());
    }

    /**
     * 请求成功
     *
     * @return
     */
    private Response.Listener searchSuccessListener() {
        return new Response.Listener<SearchResult>() {
            @Override
            public void onResponse(SearchResult searchResulte) {
                if (TongleAppConst.SUCCESS.equals(searchResulte.getHead().getReturn_flag())) {

                    if (searchResulte.getBody().getOrganization_list().size() > 0) {
                        org_list.addAll(searchResulte.getBody().getOrganization_list());
                        handler.sendEmptyMessage(REQUEST_LIST);
                    } else {
                        handler.sendEmptyMessage(NO_DATA_REQUEST);
                    }

                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + searchResulte.getHead().getReturn_message());
                }
                mPDialog.dismiss();
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
                mPDialog.dismiss();
            }
        };
    }

    /**
     * 二维码扫描请求
     *
     * @return
     */
    private Response.Listener<ScanDataResult> codeSuccessListener() {
        return new Response.Listener<ScanDataResult>() {
            @Override
            public void onResponse(ScanDataResult response) {
                mPDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    QRData qrData = response.getBody().getQRData();
                    codeJump(qrData);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 请求数据
     *
     * @return
     */
    private Response.Listener<ExpertBaseResult> ExpertBaseListener() {
        return new Response.Listener<ExpertBaseResult>() {
            @Override
            public void onResponse(ExpertBaseResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    Intent expertIntent = new Intent(getActivity(), CollectExpertDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("user_id", response.getBody().user_id);
                    bundle.putString("user_favorited_by_me", response.getBody().user_favorited_by_me);
                    expertIntent.putExtras(bundle);
                    startActivity(expertIntent);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 二维码跳转
     *
     * @param qrData
     */
    private void codeJump(QRData qrData) {
        if (!TextUtils.isEmpty(qrData.code_type)) {
            //达人详情
            if (TongleAppConst.STREXPERT.equals(qrData.code_type)) {
                ServiceManager.doExpertBaseRequest(qrData.user_id, ExpertBaseListener(), errorListener());
                //帖子详情
            } else if (TongleAppConst.STRFORUM.equals(qrData.code_type)) {
                Intent forumIntent = new Intent(getActivity(), CollectForumDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("post_id", qrData.post_id);
                forumIntent.putExtras(bundle);
                startActivity(forumIntent);
                //魔店详情
            } else if (TongleAppConst.STRSTORE.equals(qrData.code_type)) {
                Intent intent = new Intent(getActivity(), CollectMStoreDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("mstore_id", qrData.store_id);
                intent.putExtras(bundle);
                startActivity(intent);
                //产品详情
            } else if (TongleAppConst.STRPRODUCT.equals(qrData.code_type)) {
                CollectProductDetailActivity.launch(getActivity(), qrData.prod_id, TongleAppConst.ACTIONID_SCAN_CODE, qrData.relation_id, "", "");
                //团购商品详情
            } else if (TongleAppConst.STRGROUP_PRODUCT.equals(qrData.code_type)) {
                ProductTicketDetailActivity.launch(getActivity(), qrData.prod_id, TongleAppConst.ACTIONID_SCAN_CODE, qrData.relation_id, "", "");
            } else if (TongleAppConst.STRORGANIZATION.equals(qrData.code_type)) {//组织详情
                OrganizationDetailActivity.launcher(CKFindFragment.this, qrData.org_id);
            }
        }
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.find_img_qr) {//请求扫码
            requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(getActivity(), needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                        @Override
                        public void requestSuccess() {
                            Intent intent = new Intent(getActivity(), CaptureActivity.class);
                            intent.setAction(Intents.Scan.ACTION);
                            intent.putExtra(Intents.Scan.MODE, Intents.Scan.QR_CODE_MODE);
                            intent.putExtra(Intents.Scan.RESULT_DISPLAY_DURATION_MS, 0L);
                            intent.putExtra("TopName", getString(com.ilingtong.library.tongle.R.string.find));
                            startActivityForResult(intent, FIND_SCAN_CODE);
                        }

                        @Override
                        public void requestFail() {
                            SelectDialog dialog = new SelectDialog(getActivity(), getString(com.ilingtong.library.tongle.R.string.please_open_camera_permission));
                            dialog.show();
                        }
                    });
            requestPermissionUtils.checkPermissions(CKFindFragment.this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (FIND_SCAN_CODE == requestCode) {
            if (resultCode == RESULT_OK) {
                Bundle b = data.getExtras();
                String qrcode = b.getString("SCAN_RESULT");
                if (!TextUtils.isEmpty(qrcode)) {
                    //提示正在解析
                    mPDialog.show();
                    mPDialog.setTitle(null);
                    mPDialog.setMessage(getString(com.ilingtong.library.tongle.R.string.fragment_find_dialog_message));
                    ServiceManager.doScanDataRequest(TongleAppInstance.getInstance().getUserID(), qrcode, codeSuccessListener(), errorListener());
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}