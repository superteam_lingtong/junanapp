package com.ilingtong.junan.chouke.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.adapter.RetireTypeListAdapter;
import com.ilingtong.junan.chouke.entity.ArmInfo;
import com.ilingtong.junan.chouke.entity.OfficerInfo;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.List;

/**
 * Package:com.ilingtong.junan.chouke.widget
 * author:liuting
 * Date:2017/4/13
 * Desc:军种类型选择Dialog
 */

public class RetireSelectDialog extends Dialog implements View.OnClickListener{
    private ListView lvList;//列表
    private Button btnCancel;//取消
    private Button btnSure;//确定
    private IOnClickListener listener;//事件监听
    private RetireTypeListAdapter retireTypeListAdapter;//军种类型列表Adapter
    private Context context;//context
    private List<ArmInfo> listArm;//兵种列表
    private List<OfficerInfo> listOfficer;//类型列表
    private int mIndex = -1;//选中的选项index

    public RetireSelectDialog(Context context) {
        super(context);
    }

    /**
     * 构造函数
     *
     * @param context       Context
     * @param listArm       军种列表
     * @param listOfficer   类型列表
     * @param clickListener 事件监听
     */
    public RetireSelectDialog(Context context,List<ArmInfo> listArm,List<OfficerInfo> listOfficer, IOnClickListener clickListener) {
        super(context, com.ilingtong.library.tongle.R.style.select_dialog_style);
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.CENTER;
        window.setAttributes(params);
        this.context = context;
        this.listener = clickListener;
        this.listArm = listArm;
        this.listOfficer = listOfficer;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ja_dialog_retire_select_layout);
        initView();
    }

    /**
     * 初始化控件
     */
    private void initView() {
        lvList = (ListView) findViewById(R.id.retire_select_lv_list);
        btnCancel = (Button) findViewById(R.id.retire_select_btn_cancel);
        btnSure = (Button) findViewById(R.id.retire_select_btn_sure);
        retireTypeListAdapter = new RetireTypeListAdapter(context, new RetireTypeListAdapter.IOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mIndex = position;
                for (int i = 0; i < lvList.getCount(); i++) {
                    View convertView = lvList.getChildAt(i);
                    ImageView imgCheck = (ImageView) convertView.findViewById(R.id.retire_type_img_check);
                    TextView tvName = (TextView)convertView.findViewById(R.id.retire_type_tv_name);
                    tvName.setEnabled(false);
                    imgCheck.setVisibility(View.INVISIBLE);
                }
                ImageView imgCheck = (ImageView) view.findViewById(R.id.retire_type_img_check);
                TextView tvName = (TextView)view.findViewById(R.id.retire_type_tv_name);
                imgCheck.setVisibility(View.VISIBLE);
                tvName.setEnabled(true);
            }
        },listArm,listOfficer);
        lvList.setAdapter(retireTypeListAdapter);
        btnCancel.setOnClickListener(this);
        btnSure.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(listener != null){
            switch (view.getId()){
                case R.id.retire_select_btn_cancel://取消
                    listener.onCancelListener(view);
                    break;
                case R.id.retire_select_btn_sure://确认
                    if(mIndex == -1){
                        ToastUtils.toastShort(R.string.retire_type_select_null);
                    }else{
                        listener.onSureListener(view,mIndex);
                    }
                    break;
            }
        }
    }

    public interface IOnClickListener {
        void onCancelListener(View view);//取消事件

        void onSureListener(View view, int position);//确认事件
    }
}

