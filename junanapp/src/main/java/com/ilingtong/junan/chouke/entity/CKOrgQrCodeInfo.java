package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 9:21
 * Email: liuting@ilingtong.com
 * Desc:7007接口(获取组织邀请二维码)的body 组织邀请二维码信息
 */
public class CKOrgQrCodeInfo {
    private String org_qr_code_url;//二维码图片URL

    public String getOrg_qr_code_url() {
        return org_qr_code_url;
    }

    public void setOrg_qr_code_url(String org_qr_code_url) {
        this.org_qr_code_url = org_qr_code_url;
    }
}
