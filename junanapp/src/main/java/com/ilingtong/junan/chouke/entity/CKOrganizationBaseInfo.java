package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 15:40
 * Email: liuting@ilingtong.com
 * Desc:used by 7003接口(获取指定组织的详情) 组织基本信息
 */
public class CKOrganizationBaseInfo {
    private String org_id;//组织ID
    private String org_name;//组织名称
    private String org_summary;//组织简介
    private String org_photo_url;//组织图片
    private int org_member_count;//组织成员数量
    private int org_post_count;//组织发帖总数量
    private String is_join_in;//是否加入当前组织 0:是 1:否
    private String is_default_org;//是否是默认组织 0:是 1:否
    private String protocol_url;//组织协议URL

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getOrg_summary() {
        return org_summary;
    }

    public void setOrg_summary(String org_summary) {
        this.org_summary = org_summary;
    }

    public String getOrg_photo_url() {
        return org_photo_url;
    }

    public void setOrg_photo_url(String org_photo_url) {
        this.org_photo_url = org_photo_url;
    }

    public int getOrg_member_count() {
        return org_member_count;
    }

    public void setOrg_member_count(int org_member_count) {
        this.org_member_count = org_member_count;
    }

    public int getOrg_post_count() {
        return org_post_count;
    }

    public void setOrg_post_count(int org_post_count) {
        this.org_post_count = org_post_count;
    }

    public String getIs_join_in() {
        return is_join_in;
    }

    public void setIs_join_in(String is_join_in) {
        this.is_join_in = is_join_in;
    }

    public String getIs_default_org() {
        return is_default_org;
    }

    public void setIs_default_org(String is_default_org) {
        this.is_default_org = is_default_org;
    }

    public String getProtocol_url() {
        return protocol_url;
    }

    public void setProtocol_url(String protocol_url) {
        this.protocol_url = protocol_url;
    }
}
