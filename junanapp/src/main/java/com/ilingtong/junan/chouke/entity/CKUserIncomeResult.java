package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 10:55
 * Email: liuting@ilingtong.com
 * Desc:7013接口(取得我的个人业绩明细)返回json结构
 */
public class CKUserIncomeResult extends BaseResult implements Serializable {
    private CKUserIncomeList body;//个人业绩明细信息

    public CKUserIncomeList getBody() {
        return body;
    }

    public void setBody(CKUserIncomeList body) {
        this.body = body;
    }
}
