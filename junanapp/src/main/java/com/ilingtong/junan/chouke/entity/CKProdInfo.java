package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 16:03
 * Email: liuting@ilingtong.com
 * Desc:used by 7003接口(获取指定组织的详情) 组织推荐商品信息
 */
public class CKProdInfo {
    private int prod_count;//推荐商品数量
    private String prod_referee_url;//组织推荐商品URL  add 2016/5/19
    private String prod_referee_title;//组织推荐商品标题  add 2016/5/19

    public int getProd_count() {
        return prod_count;
    }

    public void setProd_count(int prod_count) {
        this.prod_count = prod_count;
    }

    public String getProd_referee_url() {
        return prod_referee_url;
    }

    public void setProd_referee_url(String prod_referee_url) {
        this.prod_referee_url = prod_referee_url;
    }

    public String getProd_referee_title() {
        return prod_referee_title;
    }

    public void setProd_referee_title(String prod_referee_title) {
        this.prod_referee_title = prod_referee_title;
    }
}
