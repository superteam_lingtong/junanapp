package com.ilingtong.junan.chouke.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.Intents;
import com.ilingtong.junan.chouke.CKConstant;
import com.ilingtong.junan.chouke.CKServiceManager;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.entity.CKPayoffURLResult;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.BaseActivity;
import com.ilingtong.library.tongle.protocol.CodeBaseResult;
import com.ilingtong.library.tongle.protocol.CodeResult;
import com.ilingtong.library.tongle.protocol.RegistInputParam;
import com.ilingtong.library.tongle.protocol.RegistResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.Utils;
import com.ilingtong.library.tongle.widget.SelectDialog;

/**
 * author: liuting
 * Date: 2016/5/27
 * Time: 15:49
 * Email: liuting@ilingtong.com
 * Desc:注册
 */
public class CKRegisterActivity extends BaseActivity implements View.OnClickListener {
    private EditText mEdtPhone;//手机号
    private TextView mTvCode;//获取验证码
    private EditText mEdtCode;//验证码编辑框
    private EditText mEdtPassword;//密码
    private TextView mTvRecommend;//扫描推荐码
    private EditText mEdtRecommend;//推荐码
    private CheckBox mCbProtocol;//协议选择框
    private LinearLayout mLlyProtocol;//协议
    private Button mBtnRegister;//注册按钮

    private ImageView mImgBack;//返回
    private TextView mTxtTitle;//标题

    private Dialog mDialog;//加载对话框

    public static final int AGREE_PROTOCOL_REQUEST = 3001;//获取协议请求码
    public static final int REGISTER_SCAN_CODE = 3002;//扫码请求码

    private String TAG = "CKRegisterActivity";
    private String mPhone;//手机号
    private String mPassword;//密码
    private String mCode;//验证码

    private String[] needPermissions = {Manifest.permission.CAMERA};//相机权限，扫码所需
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ck_activity_register);

        initView();
    }

    /**
     * 初始化控件
     */
    private void initView() {
        mDialog = DialogUtils.createLoadingDialog(CKRegisterActivity.this);
        mDialog.setCancelable(true);

        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgBack.setVisibility(View.VISIBLE);
        mTxtTitle.setText(R.string.regist);
        mImgBack.setOnClickListener(this);

        mEdtPhone = (EditText) findViewById(R.id.register_edt_phone);
        mTvCode = (TextView) findViewById(R.id.register_tv_code);
        mEdtCode = (EditText) findViewById(R.id.register_edt_code);
        mEdtPassword = (EditText) findViewById(R.id.register_edt_password);
        mTvRecommend = (TextView) findViewById(R.id.register_tv_recommend);
        mEdtRecommend = (EditText) findViewById(R.id.register_edt_recommend);
        mCbProtocol = (CheckBox) findViewById(R.id.register_cb_protocol);
        mLlyProtocol = (LinearLayout) findViewById(R.id.register_lly_protocol);
        mBtnRegister = (Button) findViewById(R.id.register_btn_register);
        mBtnRegister.setOnClickListener(this);
        mTvCode.setOnClickListener(this);
        mTvRecommend.setOnClickListener(this);
        mLlyProtocol.setOnClickListener(this);
        initChange();
    }

    /**
     * 文本框改变监听
     */
    public void initChange() {
        //做手机号的改变监听，判断底部注册按钮的背景
        mEdtPhone.addTextChangedListener(textChange);
        //做密码的改变监听，判断底部注册按钮的背景
        mEdtPassword.addTextChangedListener(textChange);
        //做验证码的改变监听，判断底部注册按钮的背景
        mEdtCode.addTextChangedListener(textChange);
    }

    //文本框的改变监听，判断底部登录按钮的背景
    private TextWatcher textChange= new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            mPhone = mEdtPhone.getText().toString();
            mPassword = mEdtPassword.getText().toString();
            mCode = mEdtCode.getText().toString();

            if (mPhone.equals("") || mPassword.equals("") || mCode.equals("")) {
                mBtnRegister.setEnabled(false);
            } else {
                mBtnRegister.setEnabled(true);
            }
        }
    };

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.left_arrow_btn) {
            finish();

        } else if (i == R.id.register_btn_register) {//注册
            mPhone = mEdtPhone.getText().toString();
            mPassword = mEdtPassword.getText().toString();
            mCode = mEdtCode.getText().toString();

            //手机号验证
            if ((!Utils.isMobileNO(mPhone) || (mPhone.length() != TongleAppConst.PHONE_NUMBER_LENGTH))) {
                ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.regist_phone_error));
                return;
            }

            //验证码长度验证
            if (mCode.length() != TongleAppConst.CODE_LENGTH) {
                ToastUtils.toastShort(getString(R.string.ck_register_code_error));
                return;
            }
            //密码长度验证
            if (mPassword.length() < TongleAppConst.PASSWORD_LENGTH) {
                ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.regist_password_error));
                return;
            }
            //没有接受用户协议
            if (mCbProtocol.isChecked() == false) {
                ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.regist_agreement_nocheck));
                return;
            }

            if (!"".equals(mPhone) && !"".equals(mPassword) && !"".equals(mCode)) {
                mDialog.show();
                RegistInputParam param = new RegistInputParam();
                param.phone_number = mPhone;
                param.password = mPassword;
                param.verification_code = mCode;
                param.user_nick_name = "";
                param.channel_no = "";
                param.mobile_os_version = TongleAppConst.ANDROID_PHONE;
                param.terminal_device = "";
                param.recommend_code = mEdtRecommend.getText().toString();

                ServiceManager.doRegistRequest(param, registerListener(), errorListener());
            } else {
                ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.register_txt_null));
            }

        } else if (i == R.id.register_tv_code) {//获取验证码
            mPhone = mEdtPhone.getText().toString();
            if (TextUtils.isEmpty(mPhone) || !Utils.isMobileNO(mPhone) || (mPhone.length() != TongleAppConst.PHONE_NUMBER_LENGTH)) {
                ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.regist_phone_error));
                return;
            }
            if (mTvCode.isEnabled()) {
                timer.start();
                ServiceManager.doCodeRequest(mEdtPhone.getText().toString(), codeListener(), errorListener());
            }

        } else if (i == R.id.register_tv_recommend) {//扫描推荐码
            requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(CKRegisterActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                        @Override
                        public void requestSuccess() {
                            Intent intent = new Intent(CKRegisterActivity.this, CaptureActivity.class);
                            intent.setAction(Intents.Scan.ACTION);
                            intent.putExtra(Intents.Scan.MODE, Intents.Scan.QR_CODE_MODE);
                            intent.putExtra(Intents.Scan.RESULT_DISPLAY_DURATION_MS, 0L);
                            intent.putExtra("TopName", getString(com.ilingtong.library.tongle.R.string.regist));
                            startActivityForResult(intent, REGISTER_SCAN_CODE);
                        }

                        @Override
                        public void requestFail() {
                            SelectDialog dialog = new SelectDialog(CKRegisterActivity.this, getString(com.ilingtong.library.tongle.R.string.please_open_camera_permission));
                            dialog.show();
                        }
                    });
            requestPermissionUtils.checkPermissions(CKRegisterActivity.this);

        } else if (i == R.id.register_lly_protocol) {//协议
            mDialog.show();
            CKServiceManager.getPayoffProtocolURL(successListener(),errorListener());
//            RegisterProtocolActivity.launcher(CKRegisterActivity.this,"", CKConstant.REGISTER_INTO_TYPE,AGREE_PROTOCOL_REQUEST);
        }
    }

    //获取验证码计时
    private CountDownTimer timer = new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            mTvCode.setEnabled(false);
            mTvCode.setText((millisUntilFinished / 1000) + getString(com.ilingtong.library.tongle.R.string.common_validat_btn_txt_before));
        }

        @Override
        public void onFinish() {
            mTvCode.setEnabled(true);
            mTvCode.setText(getString(com.ilingtong.library.tongle.R.string.common_validat_btn_txt_after));
        }
    };

    /**
     * 获取协议请求成功
     *
     * @return
     */
    private Response.Listener<CKPayoffURLResult> successListener() {
        return new Response.Listener<CKPayoffURLResult>() {
            @Override
            public void onResponse(CKPayoffURLResult response) {
                mDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    String url = response.getBody().getPayoff_protocol_url()+"?user_id=''&user_token=''&app_inner_no="+ TongleAppInstance.getInstance().getApp_inner_no();
                    if (!TextUtils.isEmpty(url)){
                        RegisterProtocolActivity.launcher(CKRegisterActivity.this,url, CKConstant.REGISTER_INTO_TYPE,AGREE_PROTOCOL_REQUEST);
                    }
                } else {
                    ToastUtils.toastLong(getResources().getString(com.ilingtong.library.tongle.R.string.para_exception)+ response.getHead().getReturn_message());
                }
            }
        };
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AGREE_PROTOCOL_REQUEST) {
            if (resultCode == RESULT_OK) {
                //同意协议结果返回，勾选同意协议
                mCbProtocol.setChecked(true);
            }
        } else if (REGISTER_SCAN_CODE == requestCode) {
            if (resultCode == RESULT_OK) {
                Bundle b = data.getExtras();
                String qrcode = b.getString("SCAN_RESULT");
                if (qrcode != null) {
                    mDialog.show();
                    ServiceManager.scanUserDecode(qrcode, CodeBaseRequest(), errorListener());
                    Log.e(TAG, qrcode);
                } else {
                    Log.e(TAG, getString(com.ilingtong.library.tongle.R.string.register_get_qrcode_error));
                }
            }
        }
    }

    /**
     * 功能：会员个人二维码解析响应成功，返回数据
     */
    private Response.Listener<CodeBaseResult> CodeBaseRequest() {
        return new Response.Listener<CodeBaseResult>() {
            @Override
            public void onResponse(CodeBaseResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mEdtRecommend.setText(response.getBody().getUser_data().phone);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：获取验证码网络响应成功，返回数据
     */
    private Response.Listener<CodeResult> codeListener() {
        return new Response.Listener<CodeResult>() {
            @Override
            public void onResponse(CodeResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.common_get_code_success));
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.common_get_code_error) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 请求成功：注册
     *
     * @return
     */
    private Response.Listener<RegistResult> registerListener() {
        return new Response.Listener<RegistResult>() {
            @Override
            public void onResponse(RegistResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.ck_register_success_msg));
                    finish();
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }

}
