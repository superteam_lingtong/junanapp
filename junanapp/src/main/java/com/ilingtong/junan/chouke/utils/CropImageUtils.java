package com.ilingtong.junan.chouke.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.FileNotFoundException;

/**
 * Created by wuqian on 2016/8/19.
 * mail: wuqian@ilingtong.com
 * Description:图片裁剪
 */
public class CropImageUtils {
    /**
     * 裁剪图片
     *
     * @param activity
     * @param uri         需要裁剪的原图Uri
     * @param requestCode
     * @param aspectX     裁剪框X比例
     * @param aspectY     裁剪框Y比例
     * @param outputX     输出图片size
     * @param outputY     输出图片size
     * @param imageUri    裁剪后图片Uri
     */
    public static void crop(Activity activity, Uri uri, int requestCode, int aspectX, int aspectY, int outputX, int outputY, Uri imageUri) {
        // 裁剪图片意图
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 裁剪框的比例
        intent.putExtra("aspectX", aspectX);
        intent.putExtra("aspectY", aspectY);
        // 裁剪后输出图片的尺寸大小
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        // 图片格式
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);// 取消人脸识别
        intent.putExtra("return-data", false);// true:不返回uri，false：返回uri
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);  // 图片裁剪后保存的地方

        activity.startActivityForResult(intent, requestCode);
    }
    public static void crop(Activity activity, Uri uri, int requestCode, Uri imageUri) {
        crop(activity,uri,requestCode,8,5,400,250,imageUri);
    }
    /**
     * 将uri转化成Bitmap
     * @param context
     * @param uri
     * @return
     */
    public static Bitmap decodeUriAsBitmap(Context context,Uri uri) {
        Bitmap bitmap = null;
        try {
            // 先通过getContentResolver方法获得一个ContentResolver实例，
            // 调用openInputStream(Uri)方法获得uri关联的数据流stream
            // 把上一步获得的数据流解析成为bitmap
            bitmap = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return bitmap;
    }
}
