package com.ilingtong.junan.chouke.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.Intents;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.entity.JARegisterParam;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.BaseActivity;
import com.ilingtong.library.tongle.protocol.CodeBaseResult;
import com.ilingtong.library.tongle.protocol.CodeResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.Utils;
import com.ilingtong.library.tongle.widget.SelectDialog;

/**
 * Created by wuqian on 2016/7/20.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class JARegisterActivity extends BaseActivity implements View.OnClickListener {
    private EditText jaRegisterEdtPhone;
    private TextView jaRegisterTvCode;
    private EditText jaRegisterEdtCode;
    private EditText jaRegisterEdtPassword;
    private Button jaRegisterBtnNext;
    private ImageView imgBack;
    private TextView txtTitle;
    private TextView txt_recommend;
    private EditText edt_recommend;

    private String phoneNumber;
    private String password;
    private String verificationCode;
    private JARegisterParam registerParam;

    public static JARegisterActivity instance = null;
    public static final int NEXTREQUESTCODE = 1000;
    private static final int SCANREQUESTCODE = 1001;

    private Dialog mDialog;

    private String[] needPermissions = {Manifest.permission.CAMERA};//相机权限，扫码所需
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ja_activity_register);
        registerParam = new JARegisterParam(JARegisterParam.SYORI_FLAG_REGIST);
        instance = this;
        initView();
    }

    private void initView() {
        mDialog = DialogUtils.createLoadingDialog(this);
        imgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        txtTitle = (TextView) findViewById(R.id.top_name);
        txtTitle.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        txtTitle.setText(R.string.regist);
        imgBack.setOnClickListener(this);

        jaRegisterEdtPhone = (EditText) findViewById(R.id.ja_register_edt_phone);
        jaRegisterTvCode = (TextView) findViewById(R.id.ja_register_tv_code);
        jaRegisterEdtCode = (EditText) findViewById(R.id.ja_register_edt_code);
        jaRegisterEdtPassword = (EditText) findViewById(R.id.ja_register_edt_password);
        jaRegisterBtnNext = (Button) findViewById(R.id.ja_register_btn_next);
        txt_recommend = (TextView) findViewById(R.id.ja_register_tv_recommend);
        edt_recommend = (EditText) findViewById(R.id.ja_register_edt_recommend);

        jaRegisterEdtPhone.addTextChangedListener(textChange);
        jaRegisterEdtPassword.addTextChangedListener(textChange);
        jaRegisterEdtCode.addTextChangedListener(textChange);
        jaRegisterTvCode.setOnClickListener(this);
        jaRegisterBtnNext.setOnClickListener(this);
        txt_recommend.setOnClickListener(this);


    }

    private TextWatcher textChange = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            phoneNumber = jaRegisterEdtPhone.getText().toString();
            password = jaRegisterEdtPassword.getText().toString();
            verificationCode = jaRegisterEdtCode.getText().toString();

            if (phoneNumber.equals("") || password.equals("") || verificationCode.equals("")) {
                jaRegisterBtnNext.setEnabled(false);
            } else {
                jaRegisterBtnNext.setEnabled(true);
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == NEXTREQUESTCODE) {
            registerParam = (JARegisterParam) data.getSerializableExtra("registerParam");
        } else if (resultCode == RESULT_OK && requestCode == SCANREQUESTCODE) {
            Bundle b = data.getExtras();
            String qrcode = b.getString("SCAN_RESULT");
            if (qrcode != null) {
                mDialog.show();
                ServiceManager.scanUserDecode(qrcode, CodeBaseRequest(), new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
                        mDialog.dismiss();
                    }
                });
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 功能：会员个人二维码解析响应成功，返回数据
     */
    private Response.Listener<CodeBaseResult> CodeBaseRequest() {
        return new Response.Listener<CodeBaseResult>() {
            @Override
            public void onResponse(CodeBaseResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    edt_recommend.setText(response.getBody().getUser_data().phone);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ja_register_btn_next:
                phoneNumber = jaRegisterEdtPhone.getText().toString();
                password = jaRegisterEdtPassword.getText().toString();
                verificationCode = jaRegisterEdtCode.getText().toString();

                //手机号验证
                if ((!Utils.isMobileNO(phoneNumber) || (phoneNumber.length() != TongleAppConst.PHONE_NUMBER_LENGTH))) {
                    ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.regist_phone_error));
                    return;
                }
                //验证码长度验证
                if (verificationCode.length() != TongleAppConst.CODE_LENGTH) {
                    ToastUtils.toastShort(getString(R.string.ck_register_code_error));
                    return;
                }
                //密码长度验证
                if (password.length() < TongleAppConst.PASSWORD_LENGTH) {
                    ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.regist_password_error));
                    return;
                }

                registerParam.phone = jaRegisterEdtPhone.getText().toString();
                registerParam.password = jaRegisterEdtPassword.getText().toString();
                registerParam.verification_code = jaRegisterEdtCode.getText().toString();
                registerParam.referee_id = edt_recommend.getText().toString();
                JARegisterIdCardInfoActivity.launch(this,registerParam);
                break;
            case R.id.ja_register_tv_code:
                phoneNumber = jaRegisterEdtPhone.getText().toString();
                if (TextUtils.isEmpty(phoneNumber) || !Utils.isMobileNO(phoneNumber) || (phoneNumber.length() != TongleAppConst.PHONE_NUMBER_LENGTH)) {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.regist_phone_error));
                    return;
                }
                if (jaRegisterTvCode.isEnabled()) {
                    timer.start();
                    ServiceManager.doCodeRequest(phoneNumber, codeListener(), new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
                        }
                    });
                    return;
                }
                break;
            case R.id.left_arrow_btn:
                finish();
                break;
            case R.id.ja_register_tv_recommend:
                //扫描推荐二维码
                requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(JARegisterActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                            @Override
                            public void requestSuccess() {
                                Intent intentScan = new Intent(JARegisterActivity.this, CaptureActivity.class);
                                intentScan.setAction(Intents.Scan.ACTION);
                                intentScan.putExtra(Intents.Scan.MODE, Intents.Scan.QR_CODE_MODE);
                                intentScan.putExtra(Intents.Scan.RESULT_DISPLAY_DURATION_MS, 0L);
                                intentScan.putExtra("TopName", getString(com.ilingtong.library.tongle.R.string.regist));
                                startActivityForResult(intentScan, SCANREQUESTCODE);
                            }

                            @Override
                            public void requestFail() {
                                SelectDialog dialog = new SelectDialog(JARegisterActivity.this, getString(com.ilingtong.library.tongle.R.string.please_open_camera_permission));
                                dialog.show();
                            }
                        });
                requestPermissionUtils.checkPermissions(JARegisterActivity.this);
                break;
        }
    }

    //获取验证码计时
    private CountDownTimer timer = new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            jaRegisterTvCode.setEnabled(false);
            jaRegisterTvCode.setText((millisUntilFinished / 1000) + getString(com.ilingtong.library.tongle.R.string.common_validat_btn_txt_before));
        }

        @Override
        public void onFinish() {
            jaRegisterTvCode.setEnabled(true);
            jaRegisterTvCode.setText(getString(com.ilingtong.library.tongle.R.string.common_validat_btn_txt_after));
        }
    };

    /**
     * 功能：获取验证码网络响应成功，返回数据
     */
    private Response.Listener<CodeResult> codeListener() {
        return new Response.Listener<CodeResult>() {
            @Override
            public void onResponse(CodeResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.common_get_code_success));
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.common_get_code_error) + response.getHead().getReturn_message());
                }
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}
