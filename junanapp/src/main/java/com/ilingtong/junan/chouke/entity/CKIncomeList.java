package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 9:44
 * Email: liuting@ilingtong.com
 * Desc:used by 7009接口(取得组织业绩组织酬金列表) 业绩列表
 */
public class CKIncomeList {
    private String org_id;//组织ID
    private String org_name;//组织名称
    private String org_summary;//组织简介
    private String org_photo_url;//组织图片
    private String income_update_time;//最后返利时间
    private double income;//收入金额

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getOrg_summary() {
        return org_summary;
    }

    public void setOrg_summary(String org_summary) {
        this.org_summary = org_summary;
    }

    public String getOrg_photo_url() {
        return org_photo_url;
    }

    public void setOrg_photo_url(String org_photo_url) {
        this.org_photo_url = org_photo_url;
    }

    public String getIncome_update_time() {
        return income_update_time;
    }

    public void setIncome_update_time(String income_update_time) {
        this.income_update_time = income_update_time;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }
}
