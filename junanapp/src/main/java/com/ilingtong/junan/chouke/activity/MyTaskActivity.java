package com.ilingtong.junan.chouke.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.junan.chouke.CKConstant;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.adapter.HomeNewsListAdapter;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.BaseActivity;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.ExpertUpdateRequestParam;
import com.ilingtong.library.tongle.protocol.ExpertUpdateResult;
import com.ilingtong.library.tongle.protocol.UserFollowPostList;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuqian on 2016/9/20.
 * mail: wuqian@ilingtong.com
 * Description: 我的任务列表
 */
public class MyTaskActivity extends BaseActivity implements XListView.IXListViewListener,View.OnClickListener{

    private TextView txtTitle;
    private ImageView imgBack;
    private XListView lvTask;
    private HomeNewsListAdapter taskListAdapter;
    private List<UserFollowPostList> mNewsList = new ArrayList<>();
    private boolean clearFlag;
    private Dialog mDialog;//加载对话框
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    taskListAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ck_activity_my_task);
        initView();
        getTaskList("",true);
    }

    /**
     * 初始化
     */
    private void initView(){
        clearFlag = true;

        mDialog = DialogUtils.createLoadingDialog(this);
        mDialog.setCancelable(false);
        mDialog.show();

        txtTitle = (TextView) findViewById(R.id.top_name);
        imgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        lvTask = (XListView) findViewById(R.id.ck_my_task_lv);

        txtTitle.setText(getString(R.string.my_posts));
        txtTitle.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setOnClickListener(this);

        taskListAdapter = new HomeNewsListAdapter(this,mNewsList);
        lvTask.setAdapter(taskListAdapter);
        lvTask.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent detailIntent = new Intent(MyTaskActivity.this, CollectForumDetailActivity.class);
                detailIntent.putExtra("post_id", mNewsList.get(position -1).post_id);
                startActivity(detailIntent);
            }
        });
    }
    private void getTaskList(String postid,boolean flag){
        clearFlag = flag;
        lvTask.setPullLoadEnable(false); //刷新时设置加载更多功能为false。避免刷新时帖子列表头端出现“加载更多"

        ExpertUpdateRequestParam param = new ExpertUpdateRequestParam();
        param.user_id = TongleAppInstance.getInstance().getUserID();
        param.forward = "1";
        param.post_id = postid;
        param.expert_user_id = CKConstant.HEADUSER;
        param.fetch_count = TongleAppConst.FETCH_COUNT;
        param.action = "1";
        param.magazine_type = "1";
        mDialog.show();
        ServiceManager.doExpertUpdateRequest(param, forumSuccessListener(), errorListener());
    }

    /**
     * 功能：请求帖子网络响应成功，返回数据
     */
    private Response.Listener<ExpertUpdateResult> forumSuccessListener() {
        return new Response.Listener<ExpertUpdateResult>() {
            @Override
            public void onResponse(ExpertUpdateResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    if (mNewsList != null && clearFlag) {
                        mNewsList.clear();
                    }
                    mNewsList.addAll(response.getBody().getUser_post_list());
                    if (response.getBody().getUser_post_list().size()>0){
                        SharedPreferences spUnreadPost = TongleAppInstance.getAppContext().getSharedPreferences("spUnreadPost", Context.MODE_PRIVATE);
                        spUnreadPost.edit().putString("lastPostId",response.getBody().getUser_post_list().get(0).post_id).commit();
                    }
                    if (mNewsList.size() < Integer.parseInt(response.getBody().getData_total_count())) {
                        lvTask.setPullLoadEnable(true);
                    } else {
                        lvTask.setPullLoadEnable(false);
                    }
                    mHandler.sendEmptyMessage(0);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }
    /**
     * 功能：请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
            }
        };
    }
    /***
     * 点击事件处理
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.left_arrow_btn:
                this.finish();
                break;
        }
    }

    @Override
    public void onRefresh(int id) {
        getTaskList("",true);
    }

    @Override
    public void onLoadMore(int id) {
        getTaskList(mNewsList.get(mNewsList.size() - 1).post_id, false);
    }
}
