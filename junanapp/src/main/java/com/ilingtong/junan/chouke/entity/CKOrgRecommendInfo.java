package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 15:55
 * Email: liuting@ilingtong.com
 * Desc:used by 7003接口(获取指定组织的详情) 组织推荐者信息
 */
public class CKOrgRecommendInfo {
    private String rec_user_id;//推荐者用户ID
    private String rec_user_name;//推荐者用户名称
    private String rec_user_phone;//推荐者用户手机
    private String rec_user_photo_url;//推荐者头像URL

    public String getRec_user_id() {
        return rec_user_id;
    }

    public void setRec_user_id(String rec_user_id) {
        this.rec_user_id = rec_user_id;
    }

    public String getRec_user_name() {
        return rec_user_name;
    }

    public void setRec_user_name(String rec_user_name) {
        this.rec_user_name = rec_user_name;
    }

    public String getRec_user_phone() {
        return rec_user_phone;
    }

    public void setRec_user_phone(String rec_user_phone) {
        this.rec_user_phone = rec_user_phone;
    }

    public String getRec_user_photo_url() {
        return rec_user_photo_url;
    }

    public void setRec_user_photo_url(String rec_user_photo_url) {
        this.rec_user_photo_url = rec_user_photo_url;
    }
}
