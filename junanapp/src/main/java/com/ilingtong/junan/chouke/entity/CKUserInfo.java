package com.ilingtong.junan.chouke.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/5/16.
 * mail: wuqian@ilingtong.com
 * Description:7001接口(获取我的基本信息) 用户信息
 */
public class CKUserInfo implements Serializable {
    private CKBaseInfo base_info;//基础信息
    private CKIncomeInfo income_info;//收益信息
    private CKFansInfo fans_info;//粉丝信息
    private List<CKOrganizationList> organization_list;//组织信息

    public CKBaseInfo getBase_info() {
        return base_info;
    }

    public void setBase_info(CKBaseInfo base_info) {
        this.base_info = base_info;
    }

    public CKIncomeInfo getIncome_info() {
        return income_info;
    }

    public void setIncome_info(CKIncomeInfo income_info) {
        this.income_info = income_info;
    }

    public CKFansInfo getFans_info() {
        return fans_info;
    }

    public void setFans_info(CKFansInfo fans_info) {
        this.fans_info = fans_info;
    }

    public List<CKOrganizationList> getOrganization_list() {
        return organization_list;
    }

    public void setOrganization_list(List<CKOrganizationList> organization_list) {
        this.organization_list = organization_list;
    }
}
