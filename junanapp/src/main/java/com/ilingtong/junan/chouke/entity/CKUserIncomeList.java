package com.ilingtong.junan.chouke.entity;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 10:50
 * Email: liuting@ilingtong.com
 * Desc:7013接口(取得我的个人业绩明细)的body 个人业绩明细信息
 */
public class CKUserIncomeList {
    private int data_total_count;//明细总数量
    private List<CKUserIncomeInfo> income_list;//业绩列表

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<CKUserIncomeInfo> getIncome_list() {
        return income_list;
    }

    public void setIncome_list(List<CKUserIncomeInfo> income_list) {
        this.income_list = income_list;
    }
}
