package com.ilingtong.junan.chouke.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.Intents;
import com.ilingtong.junan.chouke.CKConstant;
import com.ilingtong.junan.chouke.CKServiceManager;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.adapter.OrgPostListAdapter;
import com.ilingtong.junan.chouke.entity.CKOrgQrCodeResult;
import com.ilingtong.junan.chouke.entity.CKOrganizationDetailResult;
import com.ilingtong.junan.chouke.entity.CKUserPostList;
import com.ilingtong.junan.chouke.utils.CustomDialogUtils;
import com.ilingtong.junan.chouke.utils.ImageLoaderOptionsUtils;
import com.ilingtong.junan.chouke.widget.MyDialog;
import com.ilingtong.junan.chouke.widget.swiperefreshloadlayout.SwipeRefreshLoadLayout;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.BaseActivity;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.activity.ShowQRCodeActivity;
import com.ilingtong.library.tongle.protocol.BaseResult;
import com.ilingtong.library.tongle.protocol.CodeBaseResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/23
 * Time: 10:33
 * Email: liuting@ilingtong.com
 * Desc:组织详情
 */
public class OrganizationDetailActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mImgBack;//返回按钮
    private TextView mTxtTitle;//标题
    private ImageView mImgQr;//二维码图标

    private ImageView mOrgDetailImgIcon;//组织默认图片
    private Button mOrgDetailBtnJoin;//加入组织
    private TextView mOrgDetailTxtName;//组织名称
    private TextView mOrgDetailTxtDefault;//默认设置文字
    private TextView mOrgDetailTxtMember;//成员数
    private TextView mOrgDetailTxtPost;//帖子数
    private TextView mOrgDetailTxtDetail;//组织介绍

    private RelativeLayout mOrgDetailRlyMain;//组织推荐人主布局
    private ImageView mOrgDetailRecommendImgIcon;//组织推荐人头像

    private RelativeLayout mOrgDetailProdRlyMain;//组织推荐商品主布局
    private TextView mOrgDetailProdTxtCount;//组织推荐商品数量

    private ListView mOrgDetailLvPost;//帖子列表
    private OrgPostListAdapter mOrgPostListAdapter;//帖子列表Adapter
    private List<CKUserPostList> mPostList = new ArrayList<>();//帖子列表
    private CKOrganizationDetailResult mCKOrganizationDetailResult;//7003接口返回组织详情

    private Button mBtnJoinCancel;//取消加入组织
    private Button mBtnJoinSure;//确定加入组织
    private ImageView mImgJoinScan;//获取邀请二维码
    private EditText mEdtJoinInfo;//推荐人信息
    private CheckBox mCbJoinAgree;//同意协议选择框
    private LinearLayout mLlyJoinAgree;//同意协议文本

    private Dialog mDialog;//加载对话框
//    private ScrollView mSvMain;//主布局控件
    private SwipeRefreshLoadLayout mSlyMain;//刷新控件
    private MyDialog mCommonDialog;//自定义dialog
    private AlertDialog mAlertQR;//二维码对话框

    private final int RECOMMEND_SCAN_CODE = 10001;
    private String TAG = "ORG_DETAIL_ACTIVITY";//TAG
    private String mOrgId;//组织ID

    private final String JOIN_ORG_TYPE = "1";//加入组织标志
    private final String EXIT_ORG_TYPE = "2";//退出组织标志

    public static final int REFRESH_LIST = 0;  //刷新列表
    public static final int NO_DATA_REFRESH = 1;  //无数据时
    private boolean loadAllFlag = false;  //为true时表示数据已全部加载完毕
    private boolean clearFlag = true;
    private View headView;
    public static final int AGREE_PROTOCOL_REQUEST = 3001;//获取协议请求码

    private String[] needPermissions = {Manifest.permission.CAMERA};//相机权限，扫码所需
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_LIST:
                    mOrgDetailLvPost.setVisibility(View.VISIBLE);
                    mOrgPostListAdapter.notifyDataSetChanged();
                    break;
                case NO_DATA_REFRESH:
//                    mOrgDetailLvPost.setVisibility(View.GONE);
                    break;
            }
        }
    };

    /**
     * @param context
     * @param org_id      组织ID
     * @param requestCode
     */
    public static void launcher(Activity context, String org_id, int requestCode) {
        Intent intent = new Intent(context, OrganizationDetailActivity.class);
        intent.putExtra("org_id", (Serializable) org_id);
        context.startActivityForResult(intent, requestCode);
    }

    /**
     * @param context
     * @param org_id  组织ID
     */
    public static void launcher(Fragment context, String org_id) {
        Intent intent = new Intent(context.getActivity(), OrganizationDetailActivity.class);
        intent.putExtra("org_id", (Serializable) org_id);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ck_activity_org_detail);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        initView();
    }

    /**
     * 初始化控件
     */
    public void initView() {
        mOrgId = getIntent().getExtras().getString("org_id");

        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgQr = (ImageView) findViewById(R.id.QR_share_btn);
        mImgBack.setVisibility(View.VISIBLE);
        mImgBack.setOnClickListener(this);
        mImgQr.setOnClickListener(this);
        //界面标题设置
        mTxtTitle.setMaxEms(9);
        mTxtTitle.setSingleLine(true);
        mTxtTitle.setEllipsize(TextUtils.TruncateAt.END);

        headView = LayoutInflater.from(this).inflate(R.layout.ck_org_detail_head_view, null);
        mOrgDetailImgIcon = (ImageView) headView.findViewById(R.id.org_detail_info_img_icon);
        mOrgDetailBtnJoin = (Button) headView.findViewById(R.id.org_detail_info_btn_join);
        mOrgDetailTxtName = (TextView) headView.findViewById(R.id.org_detail_info_txt_name);
        mOrgDetailTxtDefault = (TextView) headView.findViewById(R.id.org_detail_info_txt_default);
        mOrgDetailTxtMember = (TextView) headView.findViewById(R.id.org_detail_info_txt_member);
        mOrgDetailTxtPost = (TextView) headView.findViewById(R.id.org_detail_info_txt_post);
        mOrgDetailTxtDetail = (TextView) headView.findViewById(R.id.org_detail_info_txt_detail);
        mOrgDetailBtnJoin.setOnClickListener(this);
        mOrgDetailTxtDefault.setOnClickListener(this);

        mOrgDetailRlyMain = (RelativeLayout) headView.findViewById(R.id.org_detail_recommend_rly_main);
        mOrgDetailRecommendImgIcon = (ImageView) headView.findViewById(R.id.org_detail_recommend_img_icon);
        mOrgDetailRlyMain.setOnClickListener(this);

        mOrgDetailProdRlyMain = (RelativeLayout) headView.findViewById(R.id.org_detail_prod_rly_main);
        mOrgDetailProdTxtCount = (TextView) headView.findViewById(R.id.org_detail_prod_txt_count);
        mOrgDetailProdRlyMain.setOnClickListener(this);

        mSlyMain = (SwipeRefreshLoadLayout) findViewById(R.id.org_detail_sly_main);

        mOrgDetailLvPost = (ListView) findViewById(R.id.org_detail_lv_post);
        mOrgDetailLvPost.addHeaderView(headView);

        mPostList.clear();

        mOrgPostListAdapter = new OrgPostListAdapter(OrganizationDetailActivity.this, mPostList);
        mOrgDetailLvPost.setAdapter(mOrgPostListAdapter);
        mOrgDetailLvPost.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //跳转到帖子详情
                Intent intent = new Intent(OrganizationDetailActivity.this, CollectForumDetailActivity.class);
                intent.putExtra("post_id", mPostList.get(position-1).getPost_id());
                startActivity(intent);
            }
        });

        mDialog = DialogUtils.createLoadingDialog(OrganizationDetailActivity.this);
        mDialog.setCancelable(true);

//        mSlyMain.setEnabled(true);
//        mSlyMain.setLoadMore(true);
        //下拉刷新监听
        mSlyMain.setOnRefreshListener(new SwipeRefreshLoadLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //下拉刷新
                clearFlag = true;
                getData("");
            }
        });

        //加载更多监听
        mSlyMain.setLoadMoreListener(new SwipeRefreshLoadLayout.LoadMoreListener() {
            @Override
            public void loadMore() {
                Log.e("TAG","more");
                if (loadAllFlag) {
                    ToastUtils.toastShort(com.ilingtong.library.tongle.R.string.common_list_end);
                } else {
                    clearFlag = false;
//                    mDialog.show();
                    getData(mPostList.get(mPostList.size() - 1).getPost_id());
                }
            }
        });
//        mDialog.show();
        getData("");
    }

    /**
     * 未加入组织，按钮显示“加入”
     */
    private void initJoin() {
        mOrgDetailBtnJoin.setText(R.string.org_detail_btn_join_txt);
        mOrgDetailBtnJoin.setBackgroundResource(R.drawable.ck_btn_join_selector);
        mOrgDetailRlyMain.setVisibility(View.GONE);
        mOrgDetailProdRlyMain.setVisibility(View.GONE);
        mOrgDetailTxtDefault.setVisibility(View.GONE);
        mOrgDetailTxtName.setMaxEms(11);
    }

    /**
     * 已经加入组织，按钮显示“退出”
     */
    private void initExit() {
        mOrgDetailBtnJoin.setText(R.string.org_detail_btn_exit_txt);
        mOrgDetailBtnJoin.setBackgroundResource(R.drawable.ck_btn_exit_selector);
        mOrgDetailRlyMain.setVisibility(View.VISIBLE);
        mOrgDetailProdRlyMain.setVisibility(View.VISIBLE);
        mOrgDetailTxtDefault.setVisibility(View.VISIBLE);
        mOrgDetailTxtName.setMaxEms(7);
        if (mCKOrganizationDetailResult.getBody().getBase_info().getIs_default_org().equals(TongleAppConst.YES)) {
            //为默认组织，显示当前默认
            initCancelDefault();
        } else {
            //不为默认组织，显示设置默认
            initSetDefault();
        }
    }

    /**
     * 显示设置默认
     */
    public void initSetDefault() {
        mOrgDetailTxtDefault.setBackgroundResource(R.drawable.ck_btn_set_default_selector);
        mOrgDetailTxtDefault.setText(R.string.set_default);
        mOrgDetailTxtDefault.setEnabled(true);
        mOrgDetailBtnJoin.setEnabled(true);
    }

    /**
     * 显示当前默认
     */
    public void initCancelDefault() {
        mOrgDetailBtnJoin.setEnabled(false);
        mOrgDetailTxtDefault.setEnabled(false);
        mOrgDetailTxtDefault.setText(R.string.org_detail_default_cancel_txt);
    }

    /**
     * 获取组织
     *
     * @param post_id 帖子ID
     */
    public void getData(String post_id) {
        mSlyMain.setRefreshing(true);

        if (mPostList != null && clearFlag) {
            //下拉刷新时
            mPostList.clear();
        }
//        mCKOrganizationDetailResult = new CKOrganizationDetailResult();
//        mCKOrganizationDetailResult = (CKOrganizationDetailResult) TestInterface.parseJson(CKOrganizationDetailResult.class, "7003.txt");
        CKServiceManager.getOrganizationDetail(mOrgId, post_id, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, getSuccessListener(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (clearFlag) {
                    //首次加载或者刷新时才需要隐藏数据
                    mTxtTitle.setVisibility(View.GONE);
                    mImgQr.setVisibility(View.GONE);
                    mOrgDetailLvPost.setVisibility(View.GONE);
//                mDialog.dismiss();
                }
                mSlyMain.setRefreshing(false);
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
            }
        });
    }

    /**
     * 初始化数据
     */
    public void initData() {
        mOrgPostListAdapter.notifyDataSetChanged();
        ImageLoader.getInstance().displayImage(mCKOrganizationDetailResult.getBody().getBase_info().getOrg_photo_url(), mOrgDetailImgIcon, ImageLoaderOptionsUtils.orgIconOptions());
        if (mCKOrganizationDetailResult.getBody().getBase_info().getIs_join_in().equals(TongleAppConst.YES)) {
            //已经是组织成员，按钮显示“退出”
            initExit();
        } else {//不是组织成员，按钮显示“加入”
            initJoin();
        }
        mTxtTitle.setText(mCKOrganizationDetailResult.getBody().getBase_info().getOrg_name());
        mOrgDetailTxtName.setText(mCKOrganizationDetailResult.getBody().getBase_info().getOrg_name());
        mOrgDetailTxtMember.setText(String.format(getString(R.string.org_detail_member_txt), mCKOrganizationDetailResult.getBody().getBase_info().getOrg_member_count()));
        mOrgDetailTxtPost.setText(String.format(getString(R.string.org_detail_post_txt), mCKOrganizationDetailResult.getBody().getBase_info().getOrg_post_count()));
        mOrgDetailTxtDetail.setText(mCKOrganizationDetailResult.getBody().getBase_info().getOrg_summary());

        ImageLoader.getInstance().displayImage(mCKOrganizationDetailResult.getBody().getOrg_recommend_info().getRec_user_photo_url(), mOrgDetailRecommendImgIcon, ImageLoaderOptionsUtils.recommendHeadOptions());
        mOrgDetailProdTxtCount.setText(String.format(getString(R.string.ord_detail_count_txt), mCKOrganizationDetailResult.getBody().getProd_info().getProd_count()));
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.left_arrow_btn) {//返回
            OrganizationDetailActivity.this.finish();
        } else if (i == R.id.QR_share_btn) {//显示二维码

            mDialog.show();
            CKServiceManager.getOrgQRCode(mCKOrganizationDetailResult.getBody().getBase_info().getOrg_id(), qrCodeSuccessListener(), errorListener());

        } else if (i == R.id.org_detail_info_btn_join) {//加入组织

            if (mCKOrganizationDetailResult.getBody().getBase_info().getIs_join_in().equals(TongleAppConst.YES)) {
                //已经是组织成员，显示退出对话框
                mCommonDialog = CustomDialogUtils.showCommonDialog(OrganizationDetailActivity.this, getString(R.string.exit_org_dialog_msg), cancelListener, exitListener);

            } else {//不是组织成员，显示加入对话框
                showJoinDialog();
            }

        } else if (i == R.id.org_detail_info_txt_default) {//设置组织为默认

            mCommonDialog = CustomDialogUtils.showCommonDialog(OrganizationDetailActivity.this, getString(R.string.default_org_dialog_msg), cancelListener, defaultListener);

        } else if (i == R.id.org_detail_recommend_rly_main) {//查看推荐人信息

        } else if (i == R.id.org_detail_prod_rly_main) {//查看推荐商品信息

            String strUrl = mCKOrganizationDetailResult.getBody().getProd_info().getProd_referee_url() + "?user_id=" + TongleAppInstance.getInstance().getUserID() + "&user_token=" + TongleAppInstance.getInstance().getToken() + "&org_id=" + mCKOrganizationDetailResult.getBody().getBase_info().getOrg_id()+"&app_inner_no="+TongleAppInstance.getInstance().getApp_inner_no();
            OrgProdListActivity.launcher(OrganizationDetailActivity.this,strUrl,mCKOrganizationDetailResult.getBody().getProd_info().getProd_referee_title());
//            Intent expertIntent = new Intent(this, OrgProdListActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putString("prod_url", strUrl);
//            bundle.putString("myTitle", mCKOrganizationDetailResult.getBody().getProd_info().getProd_referee_title());
//            expertIntent.putExtras(bundle);
//            startActivity(expertIntent);

        } else if (i == R.id.join_org_dialog_btn_cancel) {//取消加入组织

            mCommonDialog.dismiss();

        } else if (i == R.id.join_org_dialog_btn_sure) {//确定加入组织

            if (mCbJoinAgree.isChecked() == false) {//没有接受协议
                ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.regist_agreement_nocheck));
                return;
            }
            if (TextUtils.isEmpty(mEdtJoinInfo.getText())) {//没有扫码或输入推荐人信息
                ToastUtils.toastShort(getString(R.string.join_org_dialog_recommend_null));
                return;
            }
//            BaseResult baseResult = (BaseResult) TestInterface.parseJson(BaseResult.class, "7005.txt");
            CKServiceManager.joinOrOutOrg(mOrgId, JOIN_ORG_TYPE, mEdtJoinInfo.getText().toString(), joinSuccessListener(), commonErrorListener());

        } else if (i == R.id.join_org_dialog_img_scan) {//扫描邀请码
            requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(OrganizationDetailActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                        @Override
                        public void requestSuccess() {
                            //跳转到扫码
                            Intent intent = new Intent(OrganizationDetailActivity.this, CaptureActivity.class);
                            intent.setAction(Intents.Scan.ACTION);
                            intent.putExtra(Intents.Scan.MODE, Intents.Scan.QR_CODE_MODE);
                            intent.putExtra(Intents.Scan.RESULT_DISPLAY_DURATION_MS, 0L);
                            intent.putExtra("TopName", getString(R.string.join_org_dialog_msg));
                            startActivityForResult(intent, RECOMMEND_SCAN_CODE);
                        }

                        @Override
                        public void requestFail() {
                            SelectDialog dialog = new SelectDialog(OrganizationDetailActivity.this, getString(com.ilingtong.library.tongle.R.string.please_open_camera_permission));
                            dialog.show();
                        }
                    });
            requestPermissionUtils.checkPermissions(OrganizationDetailActivity.this);

        } else if (i == R.id.join_org_dialog_lly_agree) {//获取加入组织协议

            String strUrl = mCKOrganizationDetailResult.getBody().getBase_info().getProtocol_url()+ "?user_id=" + TongleAppInstance.getInstance().getUserID() + "&user_token=" + TongleAppInstance.getInstance().getToken() + "&org_id=" + mCKOrganizationDetailResult.getBody().getBase_info().getOrg_id();
            RegisterProtocolActivity.launcher(OrganizationDetailActivity.this,strUrl, CKConstant.REGISTER_INTO_TYPE,AGREE_PROTOCOL_REQUEST);
//            Intent expertIntent = new Intent(this, ProdIntegralActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putString("prod_url", strUrl);
//            bundle.putString("myTitle", getString(R.string.org_detail_join_protocol_title));
//            expertIntent.putExtras(bundle);
//            startActivity(expertIntent);
        }

    }

    /**
     * 显示加入对话框
     */
    private void showJoinDialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.common_dialog_style);
        View view = LayoutInflater.from(this).inflate(R.layout.ck_join_org_dialog_layout, null);
//        builder.setView(view);
//        mCommonAlert = builder.create();
        mCommonDialog = new MyDialog(this, DipUtils.px2dip(280), 0, view, R.style.ck_common_dialog_style);
        mCommonDialog.setCanceledOnTouchOutside(true);

        mBtnJoinCancel = (Button) view.findViewById(R.id.join_org_dialog_btn_cancel);
        mBtnJoinSure = (Button) view.findViewById(R.id.join_org_dialog_btn_sure);
        mImgJoinScan = (ImageView) view.findViewById(R.id.join_org_dialog_img_scan);
        mEdtJoinInfo = (EditText) view.findViewById(R.id.join_org_dialog_edt_info);
        mCbJoinAgree = (CheckBox) view.findViewById(R.id.join_org_dialog_cb_agree);
        mLlyJoinAgree = (LinearLayout) view.findViewById(R.id.join_org_dialog_lly_agree);

        mBtnJoinCancel.setOnClickListener(this);
        mBtnJoinSure.setOnClickListener(this);
        mImgJoinScan.setOnClickListener(this);
        mLlyJoinAgree.setOnClickListener(this);
        mCommonDialog.show();
    }

    /**
     * 取消事件监听
     */
    View.OnClickListener cancelListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mCommonDialog.dismiss();
        }
    };

    /**
     * 退出组织事件监听
     */
    View.OnClickListener exitListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            BaseResult baseResult = (BaseResult) TestInterface.parseJson(BaseResult.class, "7005.txt");
            CKServiceManager.joinOrOutOrg(mOrgId, EXIT_ORG_TYPE, "", exitSuccessListener(), commonErrorListener());
        }
    };

    /**
     * 设置默认组织事件监听
     */
    View.OnClickListener defaultListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            BaseResult baseResult = (BaseResult) TestInterface.parseJson(BaseResult.class, "7004.txt");

            CKServiceManager.setMyOrganization(mOrgId, setDefaultSuccessListener(), commonErrorListener());
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (RECOMMEND_SCAN_CODE == requestCode) {
            if (resultCode == RESULT_OK) {
                Bundle b = data.getExtras();
                String qrcode = b.getString("SCAN_RESULT");
                if (qrcode != null) {
                    mDialog.show();
                    ServiceManager.scanUserDecode(qrcode, CodeBaseRequest(), errorListener());
                    Log.e(TAG, qrcode);
                } else {
                    Log.e(TAG, getString(com.ilingtong.library.tongle.R.string.register_get_qrcode_error));
                }
            }
        }else if(requestCode==AGREE_PROTOCOL_REQUEST){
            if(resultCode==RESULT_OK){
                //同意协议结果返回，勾选同意协议
                mCbJoinAgree.setChecked(true);
            }
        }
    }

    /**
     * 请求成功，会员个人二维码解析
     *
     * @return
     */
    private Response.Listener<CodeBaseResult> CodeBaseRequest() {
        return new Response.Listener<CodeBaseResult>() {
            @Override
            public void onResponse(CodeBaseResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mEdtJoinInfo.setText(response.getBody().getUser_data().phone);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    /**
     * 网络请求异常
     *
     * @return
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
                mDialog.dismiss();
            }
        };
    }

    /**
     * 组织详情接口请求成功
     *
     * @return
     */
    private Response.Listener<CKOrganizationDetailResult> getSuccessListener() {
        return new Response.Listener<CKOrganizationDetailResult>() {
            @Override
            public void onResponse(CKOrganizationDetailResult ckOrganizationDetailResult) {
                mSlyMain.setRefreshing(false);
                mCKOrganizationDetailResult = ckOrganizationDetailResult;
                if (mCKOrganizationDetailResult.getHead().getReturn_flag().equals(TongleAppConst.YES)) {
                    //获取数据成功
                    mOrgDetailLvPost.setVisibility(View.VISIBLE);
                    mTxtTitle.setVisibility(View.VISIBLE);
                    mImgQr.setVisibility(View.VISIBLE);
                    if (mCKOrganizationDetailResult.getBody().getPost_info().getData_total_count() > 0) {
                        mPostList.addAll(mCKOrganizationDetailResult.getBody().getPost_info().getUser_post_list());
                        loadAllFlag = (mPostList.size() < mCKOrganizationDetailResult.getBody().getPost_info().getData_total_count()) ? false : true;
                        mSlyMain.setLoadMore(false);
                        handler.sendEmptyMessage(REFRESH_LIST);
                    } else {
                        handler.sendEmptyMessage(NO_DATA_REFRESH);
                        mSlyMain.setLoadMore(false);
                    }
                    if (clearFlag) {
                        //首次加载或者刷新时才需要初始化数据
                        initData();
                    }

                } else {//获取数据失败
                    if (clearFlag) {
                        //首次加载或者刷新时才需要隐藏数据
                        mTxtTitle.setVisibility(View.GONE);
                        mImgQr.setVisibility(View.GONE);
                        mOrgDetailLvPost.setVisibility(View.GONE);
                    }
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + mCKOrganizationDetailResult.getHead().getReturn_message());
                }
//                mDialog.dismiss();
//                mSlyMain.setRefreshing(false);
            }
        };
    }

    /**
     * 请求成功，设置默认组织
     *
     * @return
     */
    private Response.Listener<BaseResult> setDefaultSuccessListener() {
        return new Response.Listener<BaseResult>() {
            @Override
            public void onResponse(BaseResult baseResult) {
                if (baseResult.getHead().getReturn_flag().equals(TongleAppConst.YES)) {
                    //设置默认成功，显示当前默认
                    initCancelDefault();
                } else {//操作失败
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + baseResult.getHead().getReturn_message());
                }
                mCommonDialog.dismiss();
            }
        };
    }

    /**
     * 请求成功，退出组织
     *
     * @return
     */
    private Response.Listener<BaseResult> exitSuccessListener() {
        return new Response.Listener<BaseResult>() {
            @Override
            public void onResponse(BaseResult baseResult) {
                if (baseResult.getHead().getReturn_flag().equals(TongleAppConst.YES)) {//退出成功，按钮显示为加入状态
                    clearFlag = true;
                    getData("");
                    setResult(RESULT_OK);
                } else {//操作失败
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + baseResult.getHead().getReturn_message());
                }
                mCommonDialog.dismiss();
            }
        };
    }

    /**
     * 请求成功，加入组织
     *
     * @return
     */
    private Response.Listener<BaseResult> joinSuccessListener() {
        return new Response.Listener<BaseResult>() {
            @Override
            public void onResponse(BaseResult baseResult) {
                if (baseResult.getHead().getReturn_flag().equals(TongleAppConst.YES)) {
                    //加入成功，按钮显示为退出状态
                    clearFlag = true;
                    getData("");
                    setResult(RESULT_OK);
                } else {//操作失败
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + baseResult.getHead().getReturn_message());
                }
                mCommonDialog.dismiss();
            }
        };
    }

    /**
     * 网络请求异常
     *
     * @return
     */
    private Response.ErrorListener commonErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mCommonDialog.dismiss();
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
            }
        };
    }

    /**
     * 7007接口请求二维码 成功回调
     *
     * @return
     */
    private Response.Listener<CKOrgQrCodeResult> qrCodeSuccessListener() {
        return new Response.Listener<CKOrgQrCodeResult>() {
            @Override
            public void onResponse(CKOrgQrCodeResult ckOrgQrCodeResult) {
                mDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(ckOrgQrCodeResult.getHead().getReturn_flag())) {
                    ShowQRCodeActivity.launcher(OrganizationDetailActivity.this, ckOrgQrCodeResult.getBody().getOrg_qr_code_url(),mCKOrganizationDetailResult.getBody().getBase_info().getOrg_id(),TongleAppConst.STRORGANIZATION);
//                    ShowQRCodeDialog dialog = new ShowQRCodeDialog(OrganizationDetailActivity.this, ckOrgQrCodeResult.getBody().getOrg_qr_code_url(),mCKOrganizationDetailResult.getBody().getBase_info().getOrg_id(),TongleAppConst.STRORGANIZATION);
//                    dialog.show();
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + ckOrgQrCodeResult.getHead().getReturn_message());
                }
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}
