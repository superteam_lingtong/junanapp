package com.ilingtong.junan.chouke.entity;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/4/26.
 * mail: wuqian@ilingtong.com
 * Description:创业就业中心列表项信息
 * use by 7017接口
 */
public class EmploymentInfo implements Serializable {
    public String center_name;   //中心名称
    public String center_pic_url;  //logo
    public String post_id;   //对应的帖子id
}
