package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 14:53
 * Email: liuting@ilingtong.com
 * Desc:use by 7001接口(获取我的基本信息) 收益信息
 */
public class CKIncomeInfo {
    private double today_income;//今日收益
    private double month_income;//月收益
    private double total_income;//总收益
    private String income_url;//我的收益url

    public double getToday_income() {
        return today_income;
    }

    public void setToday_income(double today_income) {
        this.today_income = today_income;
    }

    public double getMonth_income() {
        return month_income;
    }

    public void setMonth_income(double month_income) {
        this.month_income = month_income;
    }

    public double getTotal_income() {
        return total_income;
    }

    public void setTotal_income(double total_income) {
        this.total_income = total_income;
    }

    public String getIncome_url() {
        return income_url;
    }

    public void setIncome_url(String income_url) {
        this.income_url = income_url;
    }
}
