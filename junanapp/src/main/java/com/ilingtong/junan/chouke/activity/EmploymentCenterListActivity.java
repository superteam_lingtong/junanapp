package com.ilingtong.junan.chouke.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.junan.chouke.CKServiceManager;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.adapter.EmploymentCenterListAdapter;
import com.ilingtong.junan.chouke.entity.EmploymentInfo;
import com.ilingtong.junan.chouke.entity.EmploymentResult;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuqian on 2017/4/25.
 * mail: wuqian@ilingtong.com
 * Description:创业就业中心列表
 */
public class EmploymentCenterListActivity extends Activity implements View.OnClickListener {
    private ListView lvList;//就业创业信息列表
    private ImageView btnBack;//返回
    private TextView topName;//标题
    private EmploymentCenterListAdapter mEmploymentCenterListAdapter;//就业创业信息列表 Adapter
    private List<EmploymentInfo> listEmployment;//就业创业信息列表
    private Dialog dialogLoading;//加载Dialog
    private static final int REQUEST_SUCCESS = 0;//请求成功
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REQUEST_SUCCESS:
                    mEmploymentCenterListAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    /**
     * @throws
     * @author: liuting
     * @date: 2017/4/26 16:57
     * @Title: launcher
     * @Description: 页面跳转
     * @param: fragment Fragment
     * @return: void
     */
    public static void launcher(Fragment fragment) {
        Intent intent = new Intent(fragment.getActivity(), EmploymentCenterListActivity.class);
        fragment.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ja_activity_employment_center_layout);
        initView();
        doRequest();
    }

    /**
     * @throws
     * @author: liuting
     * @date: 2017/4/26 16:03
     * @Title: initView
     * @Description: 初始化控件
     * @param:
     * @return: void
     */
    public void initView() {
        listEmployment = new ArrayList<>();
        dialogLoading = DialogUtils.createLoadingDialog(EmploymentCenterListActivity.this);
        dialogLoading.setCancelable(true);

        lvList = (ListView) findViewById(R.id.employment_center_lv_list);
        btnBack = (ImageView) findViewById(R.id.left_arrow_btn);
        topName = (TextView) findViewById(R.id.top_name);

        btnBack.setVisibility(View.VISIBLE);
        topName.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(this);
        topName.setText(getResources().getString(R.string.employment_center));

        mEmploymentCenterListAdapter = new EmploymentCenterListAdapter(EmploymentCenterListActivity.this, listEmployment, new EmploymentCenterListAdapter.IOnItemClickListener() {
            @Override
            public void onItemClick(View view, String post_id) {
                //跳转到帖子详情
                Intent intent = new Intent(EmploymentCenterListActivity.this, CollectForumDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("post_id", post_id);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        lvList.setAdapter(mEmploymentCenterListAdapter);
    }

    /**
     * @throws
     * @author: liuting
     * @date: 2017/4/26 16:02
     * @Title: doRequest
     * @Description: 请求数据
     * @param:
     * @return: void
     */
    public void doRequest() {
        dialogLoading.show();
        CKServiceManager.getEmploymentCenterList(new Response.Listener<EmploymentResult>() {
            @Override
            public void onResponse(EmploymentResult employmentResult) {

                dialogLoading.dismiss();
                //请求成功
                if (employmentResult.getHead().getReturn_flag().equals(TongleAppConst.SUCCESS)) {
                    listEmployment.addAll(employmentResult.body.center_info);
                    handler.sendEmptyMessage(REQUEST_SUCCESS);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + employmentResult.getHead().getReturn_message());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialogLoading.dismiss();
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.left_arrow_btn://返回
                finish();
                break;
        }
    }
}
