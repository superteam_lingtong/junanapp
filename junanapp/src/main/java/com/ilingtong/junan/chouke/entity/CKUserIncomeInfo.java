package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 10:51
 * Email: liuting@ilingtong.com
 * Desc:used by 7013接口(取得我的个人业绩明细) 业绩列表信息
 */
public class CKUserIncomeInfo {
    private CKIncomeUserInfo user_info;//用户信息
    private CKIncomeDetailInfo income_info;//获利信息

    public CKIncomeUserInfo getUser_info() {
        return user_info;
    }

    public void setUser_info(CKIncomeUserInfo user_info) {
        this.user_info = user_info;
    }

    public CKIncomeDetailInfo getIncome_info() {
        return income_info;
    }

    public void setIncome_info(CKIncomeDetailInfo income_info) {
        this.income_info = income_info;
    }
}
