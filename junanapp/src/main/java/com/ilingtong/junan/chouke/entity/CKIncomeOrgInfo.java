package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 10:25
 * Email: liuting@ilingtong.com
 * Desc:used by 7012接口(取得组织业绩组织酬金明细) 组织信息
 */
public class CKIncomeOrgInfo {
    private String org_id;//组织ID
    private String org_name;//组织名称
    private String org_summary;//组织简介
    private String org_photo_url;//组织图片

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getOrg_summary() {
        return org_summary;
    }

    public void setOrg_summary(String org_summary) {
        this.org_summary = org_summary;
    }

    public String getOrg_photo_url() {
        return org_photo_url;
    }

    public void setOrg_photo_url(String org_photo_url) {
        this.org_photo_url = org_photo_url;
    }
}
