package com.ilingtong.junan.chouke.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Package:com.ilingtong.junan.chouke.entity
 * author:liuting
 * Date:2017/4/13
 * Desc:军人类型列表类
 */

public class OfficerListInfo implements Serializable{
    public List<OfficerInfo> applicant_type_list;   //军人类型列表
}
