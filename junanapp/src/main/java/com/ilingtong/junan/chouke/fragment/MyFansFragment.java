package com.ilingtong.junan.chouke.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.junan.chouke.CKConstant;
import com.ilingtong.junan.chouke.CKServiceManager;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.adapter.MyFansListAdapter;
import com.ilingtong.junan.chouke.entity.CKDirectFansResult;
import com.ilingtong.junan.chouke.entity.CKPayoffListResult;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.CollectExpertDetailActivity;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.fragment.LazyFragment;
import com.ilingtong.library.tongle.protocol.FriendListItem;
import com.ilingtong.library.tongle.protocol.MyFansResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuqian on 2016/5/26.
 * mail: wuqian@ilingtong.com
 * Description:我的直粉/酬客/通粉列表的fragment
 */
public class MyFansFragment extends LazyFragment implements XListView.IXListViewListener {

    private XListView listView;
    private int type;
    private Dialog progressDialog;
    private MyFansListAdapter adapter;
    private List<FriendListItem> fansList = new ArrayList<>();
    private boolean flag = true; //是否需要清空列表标志。 为true时表示需要清空
    // 标志位，标志已经初始化完成。
    private boolean isPrepared;

    public static MyFansFragment newInstance(int type) {

        Bundle args = new Bundle();
        args.putInt("type", type);
        MyFansFragment fragment = new MyFansFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ck_fragment_my_fans_list_layuot, null);
        type = getArguments().getInt("type");
        listView = (XListView) view.findViewById(R.id.ck_fragment_my_fans_list);
        listView.setPullLoadEnable(false);
        listView.setPullRefreshEnable(true);
        listView.setXListViewListener(this, 0);
        progressDialog = DialogUtils.createLoadingDialog(getActivity());
        adapter = new MyFansListAdapter(fansList, getActivity());
        listView.setAdapter(adapter);
        listView.setRefreshTime();
        isPrepared = true;
        getData("");
        //点击列表Item，进入达人详情
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent expertIntent = new Intent(getActivity(), CollectExpertDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("user_id", fansList.get(position - 1).user_id);
                bundle.putString("user_favorited_by_me", fansList.get(position - 1).user_favorited_by_me);
                expertIntent.putExtras(bundle);
                startActivity(expertIntent);
            }
        });
        return view;
    }

    /**
     * 获取接口数据
     *
     * @param payoff_user_id 翻页时用
     */
    private void getData(String payoff_user_id) {
        flag = payoff_user_id.isEmpty();
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
        if (type == CKConstant.FRAGMENT_TYPE_CHOUKE) {
            CKServiceManager.getMyPayoffList(payoff_user_id, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, CKSuccessListener(), errorListener);
        } else if (type == CKConstant.FRAGMENT_TYPE_DERICT_FANS) {
            CKServiceManager.getMyDirectFanList(payoff_user_id, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, DFSuccessListener(), errorListener);
        } else if (type == CKConstant.FRAGMENT_TYPE_TONGLE_FANS) {
            ServiceManager.mineFanstRequest(payoff_user_id, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, TFSuccessListener(), errorListener);
        }
    }


    /**
     * 我的酬客接口请求成功回调
     *
     * @return
     */
    private Response.Listener<CKPayoffListResult> CKSuccessListener() {
        return new Response.Listener<CKPayoffListResult>() {
            @Override
            public void onResponse(CKPayoffListResult result) {
                progressDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(result.getHead().getReturn_flag())) {
                    initData(result.getBody().getPayoff_list());
                    listView.setPullLoadEnable(fansList.size() < result.getBody().getData_total_count() ? true : false);
                } else {
                    ToastUtils.toastLong(R.string.para_exception + result.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 我的直粉接口请求成功回调
     *
     * @return
     */
    private Response.Listener<CKDirectFansResult> DFSuccessListener() {
        return new Response.Listener<CKDirectFansResult>() {
            @Override
            public void onResponse(CKDirectFansResult result) {
                progressDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(result.getHead().getReturn_flag())) {
                    initData(result.getBody().getDirect_fans_list());
                    listView.setPullLoadEnable(fansList.size() < result.getBody().getData_total_count() ? true : false);
                } else {
                    ToastUtils.toastLong(R.string.para_exception + result.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 我的通粉接口请求成功回调
     *
     * @return
     */
    private Response.Listener<MyFansResult> TFSuccessListener() {
        return new Response.Listener<MyFansResult>() {
            @Override
            public void onResponse(MyFansResult result) {
                progressDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(result.head.getReturn_flag())) {
                    initData(result.body.user_fans_list);
                    listView.setPullLoadEnable(fansList.size() < Integer.parseInt(result.body.data_total_count) ? true : false);
                } else {
                    ToastUtils.toastLong(R.string.para_exception + result.head.getReturn_message());
                }
            }
        };
    }

    /**
     * 接口请求错误回调
     */
    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            progressDialog.dismiss();
            ToastUtils.toastLong(R.string.sys_exception);
            Log.e("Tag", volleyError.toString());
        }
    };

    /**
     * 接口请求完成之后刷新adapter
     * @param list
     */
    private void initData(List<FriendListItem> list) {
        if (flag) {
            fansList.clear();
        }
        fansList.addAll(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRefresh(int id) {
        listView.setRefreshTime();
        getData("");
    }

    @Override
    public void onLoadMore(int id) {
        getData(fansList.get(fansList.size() - 1).user_id);
    }

    @Override
    protected void lazyLoad() {
        if (!isPrepared || !isVisible) {
            return;
        }else {
            listView.setRefreshTime();
            getData("");
        }
    }
}
