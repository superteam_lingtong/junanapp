package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.PostPicUrlInfo;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/19
 * Time: 17:35
 * Email: liuting@ilingtong.com
 * Desc:used by 7003接口(获取指定组织的详情) 帖子列表
 */
public class CKUserPostList {
    private String user_id;//用户ID
    private String user_nick_name;//用户昵称
    private String user_signature;//用户签名
    private String user_head_photo_url;//用户头像URL
    private String user_favorited_by_me;//帖子所属用户是否被当前用户收藏
    private String post_id;//帖子ID
    private String post_title;//帖子标题
    private String post_comment;//帖子转发内容
    private String post_time;//帖子发布时间
    private String post_favorited_by_me;//帖子是否被当前用户收藏
    private String first_user_id;//原贴会员ID
    private String first_user_nick_name;//原贴会员昵称
    private String first_user_signature;//原贴会员签名
    private String first_user_head_photo_url;//原贴会员头像URL
    private String first_post_id;//原贴帖子ID
    private String first_post_title;//原贴帖子标题
    private String first_post_time;//原贴发布时间
    private List<PostPicUrlInfo> post_thumbnail_pic_url;//帖子缩略图片列表
    private List<PostPicUrlInfo> first_post_thumbnail_pic_url;//原贴缩略图片列表

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_nick_name() {
        return user_nick_name;
    }

    public void setUser_nick_name(String user_nick_name) {
        this.user_nick_name = user_nick_name;
    }

    public String getUser_signature() {
        return user_signature;
    }

    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }

    public String getUser_head_photo_url() {
        return user_head_photo_url;
    }

    public void setUser_head_photo_url(String user_head_photo_url) {
        this.user_head_photo_url = user_head_photo_url;
    }

    public String getUser_favorited_by_me() {
        return user_favorited_by_me;
    }

    public void setUser_favorited_by_me(String user_favorited_by_me) {
        this.user_favorited_by_me = user_favorited_by_me;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_comment() {
        return post_comment;
    }

    public void setPost_comment(String post_comment) {
        this.post_comment = post_comment;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    public String getPost_favorited_by_me() {
        return post_favorited_by_me;
    }

    public void setPost_favorited_by_me(String post_favorited_by_me) {
        this.post_favorited_by_me = post_favorited_by_me;
    }

    public String getFirst_user_id() {
        return first_user_id;
    }

    public void setFirst_user_id(String first_user_id) {
        this.first_user_id = first_user_id;
    }

    public String getFirst_user_nick_name() {
        return first_user_nick_name;
    }

    public void setFirst_user_nick_name(String first_user_nick_name) {
        this.first_user_nick_name = first_user_nick_name;
    }

    public String getFirst_user_signature() {
        return first_user_signature;
    }

    public void setFirst_user_signature(String first_user_signature) {
        this.first_user_signature = first_user_signature;
    }

    public String getFirst_user_head_photo_url() {
        return first_user_head_photo_url;
    }

    public void setFirst_user_head_photo_url(String first_user_head_photo_url) {
        this.first_user_head_photo_url = first_user_head_photo_url;
    }

    public String getFirst_post_id() {
        return first_post_id;
    }

    public void setFirst_post_id(String first_post_id) {
        this.first_post_id = first_post_id;
    }

    public String getFirst_post_title() {
        return first_post_title;
    }

    public void setFirst_post_title(String first_post_title) {
        this.first_post_title = first_post_title;
    }

    public String getFirst_post_time() {
        return first_post_time;
    }

    public void setFirst_post_time(String first_post_time) {
        this.first_post_time = first_post_time;
    }

    public List<PostPicUrlInfo> getPost_thumbnail_pic_url() {
        return post_thumbnail_pic_url;
    }

    public void setPost_thumbnail_pic_url(List<PostPicUrlInfo> post_thumbnail_pic_url) {
        this.post_thumbnail_pic_url = post_thumbnail_pic_url;
    }

    public List<PostPicUrlInfo> getFirst_post_thumbnail_pic_url() {
        return first_post_thumbnail_pic_url;
    }

    public void setFirst_post_thumbnail_pic_url(List<PostPicUrlInfo> first_post_thumbnail_pic_url) {
        this.first_post_thumbnail_pic_url = first_post_thumbnail_pic_url;
    }

    public static class PostThumbnailPicUrlBean {
        private String pic_url;

        public String getPic_url() {
            return pic_url;
        }

        public void setPic_url(String pic_url) {
            this.pic_url = pic_url;
        }
    }

    public static class FirstPostThumbnailPicUrlBean {
        private String pic_url;

        public String getPic_url() {
            return pic_url;
        }

        public void setPic_url(String pic_url) {
            this.pic_url = pic_url;
        }
    }
}
