package com.ilingtong.junan.chouke.entity;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/21.
 * mail: wuqian@ilingtong.com
 * Description:未读帖子数量
 * use by 2046
 */
public class JAUnreadPostInfo implements Serializable{
    public  int unread_post_count;
    public  String max_post_id;
}
