package com.ilingtong.junan.chouke;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.ilingtong.library.tongle.TongleAppInstance;

/**
 * Created by wuqian on 2016/5/16.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class ChouKeApplication extends TongleAppInstance {
    private static Context paramContext = null;

    private String myDefaultOrg;   //我的默认组织
    private SharedPreferences settings;
    public static ChouKeApplication ckInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        paramContext = getApplicationContext();
        settings = getSharedPreferences("jaUserInfo", Context.MODE_PRIVATE);
        ckInstance = this;
        setApp_inner_no("05");  //设置版本号
        setPackageName("com.ilingtong.junan.chouke");  //设置包名
        setIfRemberKeyName("JAIfRember");    //是否记住密码 sp存储keyname
        setLoginUseridKeyName("JAloginUserid");  //登录用户名 sp存储keyname
        setLoginPwdKeyName("JAloginPwd");   //登录密码 sp存储keyname
        setIfAutoLoginKeyName("JAifAutoLoginKeyName");  //是否自动登录 sp存储keyname
        /********设置微信，微博相关参数*******/
        setSHARE_SDK_ID("165b1e6c2e26a");
        setWECHAT_APPID("wx7cf89589d039b54a");
        setWECHAT_SECRET("37d544720aa2736798117a4f5230d868");
        setSINA_APPKEY("162262167");
        setSINA_APPSECRET("7f5c6a9c120e91f9d072b99efe1a1126");
        setSINA_REDIRECT("https://127.0.0.1/oauth2/default.html");
        initImageLoader(getApplicationContext());
        setAppContext(getApplicationContext());
    }

    public String getMyDefaultOrg() {
        if (TextUtils.isEmpty(myDefaultOrg)) {
            myDefaultOrg = settings.getString("my_defualt_org", "");
        }
        return myDefaultOrg;
    }

    public void setMyDefaultOrg(String myDefaultOrg) {
        this.myDefaultOrg = myDefaultOrg;
        settings.edit().putString("my_defualt_org", myDefaultOrg).commit();
    }

    /**
     * 获取上下文
     *
     * @return
     */
    public static Context getAppContext() {
        return paramContext;
    }
}
