package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 10:11
 * Email: liuting@ilingtong.com
 * Desc:7011接口(获取我的直粉列表) 返回json结构
 */
public class CKDirectFansResult extends BaseResult implements Serializable {
    private CKDirectFansInfo body;//我的直粉列表信息

    public CKDirectFansInfo getBody() {
        return body;
    }

    public void setBody(CKDirectFansInfo body) {
        this.body = body;
    }
}
