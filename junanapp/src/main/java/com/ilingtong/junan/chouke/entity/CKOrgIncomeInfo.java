package com.ilingtong.junan.chouke.entity;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 9:43
 * Email: liuting@ilingtong.com
 * Desc:7009接口(取得组织业绩组织酬金列表)的body 组织业绩组织酬金列表信息
 */
public class CKOrgIncomeInfo {
    private int data_total_count;//组织总数量
    private List<CKIncomeList> income_list;//业绩列表

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<CKIncomeList> getIncome_list() {
        return income_list;
    }

    public void setIncome_list(List<CKIncomeList> income_list) {
        this.income_list = income_list;
    }
}
