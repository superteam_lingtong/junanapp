package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.FriendListItem;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 16:36
 * Email: liuting@ilingtong.com
 * Desc: 7006接口(获取我的酬客列表)的body 我的酬客列表信息
 */
public class CKPayoffListInfo {
    private int data_total_count;//酬客总数量
    private List<FriendListItem> payoff_list;//酬客列表

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<FriendListItem> getPayoff_list() {
        return payoff_list;
    }

    public void setPayoff_list(List<FriendListItem> payoff_list) {
        this.payoff_list = payoff_list;
    }
}
