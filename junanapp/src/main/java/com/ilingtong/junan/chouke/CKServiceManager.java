package com.ilingtong.junan.chouke;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.ilingtong.junan.chouke.entity.ArmListResult;
import com.ilingtong.junan.chouke.entity.CKDirectFansResult;
import com.ilingtong.junan.chouke.entity.CKIncomeDetailResult;
import com.ilingtong.junan.chouke.entity.CKMyIncomeResult;
import com.ilingtong.junan.chouke.entity.CKOrgIncomeResult;
import com.ilingtong.junan.chouke.entity.CKOrgQrCodeResult;
import com.ilingtong.junan.chouke.entity.CKOrganizationDetailResult;
import com.ilingtong.junan.chouke.entity.CKOrganizationInfoResult;
import com.ilingtong.junan.chouke.entity.CKPayoffListResult;
import com.ilingtong.junan.chouke.entity.CKPayoffURLResult;
import com.ilingtong.junan.chouke.entity.CKPersonalInfoResult;
import com.ilingtong.junan.chouke.entity.CKUserIncomeResult;
import com.ilingtong.junan.chouke.entity.EmploymentResult;
import com.ilingtong.junan.chouke.entity.JARegisterParam;
import com.ilingtong.junan.chouke.entity.JAUnreadPostInfoResult;
import com.ilingtong.junan.chouke.entity.OfficerListResult;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.BaseResult;
import com.ilingtong.library.tongle.protocol.ExpertProductResult;
import com.ilingtong.library.tongle.protocol.GsonRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wuqian on 2016/5/19.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class CKServiceManager {

    /**
     *  7001 获取的我基本信息
     * @param listener
     * @param errorListener
     */
    static public void getMyPersonalInfo(Response.Listener<CKPersonalInfoResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/personal_info";
        Map<String,String> params_gson = ServiceManager.getParams_gson(new HashMap());
        GsonRequest<CKPersonalInfoResult> gsonRequest = new GsonRequest<CKPersonalInfoResult>(Request.Method.POST, url, CKPersonalInfoResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7001] 获得我的基本信息 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 7002 获取我关注的组织列表
     * @param org_id 组织id
     * @param forward 翻页方向
     * @param fetch_count 返回列表条数
     * @param listener
     * @param errorListener
     */
    static public void getMyOrganizationList(String org_id,String forward,String fetch_count,Response.Listener<CKOrganizationInfoResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/organization_info";
        Map requestParam = new HashMap();
        requestParam.put("org_id",org_id);
        requestParam.put("forward",forward);
        requestParam.put("fetch_count",fetch_count);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<CKOrganizationInfoResult> gsonRequest = new GsonRequest<CKOrganizationInfoResult>(Request.Method.POST, url, CKOrganizationInfoResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7002] 获取我关注的组织列表 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 7003 获取指定组织的详情
     * @param org_id 组织id
     * @param listener
     * @param errorListener
     */
    static public void getOrganizationDetail(String org_id,String post_id,String forward,String fetch_count,Response.Listener<CKOrganizationDetailResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/org/organization_detail";
        Map requestParam = new HashMap();
        requestParam.put("org_id",org_id);
        requestParam.put("post_id",post_id);
        requestParam.put("forward",forward);
        requestParam.put("fetch_count",fetch_count);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<CKOrganizationDetailResult> gsonRequest = new GsonRequest<CKOrganizationDetailResult>(Request.Method.POST, url, CKOrganizationDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7003] 获取指定组织的详情 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 7004 设置我的默认组织
     * @param org_id 组织id
     * @param listener
     * @param errorListener
     */
    static public void setMyOrganization(String org_id,Response.Listener<BaseResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/default_organization_set";
        Map requestParam = new HashMap();
        requestParam.put("org_id",org_id);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<BaseResult> gsonRequest = new GsonRequest<BaseResult>(Request.Method.POST, url, BaseResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7004]设置我的默认组织 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     *7005 加入/退出 组织
     * @param org_id 组织id
     * @param operation_kbn 操作类别  1：加入  2：退出
     * @param referee_id 推荐人id 操作区分为1的场合必须
     * @param listener
     * @param errorListener
     */
    static public void joinOrOutOrg(String org_id,String operation_kbn,String referee_id,Response.Listener<BaseResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/org/organization_inout";
        Map requestParam = new HashMap();
        requestParam.put("org_id",org_id);
        requestParam.put("operation_kbn",operation_kbn);
        requestParam.put("referee_id",referee_id);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<BaseResult> gsonRequest = new GsonRequest<BaseResult>(Request.Method.POST, url, BaseResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7005] 加入/退出 组织 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 7006 获取我的酬客列表
     * @param payoff_user_id 酬客用户id
     * @param forward 翻页方向
     * @param fetch_count 返回数量
     * @param listener
     * @param errorListener
     */
    static public void getMyPayoffList(String payoff_user_id,String forward,String fetch_count,Response.Listener<CKPayoffListResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/payoff_list";
        Map requestParam = new HashMap();
        requestParam.put("payoff_user_id",payoff_user_id);
        requestParam.put("forward",forward);
        requestParam.put("fetch_count",fetch_count);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<CKPayoffListResult> gsonRequest = new GsonRequest<CKPayoffListResult>(Request.Method.POST, url, CKPayoffListResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7006] 获取我的酬客列表 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 7007 获取组织邀请二维码
     * @param org_id 组织id
     * @param listener
     * @param errorListener
     */
    static public void getOrgQRCode(String org_id,Response.Listener<CKOrgQrCodeResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/qrcode/organization_show";
        Map requestParam = new HashMap();
        requestParam.put("org_id",org_id);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<CKOrgQrCodeResult> gsonRequest = new GsonRequest<CKOrgQrCodeResult>(Request.Method.POST, url, CKOrgQrCodeResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7007] 获取组织邀请二维码 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 7008 取的我的业绩总览
     * @param listener
     * @param errorListener
     */
    static public void getMyIncome(Response.Listener<CKMyIncomeResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/income_show";
        Map<String,String> params_gson = ServiceManager.getParams_gson(new HashMap());
        GsonRequest<CKMyIncomeResult> gsonRequest = new GsonRequest<CKMyIncomeResult>(Request.Method.POST, url, CKMyIncomeResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7008] 取的我的业绩总览 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 7009 取的组织 业绩/酬金 列表
     * @param org_id 组织id
     * @param forward
     * @param fetch_count
     * @param income_type 1：组织业绩  2：组织酬金
     * @param listener
     * @param errorListener
     */
    static public void getOrgIncome(String org_id,String forward,String fetch_count,String income_type,Response.Listener<CKOrgIncomeResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/organization_income";
        Map requestParam = new HashMap();
        requestParam.put("org_id",org_id);
        requestParam.put("forward",forward);
        requestParam.put("fetch_count",fetch_count);
        requestParam.put("income_type",income_type);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<CKOrgIncomeResult> gsonRequest = new GsonRequest<CKOrgIncomeResult>(Request.Method.POST, url, CKOrgIncomeResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7009] 取的组织 业绩/酬金 列表 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     *  7010 获取组织推荐商品列表
     * @param prod_id 商品id 翻页时用
     * @param forward
     * @param fetch_count
     * @param listener
     * @param errorListener
     */
    static public void getOrgProdList(String prod_id,String forward,String fetch_count,Response.Listener<ExpertProductResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/product/organization_prod_show";
        Map requestParam = new HashMap();
        requestParam.put("prod_id",prod_id);
        requestParam.put("forward",forward);
        requestParam.put("fetch_count",fetch_count);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<ExpertProductResult> gsonRequest = new GsonRequest<ExpertProductResult>(Request.Method.POST, url, ExpertProductResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7010] 获取组织推荐商品列表 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }
    /**
     * 7011 获取我的直粉列表
     * @param direct_fans_id 直粉id 翻页用
     * @param forward
     * @param fetch_count
     * @param listener
     * @param errorListener
     */
    static public void getMyDirectFanList(String direct_fans_id,String forward,String fetch_count,Response.Listener<CKDirectFansResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/direct_fans_list";
        Map requestParam = new HashMap();
        requestParam.put("direct_fans_id",direct_fans_id);
        requestParam.put("forward",forward);
        requestParam.put("fetch_count",fetch_count);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<CKDirectFansResult> gsonRequest = new GsonRequest<CKDirectFansResult>(Request.Method.POST, url, CKDirectFansResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7011] 获取我的直粉列表 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }
    /**
     * 7012 取得组织业绩/组织酬金明细
     * @param org_id 组织id
     * @param forward
     * @param fetch_count
     * @param income_type 业绩区分 1：组织业绩  2：组织酬金
     * @param income_id 获利id
     * @param listener
     * @param errorListener
     */
    static public void getOrgIncomeDetail(String org_id,String forward,String fetch_count,String income_type,String income_id,Response.Listener<CKIncomeDetailResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/org_income_detail";
        Map requestParam = new HashMap();
        requestParam.put("org_id",org_id);
        requestParam.put("forward",forward);
        requestParam.put("fetch_count",fetch_count);
        requestParam.put("income_type",income_type);
        requestParam.put("income_id",income_id);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<CKIncomeDetailResult> gsonRequest = new GsonRequest<CKIncomeDetailResult>(Request.Method.POST, url, CKIncomeDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7012] 取得组织业绩/组织酬金明细 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 7013 取的我的个人业绩明细
     * @param forward
     * @param fetch_count
     * @param income_type 业绩区分 1：组织业绩  2：组织酬金
     * @param income_id 获利id
     * @param listener
     * @param errorListener
     */
    static public void getMyPersonalncome(String forward,String fetch_count,String income_type,String income_id,Response.Listener<CKUserIncomeResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/personal_income_show";
        Map requestParam = new HashMap();
        requestParam.put("forward",forward);
        requestParam.put("fetch_count",fetch_count);
        requestParam.put("income_type",income_type);
        requestParam.put("income_id",income_id);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<CKUserIncomeResult> gsonRequest = new GsonRequest<CKUserIncomeResult>(Request.Method.POST, url, CKUserIncomeResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7013] 取的我的个人业绩明细 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 7014 同意协议
     * @param listener
     * @param errorListener
     */
    static public void agreeProtocol(Response.Listener<BaseResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/agree_protocol";
        Map<String,String> params_gson = ServiceManager.getParams_gson(new HashMap());
        GsonRequest<BaseResult> gsonRequest = new GsonRequest<BaseResult>(Request.Method.POST, url, BaseResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7014] 同意协议 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 7015 获取酬客协议url
     * @param listener
     * @param errorListener
     */
    static public void getPayoffProtocolURL(Response.Listener<CKPayoffURLResult> listener,Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/payoff_protocol";
        Map<String,String> params_gson = ServiceManager.getParams_gsonForSystem(new HashMap());
        GsonRequest<CKPayoffURLResult> gsonRequest = new GsonRequest<CKPayoffURLResult>(Request.Method.POST, url, CKPayoffURLResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7015] 获取酬客协议url " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }


    /**
     * 12001 君安注册
     * @param listener
     * @param errorListener
     */
    static public void JARegister(JARegisterParam registerParam, Response.Listener<BaseResult> listener, Response.ErrorListener errorListener) {

        String url = TongleAppConst.SERVER_ADDRESS + "system/user_register";
        Map requestParam = new HashMap();
        requestParam.put("phone", registerParam.phone);
        requestParam.put("password", registerParam.password);
        requestParam.put("id_card", registerParam.id_card);
        requestParam.put("name", registerParam.name);
        requestParam.put("id_card_positive_pic", registerParam.id_card_positive_pic);
        requestParam.put("id_card_opposite_pic", registerParam.id_card_opposite_pic);
        requestParam.put("discharged_pic", registerParam.discharged_pic);
        requestParam.put("verification_code", registerParam.verification_code);
        requestParam.put("veterans_card_number",registerParam.veterans_card_number);
        requestParam.put("is_veterans",registerParam.is_veterans);
        requestParam.put("referee_id",registerParam.referee_id);
        requestParam.put("syori_flg",registerParam.syori_flg);
        requestParam.put("register_user_id",registerParam.register_user_id);
        requestParam.put("arm_id",registerParam.arm_id);
        requestParam.put("applicant_type_id",registerParam.applicant_type_id);
        Map<String, String> params_json = ServiceManager.getParams_gsonForSystem(requestParam);
        GsonRequest<BaseResult> gsonRequest = new GsonRequest<>(Request.Method.POST, url, BaseResult.class, params_json, listener, errorListener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(5000 * 3,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "12001 君安注册" + url + "?parameters_json=" + params_json.get("parameters_json"));
    }

    /**
     * 2046 未读帖子
     * @param post_id 贴子id
     * @param post_user_id 发帖人id
     * @param listener
     * @param errorListener
     */
    public static void getUnreadPost(String post_id, String post_user_id, Response.Listener<JAUnreadPostInfoResult> listener, Response.ErrorListener errorListener){
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/user/unread_post";
        Map requestParam = new HashMap();
        requestParam.put("post_id", post_id);
        requestParam.put("post_user_id", post_user_id);
        Map<String,String> params_gson = ServiceManager.getParams_gson(requestParam);
        GsonRequest<JAUnreadPostInfoResult> gsonRequest = new GsonRequest<JAUnreadPostInfoResult>(Request.Method.POST, url, JAUnreadPostInfoResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "未读贴子" + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 12007 获取兵种列表
     * @param listener
     * @param errorListener
     */
    public static void getArmList(Response.Listener<ArmListResult> listener, Response.ErrorListener errorListener){
        String url = TongleAppConst.SERVER_ADDRESS + "system/arm";
        Map<String,String> params_gson = ServiceManager.getParams_gsonForSystem(new HashMap());
        GsonRequest<ArmListResult> gsonRequest = new GsonRequest<ArmListResult>(Request.Method.POST, url, ArmListResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[12007] 获取兵种列表 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 12008 获取军官类别 如：军属/退役军人等
     * @param listener
     * @param errorListener
     */
    public static void getOfficerTypeList(Response.Listener<OfficerListResult> listener, Response.ErrorListener errorListener){
        String url = TongleAppConst.SERVER_ADDRESS +"system/applicant_type";
        Map<String,String> params_gson = ServiceManager.getParams_gsonForSystem(new HashMap());
        GsonRequest<OfficerListResult> gsonRequest = new GsonRequest<OfficerListResult>(Request.Method.POST, url, OfficerListResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[12008] 获取军官类别列表 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * @author: liuting
     * @date: 2017/4/26 16:19
     * @Title: getEmploymentCenterList
     * @Description:
     * @param: listener 请求监听
     * @param: errorListener 请求错误监听
     * @return: void
     * @throws
     */
    public static void getEmploymentCenterList(Response.Listener<EmploymentResult> listener, Response.ErrorListener errorListener){
        String url = TongleAppConst.SERVER_ADDRESS +"system/center_list";
        Map<String,String> params_gson = ServiceManager.getParams_gsonForSystem(new HashMap());
        GsonRequest<EmploymentResult> gsonRequest = new GsonRequest<EmploymentResult>(Request.Method.POST, url, EmploymentResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[7017] 创业中心列表取得 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }
}
