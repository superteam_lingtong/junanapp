package com.ilingtong.junan.chouke.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.utils.ImageLoaderOptionsUtils;
import com.ilingtong.library.tongle.protocol.OrganizationListItem;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/26
 * Time: 9:26
 * Email: liuting@ilingtong.com
 * Desc:搜索组织Adapter
 */
public class SearchOrgListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<OrganizationListItem> mLvOrg;
    private Context mContext;

    public SearchOrgListAdapter(Context context, List<OrganizationListItem> orgList) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvOrg = orgList;
    }

    @Override
    public int getCount() {
        return mLvOrg.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvOrg.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.ck_my_organization_list_item, null);
            holder = new ViewHolder();
            holder.mOrgImgIcon = (ImageView) view.findViewById(R.id.my_organization_img_icon);
            holder.mOrgTxtTime = (TextView) view.findViewById(R.id.my_organization_txt_time);
            holder.mOrgTxtName = (TextView) view.findViewById(R.id.my_organization_txt_name);
            holder.mOrgTxtPost = (TextView) view.findViewById(R.id.my_organization_txt_post);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

//      填充数据
        OrganizationListItem orgItem = (OrganizationListItem) getItem(position);
        ImageLoader.getInstance().displayImage(orgItem.org_photo_url, holder.mOrgImgIcon, ImageLoaderOptionsUtils.orgIconOptions());
        holder.mOrgTxtTime.setText(orgItem.post_update_time);
        holder.mOrgTxtName.setText(orgItem.org_name);
        holder.mOrgTxtPost.setText(orgItem.latest_post_info);
        return view;
    }

    static class ViewHolder {
        ImageView mOrgImgIcon;//组织默认图片
        TextView mOrgTxtTime;//帖子最新更新时间
        TextView mOrgTxtName;//组织默认名称
        TextView mOrgTxtPost;//最近发帖内容
    }
}

