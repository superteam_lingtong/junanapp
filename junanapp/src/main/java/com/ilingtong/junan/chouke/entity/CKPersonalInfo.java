package com.ilingtong.junan.chouke.entity;

/**
 * Created by wuqian on 2016/5/27.
 * mail: wuqian@ilingtong.com
 * Description:7001接口(获取我的基本信息)的body
 */
public class CKPersonalInfo {
    private CKUserInfo user_info;

    public CKUserInfo getUser_info() {
        return user_info;
    }

    public void setUser_info(CKUserInfo user_info) {
        this.user_info = user_info;
    }
}
