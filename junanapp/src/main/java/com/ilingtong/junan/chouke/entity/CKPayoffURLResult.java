package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 11:22
 * Email: liuting@ilingtong.com
 * Desc:7015接口(获取酬客协议URL)返回json结构
 */
public class CKPayoffURLResult extends BaseResult implements Serializable {
    private CKPayoffURLInfo body;//酬客协议信息

    public CKPayoffURLInfo getBody() {
        return body;
    }

    public void setBody(CKPayoffURLInfo body) {
        this.body = body;
    }
}
