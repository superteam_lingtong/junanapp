package com.ilingtong.junan.chouke.entity;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 15:21
 * Email: liuting@ilingtong.com
 * Desc:7002接口(获取我的关注组织列表)的body 组织信息
 */
public class CKOrganizationInfo {
    private int data_total_count;//组织总数量
    private List<CKOrganizationList> organization_list;//我加入的组织列表

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<CKOrganizationList> getOrganization_list() {
        return organization_list;
    }

    public void setOrganization_list(List<CKOrganizationList> organization_list) {
        this.organization_list = organization_list;
    }
}
