package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 11:23
 * Email: liuting@ilingtong.com
 * Desc:7015接口(获取酬客协议URL)的body 酬客协议
 */
public class CKPayoffURLInfo {
    private String payoff_protocol_url;//酬客协议URL

    public String getPayoff_protocol_url() {
        return payoff_protocol_url;
    }

    public void setPayoff_protocol_url(String payoff_protocol_url) {
        this.payoff_protocol_url = payoff_protocol_url;
    }
}
