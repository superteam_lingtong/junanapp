package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 15:36
 * Email: liuting@ilingtong.com
 * Desc:7003接口(获取指定组织的详情)的body 组织详情
 */
public class CKOrganizationDetail {
    private CKOrganizationBaseInfo base_info;//组织基本信息
    private CKOrgRecommendInfo org_recommend_info;//组织推荐者信息
    private CKProdInfo prod_info;//组织推荐商品信息
    private CKPostInfo post_info;//组织内部发帖信息

    public CKOrganizationBaseInfo getBase_info() {
        return base_info;
    }

    public void setBase_info(CKOrganizationBaseInfo base_info) {
        this.base_info = base_info;
    }

    public CKOrgRecommendInfo getOrg_recommend_info() {
        return org_recommend_info;
    }

    public void setOrg_recommend_info(CKOrgRecommendInfo org_recommend_info) {
        this.org_recommend_info = org_recommend_info;
    }

    public CKProdInfo getProd_info() {
        return prod_info;
    }

    public void setProd_info(CKProdInfo prod_info) {
        this.prod_info = prod_info;
    }

    public CKPostInfo getPost_info() {
        return post_info;
    }

    public void setPost_info(CKPostInfo post_info) {
        this.post_info = post_info;
    }
}
