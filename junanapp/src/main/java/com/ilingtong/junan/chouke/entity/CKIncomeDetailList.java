package com.ilingtong.junan.chouke.entity;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 10:23
 * Email: liuting@ilingtong.com
 * Desc:7012接口(取得组织业绩组织酬金明细)的body 明细列表信息
 */
public class CKIncomeDetailList {
    private int data_total_count;//明细总数量
    private List<CKIncomeDetailBean> income_list;//业绩列表

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<CKIncomeDetailBean> getIncome_list() {
        return income_list;
    }

    public void setIncome_list(List<CKIncomeDetailBean> income_list) {
        this.income_list = income_list;
    }

}
