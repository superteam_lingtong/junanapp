package com.ilingtong.junan.chouke.entity;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/7/21.
 * mail: wuqian@ilingtong.com
 * Description:实体类。君安注册入口参数，用于多页面切换时数据缓存
 */
public class JARegisterParam implements Serializable {
    public static final String SYORI_FLAG_REGIST = "0";  //表示新用户注册
    public static final String SYORI_FLAG_COMPLETION = "1";  //表示老用户补全资料
    public String phone = "";    //手机号
    public String password = "";    //密码
    public String id_card = "";    //身份证
    public String name = "";    //姓名
    public String id_card_positive_pic = "";    //身份证照正面
    public String id_card_opposite_pic = "";    //身份证照反面
    public String discharged_pic = "";    //退伍证照
    public String verification_code = "";    //验证码
    public String veterans_card_number= "";  //退伍证号码
    public String referee_id = "";    //推荐码
    public String is_veterans = "";  //是否退伍军人
    public String syori_flg = "";   //处理区分 0:表注册并提交资料。1:表示已注册，只提交审核资料
    public String register_user_id = "";  //注册用户id。新注册用户为空
    public String arm_id="";   //兵种ID，不上传退役证的情况可以为空，上传了退役证则不能为空
    public String applicant_type_id = "";  //申请人类别ID 不上传退役证的情况可以为空，上传了退役证则不能为空

    public JARegisterParam(String syori_flg) {
        this.syori_flg = syori_flg;
    }
}
