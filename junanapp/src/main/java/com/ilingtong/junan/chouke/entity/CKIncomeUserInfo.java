package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 10:27
 * Email: liuting@ilingtong.com
 * Desc:used by 7012接口(取得组织业绩组织酬金明细)，7013接口(取得我的个人业绩明细) 用户信息
 */
public class CKIncomeUserInfo {
    private String user_id;//用户ID
    private String user_name;//用户名称
    private String user_nick_name;//用户昵称
    private String user_signature;//用户签名
    private String user_head_photo_url;//用户头像
    private String is_amateur_member;//是否为业余组织成员 0:是 1:否

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_nick_name() {
        return user_nick_name;
    }

    public void setUser_nick_name(String user_nick_name) {
        this.user_nick_name = user_nick_name;
    }

    public String getUser_signature() {
        return user_signature;
    }

    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }

    public String getUser_head_photo_url() {
        return user_head_photo_url;
    }

    public void setUser_head_photo_url(String user_head_photo_url) {
        this.user_head_photo_url = user_head_photo_url;
    }

    public String getIs_amateur_member() {
        return is_amateur_member;
    }

    public void setIs_amateur_member(String is_amateur_member) {
        this.is_amateur_member = is_amateur_member;
    }
}
