package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 14:54
 * Email: liuting@ilingtong.com
 * Desc:use by 7001接口(获取我的基本信息) 粉丝信息
 */
public class CKFansInfo {
    private int payoff_count;//酬客数量
    private int fans_direct_count;//直粉数量
    private int fans_tl_count;//通粉数量

    public int getPayoff_count() {
        return payoff_count;
    }

    public void setPayoff_count(int payoff_count) {
        this.payoff_count = payoff_count;
    }

    public int getFans_direct_count() {
        return fans_direct_count;
    }

    public void setFans_direct_count(int fans_direct_count) {
        this.fans_direct_count = fans_direct_count;
    }

    public int getFans_tl_count() {
        return fans_tl_count;
    }

    public void setFans_tl_count(int fans_tl_count) {
        this.fans_tl_count = fans_tl_count;
    }
}
