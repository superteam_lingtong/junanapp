package com.ilingtong.junan.chouke.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ilingtong.junan.chouke.CKConstant;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.fragment.MyFansFragment;
import com.ilingtong.library.tongle.activity.BaseFragmentActivity;
import com.ilingtong.library.tongle.adapter.CollectFragmentPagerAdapter;

/**
 * Created by wuqian on 2016/5/26.
 * mail: wuqian@ilingtong.com
 * Description:我的酬脉
 */
public class MyChoukeFansFragmentActivity extends BaseFragmentActivity {
    private RadioGroup myFansRg;
    private ViewPager myFansViewpager;
    private Fragment[] fragments = new Fragment[3];
    private int type;

    public static void luanch(Activity activity, int fragmenttype) {
        Intent intent = new Intent(activity, MyChoukeFansFragmentActivity.class);
        intent.putExtra("type", fragmenttype);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ck_activity_my_fans_layout);
        type = getIntent().getIntExtra("type", 0);
        initView();
    }

    /**
     * 初始化界面
     */
    private void initView() {
        ImageView img_back = (ImageView) findViewById(R.id.left_arrow_btn);
        TextView txt_title = (TextView) findViewById(R.id.top_name);
        txt_title.setText(R.string.my_choumai);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyChoukeFansFragmentActivity.this.finish();
            }
        });
        img_back.setVisibility(View.VISIBLE);
        txt_title.setVisibility(View.VISIBLE);
        myFansRg = (RadioGroup) findViewById(R.id.my_fans_rg);
        myFansViewpager = (ViewPager) findViewById(R.id.my_fans_viewpager);

        fragments[0] = MyFansFragment.newInstance(CKConstant.FRAGMENT_TYPE_CHOUKE);
        fragments[1] = MyFansFragment.newInstance(CKConstant.FRAGMENT_TYPE_DERICT_FANS);
        fragments[2] = MyFansFragment.newInstance(CKConstant.FRAGMENT_TYPE_TONGLE_FANS);
        CollectFragmentPagerAdapter adapter = new CollectFragmentPagerAdapter(getSupportFragmentManager(), fragments);
        myFansViewpager.setAdapter(adapter);
        myFansViewpager.setOffscreenPageLimit(3);
        if (type == CKConstant.FRAGMENT_TYPE_CHOUKE) {
            myFansRg.check(R.id.my_fans_rbt_chouke);
        } else if (type == CKConstant.FRAGMENT_TYPE_DERICT_FANS) {
            myFansRg.check(R.id.my_fans_rbt_direct_fans);
        } else {
            myFansRg.check(R.id.my_fans_rbt_tongle_fans);
        }
        myFansViewpager.setCurrentItem(type);
        myFansViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        myFansRg.check(R.id.my_fans_rbt_chouke);
                        break;
                    case 1:
                        myFansRg.check(R.id.my_fans_rbt_direct_fans);
                        break;
                    case 2:
                        myFansRg.check(R.id.my_fans_rbt_tongle_fans);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /**
         * 点击radiobutton 切换fragment
         */
        myFansRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.my_fans_rbt_chouke:
                        myFansViewpager.setCurrentItem(CKConstant.FRAGMENT_TYPE_CHOUKE);
                        break;
                    case R.id.my_fans_rbt_direct_fans:
                        myFansViewpager.setCurrentItem(CKConstant.FRAGMENT_TYPE_DERICT_FANS);
                        break;
                    case R.id.my_fans_rbt_tongle_fans:
                        myFansViewpager.setCurrentItem(CKConstant.FRAGMENT_TYPE_TONGLE_FANS);
                        break;
                }
            }
        });
    }
}
