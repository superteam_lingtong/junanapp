package com.ilingtong.junan.chouke.entity;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/4/13.
 * mail: wuqian@ilingtong.com
 * Description:兵种信息
 */
public class ArmInfo implements Serializable{
    public String arm_id;   //兵种id
    public String arm;   //兵种名称

}
