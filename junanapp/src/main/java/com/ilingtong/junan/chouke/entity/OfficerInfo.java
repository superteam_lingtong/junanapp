package com.ilingtong.junan.chouke.entity;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/4/13.
 * mail: wuqian@ilingtong.com
 * Description: 军人类型信息
 */
public class OfficerInfo implements Serializable {
    public String applicant_type_id;   //类型Id
    public String applicant_type; //类型名称

}
