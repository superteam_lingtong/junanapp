package com.ilingtong.junan.chouke.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.utils.ImageLoaderOptionsUtils;
import com.ilingtong.library.tongle.protocol.FriendListItem;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by wuqian on 2016/5/26.
 * mail: wuqian@ilingtong.com
 * Description:我的酬客/直粉/通粉 列表的 adapter
 */
public class MyFansListAdapter extends BaseAdapter {
    private List<FriendListItem> fans_list;
    private Context context;

    public MyFansListAdapter(List<FriendListItem> fans_list, Context context) {
        this.fans_list = fans_list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return fans_list.size();
    }

    @Override
    public Object getItem(int position) {
        return fans_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.ck_fragment_fans_list_item_layout, null);
            holder.img_head = (ImageView) convertView.findViewById(R.id.fans_list_item_img_head);
            holder.txt_nickname = (TextView) convertView.findViewById(R.id.fans_list_item_txt_nickname);
            holder.txt_jointime = (TextView) convertView.findViewById(R.id.fans_list_item_txt_jointime);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ImageLoader.getInstance().displayImage(fans_list.get(position).user_head_photo_url, holder.img_head, ImageLoaderOptionsUtils.fansHeadOption());
        holder.txt_nickname.setText(fans_list.get(position).user_nick_name);
        holder.txt_jointime.setText(fans_list.get(position).join_time);
        return convertView;
    }

    class ViewHolder {
        ImageView img_head;
        TextView txt_nickname;
        TextView txt_jointime;
    }
}
