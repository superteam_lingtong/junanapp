package com.ilingtong.junan.chouke.entity;//package com.ilingtong.app.chouke.entity;
//
///**
// * author: liuting
// * Date: 2016/5/17
// * Time: 16:50
// * Email: liuting@ilingtong.com
// * Desc:used by 7006接口(获取我的酬客列表)，7011接口(获取我的直粉列表) 酬客(直粉)列表
// */
//public class CKPayoffList {
//    private String user_id;//用户ID
//    private String user_nick_name;//用户昵称
//    private String user_signature;//用户签名
//    private String user_head_photo_url;//用户头像URL
//    private String latest_post_info;//最近发帖内容
//    private int latest_post_count;//未读新帖数
//    private String post_update_time;//帖子最近更新时间
//    private String join_time;//关注时间
//    private String user_favorited_by_me;//是否被当前用户收藏  0:是 1:否
//
//    public String getUser_id() {
//        return user_id;
//    }
//
//    public void setUser_id(String user_id) {
//        this.user_id = user_id;
//    }
//
//    public String getUser_nick_name() {
//        return user_nick_name;
//    }
//
//    public void setUser_nick_name(String user_nick_name) {
//        this.user_nick_name = user_nick_name;
//    }
//
//    public String getUser_signature() {
//        return user_signature;
//    }
//
//    public void setUser_signature(String user_signature) {
//        this.user_signature = user_signature;
//    }
//
//    public String getUser_head_photo_url() {
//        return user_head_photo_url;
//    }
//
//    public void setUser_head_photo_url(String user_head_photo_url) {
//        this.user_head_photo_url = user_head_photo_url;
//    }
//
//    public String getLatest_post_info() {
//        return latest_post_info;
//    }
//
//    public void setLatest_post_info(String latest_post_info) {
//        this.latest_post_info = latest_post_info;
//    }
//
//    public int getLatest_post_count() {
//        return latest_post_count;
//    }
//
//    public void setLatest_post_count(int latest_post_count) {
//        this.latest_post_count = latest_post_count;
//    }
//
//    public String getPost_update_time() {
//        return post_update_time;
//    }
//
//    public void setPost_update_time(String post_update_time) {
//        this.post_update_time = post_update_time;
//    }
//
//    public String getJoin_time() {
//        return join_time;
//    }
//
//    public void setJoin_time(String join_time) {
//        this.join_time = join_time;
//    }
//
//    public String getUser_favorited_by_me() {
//        return user_favorited_by_me;
//    }
//
//    public void setUser_favorited_by_me(String user_favorited_by_me) {
//        this.user_favorited_by_me = user_favorited_by_me;
//    }
//}
