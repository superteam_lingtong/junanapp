package com.ilingtong.junan.chouke;

/**
 * Created by wuqian on 2016/5/26.
 * mail: wuqian@ilingtong.com
 * Description:常量类
 */
public class CKConstant {
    public static int FRAGMENT_TYPE_CHOUKE = 0; //酬客
    public static int FRAGMENT_TYPE_DERICT_FANS = 1;  //直粉
    public static int FRAGMENT_TYPE_TONGLE_FANS = 2;  //通粉

    public static String QRCODE_TYPE_ORG = "6";  //二维码类别 -- 组织

    public static final String REGISTER_INTO_TYPE= "0";//类型，0代表从注册进入
    public static final String LOGIN_INTO_TYPE= "1";//类型，1代表从登录进入

    //测试环境下首页商品跳转h5页面的url
   public static final String HOME_PRODUCT_HOST_URL = "http://www.tongler.cn/apph5/2.0/";  //测试版地址
   // public static final String HOME_PRODUCT_HOST_URL="http://117.78.37.252/jaapph5/1.0/";
    public static final String HOME_DOMESTIC_PRODUCT_TYPE="001"; //国内商品标示 ADD 2016/07/25
    public static final String HOME_ABROAD_PRODUCT_TYPE="101"; //国外商品标示 ADD 2016/07/25
    public static final String HOME_BETTER_TYPE="201"; //优抚专区标示 ADD 2016/07/27

    public static final String HEADUSER = "31600000213";    //官方发布新闻和任务的userid
}
