package com.ilingtong.junan.chouke.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.junan.chouke.CKServiceManager;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.entity.JARegisterParam;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.BaseActivity;
import com.ilingtong.library.tongle.protocol.BaseResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ImageUtil;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.Utils;
import com.ilingtong.library.tongle.widget.SelectDialog;

import java.io.File;
import java.io.IOException;

/**
 * Created by wuqian on 2016/7/20.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class JARegisterIdCardInfoActivity extends BaseActivity implements View.OnClickListener {
    private EditText jaInfoEdtName;
    private EditText jaInfoEdtId;
    private ImageView jaInfoImgAddFront;
    private ImageView jaInfoImgAddOpposite;
    private Button jaInfoBtnNext;
    private ImageView imgBack;
    private TextView txtTitle;
    private Button jaBtnSubmit;

    private String realName;
    private String idCard;

    private AlertDialog alert;

    private String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static final String idCardPositiveFileName = "_positive.png";
    private static final String idCardNegativeFileName = "_negative.png";
    private File positiveImageFile, negativeImageFile;

    private static final int TYPE_POSITIVE = 10003;
    private static final int TYPE_NEGATIVE = 10004;
    private static final int IMG_IDCARD_POSITIVE_REQUEST_CAMERA = 10001;
    private static final int IMG_IDCARD_POSITIVE_REQUEST_GALLERY = 10002;
    private static final int IMG_IDCARD_NEGATIVE_REQUEST_CAMERA = 10005;
    private static final int IMG_IDCARD_NEGATIVE_REQUEST_GALLERY = 10006;
    private static final int NEXTREQUESTCODE = 10007;

    private JARegisterParam registerParam;
    public static JARegisterIdCardInfoActivity instance = null;
    private Dialog mDialog;

    private String[] needPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};//存储和相机权限
    private RequestPermissionUtils requestPermissionUtils;//请求权限类

    private String idCardPositivePath, idCardNegativePath;   //用于缓存图片路径

    /**
     * 跳转至该activity。适用于新用户注册。
     *
     * @param activity
     * @param registerParam 注册参数实体。
     */
    public static void launch(Activity activity, JARegisterParam registerParam) {
        Intent intent = new Intent(activity, JARegisterIdCardInfoActivity.class);
        intent.putExtra("registerParam", registerParam);
        activity.startActivityForResult(intent, JARegisterActivity.NEXTREQUESTCODE);
    }

    /**
     * 跳转至该activity。适用于老用户补全资料
     *
     * @param activity
     */
    public static void launch(Activity activity, String phone, String registerUserId) {
        Intent intent = new Intent(activity, JARegisterIdCardInfoActivity.class);
        JARegisterParam registerParam = new JARegisterParam(JARegisterParam.SYORI_FLAG_COMPLETION);
        registerParam.phone = phone;
        registerParam.register_user_id = registerUserId;
        intent.putExtra("registerParam", registerParam);
        activity.startActivityForResult(intent, JARegisterActivity.NEXTREQUESTCODE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (positiveImageFile != null) {
            outState.putString("positiveImagePath", positiveImageFile.getAbsolutePath());
        }
        if (negativeImageFile != null) {
            outState.putString("negativeImagePath", negativeImageFile.getAbsolutePath());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
        positiveImageFile = new File(savedInstanceState.getString("positiveImagePath"));
        negativeImageFile = new File(savedInstanceState.getString("negativeImagePath"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ja_id_info_view);
        registerParam = (JARegisterParam) getIntent().getSerializableExtra("registerParam");
        instance = this;
        initView();
        initData();
    }

    /**
     * 初始化
     */
    private void initView() {

        mDialog = DialogUtils.createLoadingDialog(this);
        imgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        txtTitle = (TextView) findViewById(R.id.top_name);
        txtTitle.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        txtTitle.setText(R.string.ja_idcard_info);
        imgBack.setOnClickListener(this);

        jaInfoEdtName = (EditText) findViewById(R.id.ja_info_edt_name);
        jaInfoEdtId = (EditText) findViewById(R.id.ja_info_edt_id);
        jaInfoImgAddFront = (ImageView) findViewById(R.id.ja_info_img_add_front);
        jaInfoImgAddOpposite = (ImageView) findViewById(R.id.ja_info_img_add_opposite);
        jaInfoBtnNext = (Button) findViewById(R.id.ja_info_btn_next);
        jaBtnSubmit = (Button) findViewById(R.id.ja_info_btn_submit);

        jaInfoEdtName.addTextChangedListener(textChange);
        jaInfoEdtId.addTextChangedListener(textChange);
        jaInfoBtnNext.setOnClickListener(this);
        jaInfoImgAddOpposite.setOnClickListener(this);
        jaInfoImgAddFront.setOnClickListener(this);
        jaBtnSubmit.setOnClickListener(this);

    }

    /**
     * 赋值
     */
    private void initData() {
        jaInfoEdtName.setText(registerParam.name + "");
        jaInfoEdtId.setText(registerParam.id_card + "");
        if (!TextUtils.isEmpty(registerParam.id_card_positive_pic)) {
            positiveImageFile = new File(registerParam.id_card_positive_pic);
            jaInfoImgAddFront.setImageURI(Uri.fromFile(positiveImageFile));
        }
        if (!TextUtils.isEmpty(registerParam.id_card_positive_pic)) {
            negativeImageFile = new File(registerParam.id_card_opposite_pic);
            jaInfoImgAddOpposite.setImageURI(Uri.fromFile(negativeImageFile));
        }
    }

    /**
     * 输入框监听
     */
    private TextWatcher textChange = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            idCard = jaInfoEdtId.getText().toString();
            realName = jaInfoEdtName.getText().toString();

            if (idCard.equals("") || realName.equals("")) {
                jaInfoBtnNext.setEnabled(false);
                jaBtnSubmit.setEnabled(false);
            } else {
                jaInfoBtnNext.setEnabled(true);
                jaBtnSubmit.setEnabled(true);
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == IMG_IDCARD_POSITIVE_REQUEST_GALLERY) {
                if (data != null) {
                    showImage(data.getData(), TYPE_POSITIVE);
                }
            } else if (requestCode == IMG_IDCARD_POSITIVE_REQUEST_CAMERA) {
                if (hasSdcard()) {
                    showImage(Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), idCardPositiveFileName)), TYPE_POSITIVE);
                } else {
                    ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.common_sd_null));
                }
            } else if (requestCode == IMG_IDCARD_NEGATIVE_REQUEST_CAMERA) {
                if (hasSdcard()) {
                    showImage(Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), idCardNegativeFileName)), TYPE_NEGATIVE);
                } else {
                    ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.common_sd_null));
                }
            } else if (requestCode == IMG_IDCARD_NEGATIVE_REQUEST_GALLERY) {
                if (data != null) {
                    showImage(data.getData(), TYPE_NEGATIVE);
                }
            } else if (requestCode == NEXTREQUESTCODE) {
                //缓存数据
                registerParam = (JARegisterParam) data.getSerializableExtra("registerParam");
            }
            if (alert != null) {
                alert.dismiss();
            }
        }
    }

    /**
     * 显示图片 压缩 保存
     *
     * @param uri
     * @param type
     */
    private void showImage(Uri uri, int type) {
        Bitmap photoBitmap = null;
        if (uri != null) {
            try {
                photoBitmap = ImageUtil.getBitmapFormUri(this, uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        switch (type) {
            case TYPE_POSITIVE:
                jaInfoImgAddFront.setImageBitmap(photoBitmap);
                ImageUtil.saveImg(photoBitmap, dirPath, idCardPositiveFileName);
                registerParam.id_card_positive_pic = dirPath + "/" + idCardPositiveFileName;
                break;
            case TYPE_NEGATIVE:
                jaInfoImgAddOpposite.setImageBitmap(photoBitmap);
                ImageUtil.saveImg(photoBitmap, dirPath, idCardNegativeFileName);
                registerParam.id_card_opposite_pic = dirPath + "/" + idCardNegativeFileName;
                break;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ja_info_img_add_front:
                positiveImageFile = new File(Environment
                        .getExternalStorageDirectory(), idCardPositiveFileName);
                getIdCardImg(IMG_IDCARD_POSITIVE_REQUEST_CAMERA, IMG_IDCARD_POSITIVE_REQUEST_GALLERY, idCardPositiveFileName);
                break;
            case R.id.ja_info_img_add_opposite:
                positiveImageFile = new File(Environment
                        .getExternalStorageDirectory(), idCardNegativeFileName);
                getIdCardImg(IMG_IDCARD_NEGATIVE_REQUEST_CAMERA, IMG_IDCARD_NEGATIVE_REQUEST_GALLERY, idCardNegativeFileName);
                break;
            case R.id.ja_info_btn_next:
                if (checkParam()) {
                    registerParam.name = jaInfoEdtName.getText().toString();
                    registerParam.id_card = jaInfoEdtId.getText().toString();
                    Intent intent = new Intent(JARegisterIdCardInfoActivity.this, JARegisterOfficerInfoActivity.class);
                    intent.putExtra("registerParam", registerParam);
                    startActivityForResult(intent, NEXTREQUESTCODE);
                }
                break;
            case R.id.ja_info_btn_submit:
                if (checkParam()) {
                    mDialog.show();
                    idCardNegativePath = registerParam.id_card_opposite_pic;
                    idCardPositivePath = registerParam.id_card_positive_pic;

                    // registerParam.is_veterans为 TongleAppConst.NO。表示不是退伍军人，以下参数的值应为空值
                    registerParam.discharged_pic = "";
                    registerParam.veterans_card_number = "";
                    registerParam.arm_id = "";
                    registerParam.applicant_type_id = "";

                    registerParam.name = jaInfoEdtName.getText().toString();
                    registerParam.id_card = jaInfoEdtId.getText().toString();
                    registerParam.is_veterans = TongleAppConst.NO;
                    registerParam.id_card_positive_pic = ImageUtil.bitmaptoString(BitmapFactory.decodeFile(registerParam.id_card_positive_pic));
                    registerParam.id_card_opposite_pic = ImageUtil.bitmaptoString(BitmapFactory.decodeFile(registerParam.id_card_opposite_pic));
                    CKServiceManager.JARegister(registerParam, successListener(), new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            mDialog.dismiss();
                            ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
                            resetPicPath();
                        }
                    });
                }
                break;
            case R.id.left_arrow_btn:
                this.finish();
                break;
        }
    }

    /**
     * 提交信息失败，重新设置图片路径。
     */
    public void resetPicPath() {
        registerParam.id_card_opposite_pic = idCardNegativePath;
        registerParam.id_card_positive_pic = idCardPositivePath;
    }

    /**
     * 检查身份证是否上传；身份证格式是否合法
     *
     * @return 图片存在，输入的身份证合法则返回true。否则返回false
     */
    private boolean checkParam() {
        if (TextUtils.isEmpty(registerParam.id_card_positive_pic)) {
            ToastUtils.toastShort("请上传身份证反面");
            return false;
        }
        if (TextUtils.isEmpty(registerParam.id_card_opposite_pic)) {
            ToastUtils.toastShort("请上传身份证正面");
            return false;
        }
        if (jaInfoEdtId.getText().toString().length() != TongleAppConst.ID_NUMBER_LENGTH_five && idCard.length() != TongleAppConst.ID_NUMBER_LENGTH_eight) {
            ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.setting_my_id_card_error));
            return false;
        }
        if (!Utils.isIdCardcode(jaInfoEdtId.getText().toString())) {
            ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.setting_my_id_card_error));
            return false;
        }
        return true;
    }

    /**
     * 提交审核成功
     *
     * @return
     */
    private Response.Listener<BaseResult> successListener() {
        return new Response.Listener<BaseResult>() {
            @Override
            public void onResponse(BaseResult result) {

                mDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(result.getHead().getReturn_flag())) {
                    ToastUtils.toastShort("提交成功，请耐心等待审核");
                    if (JARegisterActivity.instance != null) {
                        JARegisterActivity.instance.finish();
                    }
                    JARegisterIdCardInfoActivity.instance.finish();
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + result.getHead().getReturn_message());
                    resetPicPath();
                }
            }
        };
    }

    /**
     * 身份证照片正反面内容填充
     * 三个参数需正确传入，用来区分拍照/相册中选择，正面/反面view
     *
     * @param requestCodeCamera  拍照时requestCode
     * @param requestCodeGallery 从相册中选择照片 requestCode
     */
    private void getIdCardImg(final int requestCodeCamera, final int requestCodeGallery, final String fileName) {
        View head_view = LayoutInflater.from(this).inflate(com.ilingtong.library.tongle.R.layout.headview_dialog_layout, null);
        TextView takephoto = (TextView) head_view.findViewById(com.ilingtong.library.tongle.R.id.takePhoto);
        TextView myPicture = (TextView) head_view.findViewById(com.ilingtong.library.tongle.R.id.myPicture);
        TextView dialog_cancle = (TextView) head_view.findViewById(com.ilingtong.library.tongle.R.id.dialog_cancle);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(head_view);
        alert = builder.create();

        //拍照
        takephoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(JARegisterIdCardInfoActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                    @Override
                    public void requestSuccess() {
                        //拍照
                        camera(requestCodeCamera, fileName);
                        alert.dismiss();
                    }

                    @Override
                    public void requestFail() {
                        SelectDialog dialog = new SelectDialog(JARegisterIdCardInfoActivity.this, getString(R.string.please_open_camera_storage_permission));
                        dialog.show();
                    }
                });
                requestPermissionUtils.checkPermissions(JARegisterIdCardInfoActivity.this);
            }
        });
        //我的相册
        myPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(JARegisterIdCardInfoActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                    @Override
                    public void requestSuccess() {
                        //拍照
                        gallery(requestCodeGallery);
                    }
                    @Override
                    public void requestFail() {
                        SelectDialog dialog = new SelectDialog(JARegisterIdCardInfoActivity.this, getString(R.string.please_open_camera_storage_permission));
                        dialog.show();
                    }
                });
                requestPermissionUtils.checkPermissions(JARegisterIdCardInfoActivity.this);
            }
        });
        //取消
        dialog_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.setCanceledOnTouchOutside(true);
        alert.show();

    }

    /*
    * 从相册获取
    */
    public void gallery(int requestCode) {
        // 激活系统图库，选择一张图片
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, requestCode);
    }

    /*
    * 拍照
    */
    public void camera(int requestCode, String fileName) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        // 判断存储卡是否可以用，可用进行存储
        if (hasSdcard()) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), fileName)));
        }
        startActivityForResult(intent, requestCode);
    }

    private boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void finish() {
        Intent intent = new Intent();
        registerParam.name = jaInfoEdtName.getText().toString();
        registerParam.id_card = jaInfoEdtId.getText().toString();
        intent.putExtra("registerParam", registerParam);
        setResult(RESULT_OK, intent);
        super.finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}
