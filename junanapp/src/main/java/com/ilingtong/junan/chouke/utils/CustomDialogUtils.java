package com.ilingtong.junan.chouke.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.widget.MyDialog;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * author: liuting
 * Date: 2016/5/25
 * Time: 9:20
 * Email: liuting@ilingtong.com
 * Desc:对话框类
 */
public class CustomDialogUtils {

    /**
     * @param context
     * @param message  对话框message
     * @param cancelListener   取消按钮监听
     * @param sureListener     确认按钮监听
     * @return
     */
    public static MyDialog showCommonDialog(Context context, String message, View.OnClickListener cancelListener,View.OnClickListener sureListener) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(context).inflate(R.layout.ck_exit_org_dialog_layout, null);
//        builder.setView(view);
//        mCommonAlert = builder.create();
        MyDialog myDialog=new MyDialog(context, DipUtils.px2dip(280),0,view,R.style.ck_common_dialog_style);
        myDialog.setCanceledOnTouchOutside(true);

        Button btnCancel = (Button) view.findViewById(R.id.exit_org_dialog_btn_cancel);
        Button btnSure = (Button) view.findViewById(R.id.exit_org_dialog_btn_sure);
        TextView msgTxt = (TextView) view.findViewById(R.id.exit_org_dialog_txt_msg);
        msgTxt.setText(message);
        btnCancel.setOnClickListener(cancelListener);
        btnSure.setOnClickListener(sureListener);
        myDialog.show();
        return myDialog;
    }

    /**
     * @param context
     * @param url
     * @return
     */
    public static AlertDialog showQRDialog(Context context,String url) {
        View view = LayoutInflater.from(context).inflate(com.ilingtong.library.tongle.R.layout.expert_popwindow_layout, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context, com.ilingtong.library.tongle.R.style.Dialog_FS);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        ImageView coverImage = (ImageView) view.findViewById(com.ilingtong.library.tongle.R.id.qr_user);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        ImageLoader.getInstance().displayImage(url, coverImage, ImageOptionsUtils.getOptions());
        alertDialog.show();
        return alertDialog;
    }
}
