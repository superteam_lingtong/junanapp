package com.ilingtong.junan.chouke.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.junan.chouke.CKServiceManager;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.entity.ArmInfo;
import com.ilingtong.junan.chouke.entity.ArmListResult;
import com.ilingtong.junan.chouke.entity.JARegisterParam;
import com.ilingtong.junan.chouke.entity.OfficerInfo;
import com.ilingtong.junan.chouke.entity.OfficerListResult;
import com.ilingtong.junan.chouke.widget.RetireSelectDialog;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.BaseActivity;
import com.ilingtong.library.tongle.protocol.BaseResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ImageUtil;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuqian on 2016/7/21.
 * mail: wuqian@ilingtong.com
 * Description:退役证信息
 */
public class JARegisterOfficerInfoActivity extends BaseActivity implements View.OnClickListener {
    private EditText jaRetireEdtNumber;
    private ImageView jaRetireImgAddCard;
    private Button jaRetireBtnSubmit;
    private JARegisterParam registerParam;
    private Dialog dialog;
    private ImageView imgBack;
    private TextView txtTitle;
    private EditText jaRetireEditArm;  //兵种
    private EditText jaRetireEditType;  //军官类型 如：退役/军属等

    private String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private AlertDialog alert;
    private static final int IMG_OFFICER_CAMERA = 10001;
    private static final int IMG_OFFICER_GALLERY = 10002;
    private String officerImgFileName = "_officer.png";

    private String[] needPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};//存储和相机权限
    private RequestPermissionUtils requestPermissionUtils;//请求权限类
    private RetireSelectDialog dialogSelect;//选择Dialog
    private List<ArmInfo> listArm;//兵种列表
    private List<OfficerInfo> listOfficer;//类型列表
    private int mArmIndex;//兵种列表索引号
    private int mOfficerIndex;//类型列表索引号
    private String idCardPositivePath,idCardNegativePath,retireImgPath;   //用于缓存图片路径
    private static final int GET_ARM_DATA_INIT = 0;//初始获取军种数据
    private static final int GET_ARM_DATA_LOAD = 1;//后期再次请求军种数据
    private static final int GET_OFFICER_DATA_INIT = 2;//初始获取类型数据
    private static final int GET_OFFICER_DATA_LOAD = 3;//后期再次请求类型数据

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case GET_ARM_DATA_INIT://初始获取军种数据
                    break;
                case GET_ARM_DATA_LOAD://后期再次请求军种数据
                    //请求数据之后，如果有数据就显示 Dialog，没有则提示无相关信息
                    if(listArm != null && listArm.size() > 0){
                        showSelectArmDialog();
                    }else{
                        ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.layout_replace_txt));
                    }
                    break;
                case GET_OFFICER_DATA_INIT://初始获取类型数据
                    break;
                case GET_OFFICER_DATA_LOAD://后期再次请求类型数据
                    //请求数据之后，如果有数据就显示 Dialog，没有则提示无相关信息
                    if (listOfficer != null && listOfficer.size() > 0) {
                        showSelectOfficerDialog();
                    }else{
                        ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.layout_replace_txt));
                    }
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ja_retire_card_view);
        registerParam = (JARegisterParam) getIntent().getSerializableExtra("registerParam");
        initView();
        initData();
    }

    /**
     * 初始化
     */
    private void initView() {

        imgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        txtTitle = (TextView) findViewById(R.id.top_name);
        txtTitle.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        txtTitle.setText(R.string.officer_info);
        imgBack.setOnClickListener(this);

        dialog = DialogUtils.createLoadingDialog(this);
        jaRetireEdtNumber = (EditText) findViewById(R.id.ja_retire_edt_number);
        jaRetireImgAddCard = (ImageView) findViewById(R.id.ja_retire_img_add_card);
        jaRetireBtnSubmit = (Button) findViewById(R.id.ja_retire_btn_submit);
        jaRetireEditArm = (EditText) findViewById(R.id.ja_retire_edt_services);
        jaRetireEditType = (EditText) findViewById(R.id.ja_retire_edt_type);

        jaRetireEdtNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                jaRetireBtnSubmit.setEnabled(!TextUtils.isEmpty(jaRetireEdtNumber.getText().toString()));
            }
        });
        jaRetireBtnSubmit.setOnClickListener(this);
        jaRetireImgAddCard.setOnClickListener(this);

        listArm = new ArrayList<>();
        listOfficer = new ArrayList<>();
        jaRetireEditArm.setOnClickListener(this);
        jaRetireEditType.setOnClickListener(this);
    }

    /**
     * 赋值
     */
    private void initData() {
        jaRetireEdtNumber.setText(registerParam.veterans_card_number + "");
        if (!TextUtils.isEmpty(registerParam.discharged_pic)) {
            jaRetireImgAddCard.setImageURI(Uri.fromFile(new File(registerParam.discharged_pic)));
        }
        getArmData(GET_ARM_DATA_INIT);
        getTypeData(GET_OFFICER_DATA_INIT);
    }

    /**
     * 获取军种数据
     */
    private void getArmData(final int type) {
        dialog.show();
        CKServiceManager.getArmList(new Response.Listener<ArmListResult>() {
            @Override
            public void onResponse(ArmListResult armListResult) {
                dialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(armListResult.getHead().getReturn_flag())) {
                    if (listArm != null && listArm.size() > 0) {
                        listArm.clear();
                    }
                    listArm.addAll(armListResult.body.arm_list);
                    handler.sendEmptyMessage(type);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + armListResult.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
            }
        });
    }

    /**
     * 获取军官类型数据
     */
    private void getTypeData(final int type) {
        dialog.show();
        CKServiceManager.getOfficerTypeList(new Response.Listener<OfficerListResult>() {
            @Override
            public void onResponse(OfficerListResult officerListResult) {
                dialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(officerListResult.getHead().getReturn_flag())) {
                    if (listOfficer != null && listOfficer.size() > 0) {
                        listOfficer.clear();
                    }
                    listOfficer.addAll(officerListResult.body.applicant_type_list);
                    handler.sendEmptyMessage(type);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + officerListResult.getHead().getReturn_message());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ja_retire_img_add_card:
                getIdCardImg(IMG_OFFICER_CAMERA, IMG_OFFICER_GALLERY, officerImgFileName);
                break;
            case R.id.ja_retire_btn_submit:
                if (TextUtils.isEmpty(registerParam.discharged_pic)) {
                    ToastUtils.toastShort("请上传退役证照片");
                    return;
                }
                if (TextUtils.isEmpty(jaRetireEditArm.getText().toString())) {
                    ToastUtils.toastShort("请选择军种");
                    return;
                }
                if (TextUtils.isEmpty(jaRetireEditType.getText().toString())) {
                    ToastUtils.toastShort("请选择类型");
                    return;
                }
                registerParam.arm_id = listArm.get(mArmIndex).arm_id.toString();
                registerParam.applicant_type_id = listOfficer.get(mOfficerIndex).applicant_type_id.toString();
                registerParam.veterans_card_number = jaRetireEdtNumber.getText().toString();
                registerParam.is_veterans = TongleAppConst.YES;
                //缓存图片路径，如果接口调用失败，恢复路径。
                idCardPositivePath = registerParam.id_card_positive_pic;
                idCardNegativePath = registerParam.id_card_opposite_pic;
                retireImgPath = registerParam.discharged_pic;
                registerParam.discharged_pic = ImageUtil.bitmaptoString(BitmapFactory.decodeFile(registerParam.discharged_pic));
                registerParam.id_card_opposite_pic = ImageUtil.bitmaptoString(BitmapFactory.decodeFile(registerParam.id_card_opposite_pic));
                registerParam.id_card_positive_pic = ImageUtil.bitmaptoString(BitmapFactory.decodeFile(registerParam.id_card_positive_pic));
                dialog.show();
                CKServiceManager.JARegister(registerParam, successListener(), new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        dialog.dismiss();
                        ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
                        //接口调用失败，恢复图片参数的值为路径
                        registerParam.id_card_positive_pic = idCardPositivePath;
                        registerParam.id_card_opposite_pic = idCardNegativePath;
                        registerParam.discharged_pic = retireImgPath;
                    }
                });
                break;
            case R.id.left_arrow_btn:
                this.finish();
                break;
            case R.id.ja_retire_edt_services://军种选择
                selectArm();
                break;
            case R.id.ja_retire_edt_type://类型
                selectType();
                break;
        }
    }

    /**
     * 选择军种
     */
    private void selectArm() {
        //没有获取到数据则再请求一遍数据，有则显示选择Dialog
        if(listArm != null && listArm.size() > 0){
            showSelectArmDialog();
        }else{
            getArmData(GET_ARM_DATA_LOAD);
        }
    }

    /**
     * 选择军官类型
     */
    private void selectType() {
        //没有获取到数据则再请求一遍数据，有则显示选择Dialog
        if(listOfficer != null && listOfficer.size() > 0){
            showSelectOfficerDialog();
        }else{
            getTypeData(GET_OFFICER_DATA_LOAD);
        }
    }

    /**
     * 显示选择军官类型 Dialog
     */
    private void showSelectOfficerDialog() {
        dialogSelect = new RetireSelectDialog(JARegisterOfficerInfoActivity.this, null, listOfficer, new RetireSelectDialog.IOnClickListener() {
            @Override
            public void onCancelListener(View view) {
                dialogSelect.dismiss();
            }

            @Override
            public void onSureListener(View view, int position) {
                dialogSelect.dismiss();
                jaRetireEditType.setText(listOfficer.get(position).applicant_type.toString().trim());
                mOfficerIndex = position;
            }
        });
        dialogSelect.show();
    }

    /**
     * 显示选择军种 Dialog
     */
    private void showSelectArmDialog() {
        dialogSelect = new RetireSelectDialog(JARegisterOfficerInfoActivity.this, listArm, null, new RetireSelectDialog.IOnClickListener() {
            @Override
            public void onCancelListener(View view) {
                dialogSelect.dismiss();
            }

            @Override
            public void onSureListener(View view, int position) {
                dialogSelect.dismiss();
                jaRetireEditArm.setText(listArm.get(position).arm.toString().trim());
                mArmIndex = position;
            }
        });
        dialogSelect.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == IMG_OFFICER_GALLERY) {
                if (data != null) {
                    showImage(data.getData());
                }

            } else if (requestCode == IMG_OFFICER_CAMERA) {
                if (hasSdcard()) {
                    showImage(Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), officerImgFileName)));
                } else {
                    ToastUtils.toastShort(getString(com.ilingtong.library.tongle.R.string.common_sd_null));
                }

            }
            alert.dismiss();
        }
    }

    /**
     * 显示及保存图片
     *
     * @param uri
     */
    private void showImage(Uri uri) {
        Bitmap photoBitmap = null;
        if (uri != null) {
            try {
                photoBitmap = ImageUtil.getBitmapFormUri(this, uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        jaRetireImgAddCard.setImageBitmap(photoBitmap);
        ImageUtil.saveImg(photoBitmap, dirPath, officerImgFileName);
        registerParam.discharged_pic = dirPath + "/" + officerImgFileName;
    }

    /**
     * 提交审核成功
     *
     * @return
     */
    private Response.Listener<BaseResult> successListener() {
        return new Response.Listener<BaseResult>() {
            @Override
            public void onResponse(BaseResult result) {

                dialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(result.getHead().getReturn_flag())) {
                    ToastUtils.toastShort("提交成功，请耐心等待审核");
                    if (JARegisterActivity.instance != null) {
                        JARegisterActivity.instance.finish();
                    }
                    if (JARegisterIdCardInfoActivity.instance != null) {
                        JARegisterIdCardInfoActivity.instance.finish();
                    }
                    JARegisterOfficerInfoActivity.this.finish();
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + result.getHead().getReturn_message());
                    //接口调用失败，恢复图片参数的值为路径
                    registerParam.id_card_positive_pic = idCardPositivePath;
                    registerParam.id_card_opposite_pic = idCardNegativePath;
                    registerParam.discharged_pic = retireImgPath;
                }
            }
        };
    }

    /**
     * 身份证照片正反面内容填充
     * 三个参数需正确传入，用来区分拍照/相册中选择，正面/反面view
     *
     * @param requestCodeCamera  拍照时requestCode
     * @param requestCodeGallery 从相册中选择照片 requestCode
     * @param fileName           文件名称
     */
    private void getIdCardImg(final int requestCodeCamera, final int requestCodeGallery, final String fileName) {
        View head_view = LayoutInflater.from(this).inflate(com.ilingtong.library.tongle.R.layout.headview_dialog_layout, null);
        TextView takephoto = (TextView) head_view.findViewById(com.ilingtong.library.tongle.R.id.takePhoto);
        TextView myPicture = (TextView) head_view.findViewById(com.ilingtong.library.tongle.R.id.myPicture);
        TextView dialog_cancle = (TextView) head_view.findViewById(com.ilingtong.library.tongle.R.id.dialog_cancle);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(head_view);
        alert = builder.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
        //拍照
        takephoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(JARegisterOfficerInfoActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                    @Override
                    public void requestSuccess() {
                        camera(requestCodeCamera, fileName);
                    }

                    @Override
                    public void requestFail() {
                        SelectDialog dialog = new SelectDialog(JARegisterOfficerInfoActivity.this, getString(R.string.please_open_camera_storage_permission));
                        dialog.show();
                    }
                });
                requestPermissionUtils.checkPermissions(JARegisterOfficerInfoActivity.this);
            }
        });
        //我的相册
        myPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(JARegisterOfficerInfoActivity.this, needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                    @Override
                    public void requestSuccess() {
                        gallery(requestCodeGallery);
                    }
                    @Override
                    public void requestFail() {
                        SelectDialog dialog = new SelectDialog(JARegisterOfficerInfoActivity.this, getString(R.string.please_open_camera_storage_permission));
                        dialog.show();
                    }
                });
                requestPermissionUtils.checkPermissions(JARegisterOfficerInfoActivity.this);
            }
        });
        //取消
        dialog_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }

    /*
    * 从相册获取
    */
    public void gallery(int requestCode) {
        // 激活系统图库，选择一张图片
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, requestCode);
    }

    /*
    * 拍照
    */
    public void camera(int requestCode, String imgFileName) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        // 判断存储卡是否可以用，可用进行存储
        if (hasSdcard()) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), imgFileName)));
        }
        startActivityForResult(intent, requestCode);
    }

    private boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void finish() {
        Intent intent = new Intent();
        registerParam.veterans_card_number = jaRetireEdtNumber.getText().toString();
        intent.putExtra("registerParam", registerParam);
        setResult(RESULT_OK, intent);
        super.finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}
