package com.ilingtong.junan.chouke.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.junan.chouke.CKServiceManager;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.adapter.MyOrgListAdapter;
import com.ilingtong.junan.chouke.entity.CKOrganizationInfoResult;
import com.ilingtong.junan.chouke.entity.CKOrganizationList;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.*;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/20
 * Time: 13:45
 * Email: liuting@ilingtong.com
 * Desc:我的关注组织列表
 */
public class MyOrganizationActivity extends BaseActivity implements View.OnClickListener, XListView.IXListViewListener {
    private XListView mXLvOrg;//组织列表
    private List<CKOrganizationList> mOrgList; //组织列表
    private CKOrganizationInfoResult mCKOrganizationInfoResult;//我的组织列表返回对象

    private ImageView mImgBack;//返回按钮
    private TextView mTxtTitle;//标题
    private ImageView mImgAdd;//添加按钮

    private RelativeLayout mRlyReplace;//无相关信息主布局
    private TextView mTxtReplace;//无相关信息提示

    private MyOrgListAdapter mMyOrgListAdapter;
    private Dialog mDialog;//加载对话框

    private boolean clearFlag = true;
    public static final int REFRESH_LIST = 0;  //刷新列表
    public static final int NO_DATA_REFRESH = 1;  //无数据时
    private boolean loadAllFlag = false;  //为true时表示数据已全部加载完毕

    private final int ORG_DETAIL_REQUEST_CODE = 10002;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_LIST:
                    mRlyReplace.setVisibility(View.GONE);
                    mXLvOrg.setRefreshTime();
                    mMyOrgListAdapter.notifyDataSetChanged();
                    break;
                case NO_DATA_REFRESH:
                    mXLvOrg.setVisibility(View.GONE);
                    mRlyReplace.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    public static void luanch(Activity activity){
        Intent intent = new Intent(activity,MyOrganizationActivity.class);
        activity.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ck_common_xlistview_layout);
        initView();
    }

    /**
     * 初始化界面控件
     */
    private void initView() {
        mXLvOrg = (XListView) findViewById(R.id.xlistview_common_lv_list);
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgAdd = (ImageView) findViewById(R.id.QR_share_btn);
        mImgBack.setVisibility(View.VISIBLE);
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgAdd.setImageResource(R.mipmap.ck_ic_org_add);
        mTxtTitle.setText(R.string.my_org);
        mImgBack.setOnClickListener(this);
        mImgAdd.setOnClickListener(this);

        mRlyReplace = (RelativeLayout) findViewById(R.id.xlistview_common_rly_replace);
        mTxtReplace = (TextView) findViewById(R.id.tv_replace);

        mDialog = DialogUtils.createLoadingDialog(MyOrganizationActivity.this);
        mDialog.setCancelable(true);

        mOrgList = new ArrayList<>();
        mOrgList.clear();
        mXLvOrg.setPullLoadEnable(false);
        mXLvOrg.setPullRefreshEnable(true);
        mXLvOrg.setXListViewListener(this, 0);
        mMyOrgListAdapter = new MyOrgListAdapter(MyOrganizationActivity.this, mOrgList);
        mXLvOrg.setAdapter(mMyOrgListAdapter);
        mXLvOrg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int itemPosition = position - 1;
                OrganizationDetailActivity.launcher(MyOrganizationActivity.this, mOrgList.get(itemPosition).getOrg_id(), ORG_DETAIL_REQUEST_CODE);
            }
        });
        getData("");
    }

    /**
     * 获取数据
     *
     * @param org_id 组织ID
     */
    public void getData(String org_id) {
        mDialog.show();
//        mCKOrganizationInfoResult = new CKOrganizationInfoResult();
//        mCKOrganizationInfoResult = (CKOrganizationInfoResult) TestInterface.parseJson(CKOrganizationInfoResult.class, "7002.txt");
        CKServiceManager.getMyOrganizationList(org_id, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, getSuccessListener(), errorListener());

    }

    /**
     * 接口请求成功
     *
     * @return
     */
    private Response.Listener<CKOrganizationInfoResult> getSuccessListener() {
        return new Response.Listener<CKOrganizationInfoResult>() {
            @Override
            public void onResponse(CKOrganizationInfoResult ckOrganizationInfoResult) {
                mCKOrganizationInfoResult = ckOrganizationInfoResult;
                if (mCKOrganizationInfoResult.getHead().getReturn_flag().equals(TongleAppConst.YES)) {
                    //获取数据成功
                    mImgAdd.setVisibility(View.VISIBLE);
                    if (mCKOrganizationInfoResult.getBody().getData_total_count() > 0) {
                        if (clearFlag) {
                            mOrgList.clear();
                        }
                        mOrgList.addAll(mCKOrganizationInfoResult.getBody().getOrganization_list());
                        loadAllFlag = (mOrgList.size() < mCKOrganizationInfoResult.getBody().getData_total_count()) ? false : true;
                        mXLvOrg.setPullLoadEnable(!loadAllFlag);
                        handler.sendEmptyMessage(REFRESH_LIST);
                    } else {
                        handler.sendEmptyMessage(NO_DATA_REFRESH);
                    }
                } else {//获取数据失败
                    mImgAdd.setVisibility(View.GONE);
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + mCKOrganizationInfoResult.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    /**
     * 接口请求失败
     *
     * @return
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
                mDialog.dismiss();
            }
        };
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.left_arrow_btn) {//返回
            finish();

        } else if (i == R.id.QR_share_btn) {//添加，进入发现
            Intent intent = new Intent(MyOrganizationActivity.this, com.ilingtong.library.tongle.activity.MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_NAME, TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_FIND);
            startActivity(intent);
        }
    }

    @Override
    public void onRefresh(int id) {
        clearFlag = true;
        getData("");
    }

    @Override
    public void onLoadMore(int id) {
        if (loadAllFlag) {
            ToastUtils.toastShort(com.ilingtong.library.tongle.R.string.common_list_end);
        } else {
            clearFlag = false;
            getData(mOrgList.get(mOrgList.size() - 1).getOrg_id());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ORG_DETAIL_REQUEST_CODE == requestCode) {
            if (resultCode == RESULT_OK) {
                //刷新数据
                onRefresh(0);
            }
        }
    }
}
