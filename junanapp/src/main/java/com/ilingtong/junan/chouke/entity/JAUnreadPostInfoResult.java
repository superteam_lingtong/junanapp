package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/9/21.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class JAUnreadPostInfoResult extends BaseResult implements Serializable{
    public JAUnreadPostInfo body;
}
