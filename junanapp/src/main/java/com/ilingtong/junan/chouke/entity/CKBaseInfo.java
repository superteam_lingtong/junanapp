package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 14:51
 * Email: liuting@ilingtong.com
 * Desc:use by 7001接口(获取我的基本信息) 用户基础信息
 */
public class CKBaseInfo {
    private String user_id;//用户ID
    private String user_name;//用户名称
    private String user_nick_name;//用户昵称
    private String user_phone;//手机号
    private String user_photo_url;//头像url
    private String user_sex;//性别
    private String user_id_no;//身份证号

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_nick_name() {
        return user_nick_name;
    }

    public void setUser_nick_name(String user_nick_name) {
        this.user_nick_name = user_nick_name;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_photo_url() {
        return user_photo_url;
    }

    public void setUser_photo_url(String user_photo_url) {
        this.user_photo_url = user_photo_url;
    }

    public String getUser_sex() {
        return user_sex;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public String getUser_id_no() {
        return user_id_no;
    }

    public void setUser_id_no(String user_id_no) {
        this.user_id_no = user_id_no;
    }
}
