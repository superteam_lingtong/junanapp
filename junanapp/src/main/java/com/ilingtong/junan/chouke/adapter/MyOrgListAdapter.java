package com.ilingtong.junan.chouke.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.entity.CKOrganizationList;
import com.ilingtong.junan.chouke.utils.ImageLoaderOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/20
 * Time: 14:40
 * Email: liuting@ilingtong.com
 * Desc:我关注的组织列表Adapter
 */
public class MyOrgListAdapter  extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<CKOrganizationList> mLvOrg;
    private Context mContext;

    public MyOrgListAdapter(Context context, List<CKOrganizationList> orgList) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvOrg = orgList;
    }

    @Override
    public int getCount() {
        return mLvOrg.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvOrg.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.ck_my_organization_list_item, null);
            holder = new ViewHolder();
            holder.mMyOrgImgIcon = (ImageView) view.findViewById(R.id.my_organization_img_icon);
            holder.mMyOrgTxtTime = (TextView) view.findViewById(R.id.my_organization_txt_time);
            holder.mMyOrgTxtName = (TextView) view.findViewById(R.id.my_organization_txt_name);
            holder.mMyOrgTxtPost = (TextView) view.findViewById(R.id.my_organization_txt_post);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

//      填充数据
        CKOrganizationList orgItem = (CKOrganizationList) getItem(position);
        ImageLoader.getInstance().displayImage(orgItem.getOrg_photo_url(), holder.mMyOrgImgIcon, ImageLoaderOptionsUtils.orgIconOptions());
        holder.mMyOrgTxtTime.setText(orgItem.getPost_update_time());
        holder.mMyOrgTxtName.setText(orgItem.getOrg_name());
        holder.mMyOrgTxtPost.setText(orgItem.getLatest_post_info());
        return view;
    }

    static class ViewHolder {
        ImageView mMyOrgImgIcon;//组织默认图片
        TextView mMyOrgTxtTime;//帖子最新更新时间
        TextView mMyOrgTxtName;//组织默认名称
        TextView mMyOrgTxtPost;//最近发帖内容
    }
}
