package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.FriendListItem;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 10:11
 * Email: liuting@ilingtong.com
 * Desc:7011接口(获取我的直粉列表)的body 我的直粉列表信息
 */
public class CKDirectFansInfo {
    private int data_total_count;//直粉总数量
    private List<FriendListItem> direct_fans_list;//直粉列表

    public int getData_total_count() {
        return data_total_count;
    }

    public void setData_total_count(int data_total_count) {
        this.data_total_count = data_total_count;
    }

    public List<FriendListItem> getDirect_fans_list() {
        return direct_fans_list;
    }

    public void setDirect_fans_list(List<FriendListItem> direct_fans_list) {
        this.direct_fans_list = direct_fans_list;
    }
}
