package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/4/13.
 * mail: wuqian@ilingtong.com
 * Description: 兵种列表
 */
public class ArmListResult extends BaseResult implements Serializable{
    public ArmListInfo body;//body类
}
