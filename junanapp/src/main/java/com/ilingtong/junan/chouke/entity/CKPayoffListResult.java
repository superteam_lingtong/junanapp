package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 16:35
 * Email: liuting@ilingtong.com
 * Desc:7006接口(获取我的酬客列表)返回json结构
 */
public class CKPayoffListResult extends BaseResult implements Serializable {
    private CKPayoffListInfo body;//我的酬客列表信息

    public CKPayoffListInfo getBody() {
        return body;
    }

    public void setBody(CKPayoffListInfo body) {
        this.body = body;
    }
}
