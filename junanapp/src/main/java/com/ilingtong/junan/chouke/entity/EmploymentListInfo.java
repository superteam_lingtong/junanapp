package com.ilingtong.junan.chouke.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2017/4/26.
 * mail: wuqian@ilingtong.com
 * Description:创业就业中心列表
 * use by 7017接口
 */
public class EmploymentListInfo implements Serializable {
    public List<EmploymentInfo> center_info;
}
