package com.ilingtong.junan.chouke.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.junan.chouke.CKConstant;
import com.ilingtong.junan.chouke.CKServiceManager;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.activity.EmploymentCenterListActivity;
import com.ilingtong.junan.chouke.activity.MyTaskActivity;
import com.ilingtong.junan.chouke.activity.OrgProdListActivity;
import com.ilingtong.junan.chouke.adapter.HomeNewsListAdapter;
import com.ilingtong.junan.chouke.entity.JAUnreadPostInfoResult;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.adapter.HomePagerAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.fragment.BaseFragment;
import com.ilingtong.library.tongle.model.HomeModel;
import com.ilingtong.library.tongle.protocol.BannerResult;
import com.ilingtong.library.tongle.protocol.ExpertUpdateRequestParam;
import com.ilingtong.library.tongle.protocol.ExpertUpdateResult;
import com.ilingtong.library.tongle.protocol.UserFollowPostList;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.ilingtong.library.tongle.utils.ImageOptionsUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * author: liuting
 * Date: 2016/7/13
 * Time: 17:25
 * Email: liuting@ilingtong.com
 * Desc:首页的fragment
 */
public class CKHomeFragment extends BaseFragment implements View.OnClickListener, ViewPager.OnPageChangeListener, XListView.IXListViewListener {
    private Dialog mDialog;//加载对话框
    private ViewPager mVpPic;//轮播
    private ImageView mImgDomestic;//国内商品
    private ImageView mImgAbroad;//国外商品
    private ImageView mImgExchange;//交换商城
    private ImageView mImgBetter;//优抚专区
    private HomePagerAdapter mPagerAdapter;
    private ArrayList<View> mPicList;//轮播图
    private ArrayList<Integer> mPicUrlList;//图片路径
    private View mTouchTarget;
    private ScheduledExecutorService scheduledExecutorService;//图片切换Service
    private int currentItem = 0;//当前页面
    private HomeModel homeModel;
    private LinearLayout mBanner;//导航栏
    private TextView mPreSelectedBt;//导航栏选择图标
    private View headView;
    private XListView mListView;
    private HomeNewsListAdapter homeNewsListAdapter;
    private List<UserFollowPostList> mNewsList = new ArrayList<>();
    public final static int UPDATE_FORUMlIST = 1;
    HomeFragmentHandler homeFragmentHandler = new HomeFragmentHandler(this);
    private boolean clearFlag = true;   //是否清空list标志。用于下拉刷新和加载更多时判断
    private TextView txtMyTask;  //我的任务数量
    private LinearLayout llMyTask;  //我的任务一栏

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ck_fragment_home_main_layout, null);
        initView(view);
        return view;
    }

    public void initView(View view) {
        mDialog = DialogUtils.createLoadingDialog(getActivity());
        mDialog.setCancelable(false);
        mDialog.show();

        headView = LayoutInflater.from(getActivity()).inflate(R.layout.ck_fragment_home_layout, null);
        if (homeModel == null)
            homeModel = new HomeModel();
        mVpPic = (ViewPager) headView.findViewById(R.id.home_vp_pic);
        //设置banner尺寸
        ViewGroup.LayoutParams params1 = mVpPic.getLayoutParams();
        params1.height = (DipUtils.getScreenWidth(getActivity()) * 5 / 13);
        mVpPic.setLayoutParams(params1);

        mImgDomestic = (ImageView) headView.findViewById(R.id.home_img_domestic);
        mImgAbroad = (ImageView) headView.findViewById(R.id.home_img_abroad);
        mImgExchange = (ImageView) headView.findViewById(R.id.home_img_exchange);
        mImgBetter = (ImageView) headView.findViewById(R.id.home_img_better);
        txtMyTask = (TextView) headView.findViewById(R.id.home_txt_my_task);
        llMyTask = (LinearLayout) headView.findViewById(R.id.home_ll_my_task);
        llMyTask.setOnClickListener(this);

        mImgDomestic.setOnClickListener(this);
        mImgAbroad.setOnClickListener(this);
        mImgBetter.setOnClickListener(this);
        mImgExchange.setOnClickListener(this);

        mPicList = new ArrayList<View>();
        mPagerAdapter = new HomePagerAdapter(getActivity(), mPicList, homeModel);
        mVpPic.setAdapter(mPagerAdapter);
        mVpPic.setCurrentItem(0);
        mVpPic.setOnPageChangeListener(this);

        mBanner = (LinearLayout) headView.findViewById(R.id.home_layout_banner);

        mListView = (XListView) view.findViewById(R.id.ck_home_main_lv);
        mListView.addHeaderView(headView);

        mListView.setPullRefreshEnable(true);
        mListView.setPullLoadEnable(false);
        mListView.setXListViewListener(this, 0);
        mListView.setRefreshTime();
        homeNewsListAdapter = new HomeNewsListAdapter(getActivity(), mNewsList);
        mListView.setAdapter(homeNewsListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent detailIntent = new Intent(getActivity(), CollectForumDetailActivity.class);
                detailIntent.putExtra("post_id", mNewsList.get(position -2).post_id);
                startActivity(detailIntent);
            }
        });

        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new ViewPagerTask(), 1, 3, TimeUnit.SECONDS);
        doBannerRequest();
        getNewsList("", true);
        getUnReadPost();
    }
    private void getNewsList(String postid, boolean flag) {
        //帖子请求
        // mNewsList.clear();
        clearFlag = flag;
        mListView.setPullLoadEnable(false); //刷新时设置加载更多功能为false。避免刷新时帖子列表头端出现“加载更多"

        ExpertUpdateRequestParam param = new ExpertUpdateRequestParam();
        param.user_id = TongleAppInstance.getInstance().getUserID();
        param.forward = "1";
        param.post_id = postid;
        param.expert_user_id = CKConstant.HEADUSER;
        param.fetch_count = TongleAppConst.FETCH_COUNT;
        param.action = "1";
        param.magazine_type = "0";
        ServiceManager.doExpertUpdateRequest(param, forumSuccessListener(), errorListener());
    }

    /**
     * 功能：请求帖子网络响应成功，返回数据
     */
    private Response.Listener<ExpertUpdateResult> forumSuccessListener() {
        return new Response.Listener<ExpertUpdateResult>() {
            @Override
            public void onResponse(ExpertUpdateResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    if (mNewsList != null && clearFlag) {
                        mNewsList.clear();
                    }
                    mNewsList.addAll(response.getBody().getUser_post_list());
                    if (mNewsList.size() < Integer.parseInt(response.getBody().getData_total_count())) {
                        mListView.setPullLoadEnable(true);
                    } else {
                        mListView.setPullLoadEnable(false);
                    }
                    homeFragmentHandler.sendEmptyMessage(UPDATE_FORUMlIST);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }
    /**
     * 获取我的任务数
     */
    private void getUnReadPost(){
        SharedPreferences spUnreadPost = TongleAppInstance.getAppContext().getSharedPreferences("spUnreadPost", Context.MODE_PRIVATE);
        String lastPostId = spUnreadPost.getString("lastPostId","");
        CKServiceManager.getUnreadPost(lastPostId, CKConstant.HEADUSER, new Response.Listener<JAUnreadPostInfoResult>() {
            @Override
            public void onResponse(JAUnreadPostInfoResult jaUnreadPostInfo) {
                if (TongleAppConst.SUCCESS.equals(jaUnreadPostInfo.getHead().getReturn_flag()))
                txtMyTask.setText(jaUnreadPostInfo.body.unread_post_count+"");
            }
        },null);
    }

    //轮播图请求
    public void doBannerRequest() {
        String limit_size = "";
        ServiceManager.doBannerRequest(TongleAppInstance.getInstance().getUserID(), limit_size, bannerSuccessListener(), errorListener());
    }

    /**
     * 功能：广告位列表网络响应成功，返回数据
     */
    private Response.Listener<BannerResult> bannerSuccessListener() {
        return new Response.Listener<BannerResult>() {
            @Override
            public void onResponse(BannerResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    homeModel.bannerList = response.getBody().getPromotionList();
                    addPicView();
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.sys_exception));
            }
        };
    }

    public void addPicView() {
        if ((homeModel == null) && (homeModel.bannerList == null))
            return;
        if (mPicList != null) {
            mPicList.clear();
        }
        try {
            for (int i = 0; i < homeModel.bannerList.size(); i++) {
                String url = homeModel.bannerList.get(i).mobile_pic_url;
                ImageView viewOne = (ImageView) LayoutInflater.from(getActivity()).inflate(com.ilingtong.library.tongle.R.layout.banner_cell_layout, null);
                ImageLoader.getInstance().displayImage(url, viewOne, ImageOptionsUtils.getOptions());
                mPicList.add(viewOne);
            }
            mBanner.removeAllViews();
            //定义viewpager下的圆点
            for (int i = 0; i < mPicList.size(); i++) {
                TextView bt = new TextView(getActivity());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(12, 12);
                params.setMargins(10, 0, 0, 0);
                bt.setLayoutParams(params);
                bt.setBackgroundResource(com.ilingtong.library.tongle.R.color.main_bgcolor);
                mBanner.addView(bt);
                mPagerAdapter.notifyDataSetChanged();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        mVpPic.setAdapter(mPagerAdapter);
    }

    //监听
    private int mPreviousState = ViewPager.SCROLL_STATE_IDLE;

    @Override
    public void onPageScrolled(int i, float v, int i2) {

    }

    @Override
    public void onPageSelected(int i) {
        if (mPreSelectedBt != null) {
            mPreSelectedBt.setBackgroundResource(com.ilingtong.library.tongle.R.color.main_bgcolor);
        }

        TextView currentBt = (TextView) mBanner.getChildAt(i);
        currentBt.setBackgroundResource(com.ilingtong.library.tongle.R.color.topview_bgcolor);
        mPreSelectedBt = currentBt;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (mPreviousState == ViewPager.SCROLL_STATE_IDLE) {
            if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                mTouchTarget = mVpPic;
            }
        } else {
            if (state == ViewPager.SCROLL_STATE_IDLE || state == ViewPager.SCROLL_STATE_SETTLING) {
                mTouchTarget = null;
            }
        }
        mPreviousState = state;
    }

    @Override
    public void onRefresh(int id) {
        getNewsList("", true);
        getUnReadPost();
    }

    @Override
    public void onLoadMore(int id) {
        getNewsList(mNewsList.get(mNewsList.size() - 1).post_id, false);
    }

    /**
     * 用来完成图片切换的任务
     */
    private class ViewPagerTask implements Runnable {
        public void run() {
            //实现我们的操作
            //改变当前页面
            currentItem = (currentItem + 1) % mPicList.size();
            //Handler来实现图片切换
            handler.obtainMessage().sendToTarget();
        }
    }

    private Handler handler = new Handler() {

        public void handleMessage(Message msg) {
            //设定viewPager当前页面
            mVpPic.setCurrentItem(currentItem);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.home_img_domestic://国内商品

                String domesticUrl = CKConstant.HOME_PRODUCT_HOST_URL + "wechatCateGoryList?goods_cate_id=" + CKConstant.HOME_DOMESTIC_PRODUCT_TYPE + "&sort_key=&sort_style=0&sort_kbn=0&forward=1&app_no=" + TongleAppInstance.getInstance().getApp_inner_no();
                Log.e("TAG", domesticUrl);
                OrgProdListActivity.launcher(getActivity(), domesticUrl, getString(R.string.home_domestic_txt_title));
                break;
            case R.id.home_img_abroad://国外商品
                String abroadUrl = CKConstant.HOME_PRODUCT_HOST_URL + "wechatCateGoryList?goods_cate_id=" + CKConstant.HOME_ABROAD_PRODUCT_TYPE + "&sort_key=&sort_style=0&sort_kbn=0&forward=1&app_no=" + TongleAppInstance.getInstance().getApp_inner_no();
                Log.e("TAG", abroadUrl);
                OrgProdListActivity.launcher(getActivity(), abroadUrl, getString(R.string.home_abroad_txt_title));
                break;
            case R.id.home_img_exchange://交换商城
                EmploymentCenterListActivity.launcher(CKHomeFragment.this);

                break;
            case R.id.home_img_better://优抚专区
                String betterUrl = CKConstant.HOME_PRODUCT_HOST_URL + "wechatCateGoryList?goods_cate_id=" + CKConstant.HOME_BETTER_TYPE + "&sort_key=&sort_style=0&sort_kbn=0&forward=1&app_no=" + TongleAppInstance.getInstance().getApp_inner_no();
                Log.e("TAG", betterUrl);
                OrgProdListActivity.launcher(getActivity(), betterUrl, getString(R.string.home_better_txt_title));
                break;
            case R.id.home_ll_my_task:  //去我的任务
                Intent postIntent = new Intent(getActivity(), MyTaskActivity.class);
                startActivityForResult(postIntent,2000);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2000){
            txtMyTask.setText(0+"");
        }
    }

    /***
     * 停止图片切换
     */
    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        //停止图片切换
        scheduledExecutorService.shutdown();
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new ViewPagerTask(), 1, 3, TimeUnit.SECONDS);
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    static class HomeFragmentHandler extends Handler {
        // 弱引用
        WeakReference<CKHomeFragment> mFragment;

        HomeFragmentHandler(CKHomeFragment fragment) {
            mFragment = new WeakReference<CKHomeFragment>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            CKHomeFragment theFragment = mFragment.get();
            switch (msg.what) {
                case UPDATE_FORUMlIST: //刷新帖子列表
                    theFragment.homeNewsListAdapter.notifyDataSetChanged();
                    break;
            }
        }
    }
}
