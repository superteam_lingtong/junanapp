package com.ilingtong.junan.chouke.entity;

/**
 * author: liuting
 * Date: 2016/5/18
 * Time: 10:30
 * Email: liuting@ilingtong.com
 * Desc:used by 7012接口(取得组织业绩组织酬金明细)，7013接口(取得我的个人业绩明细) 获利信息
 */
public class CKIncomeDetailInfo {
    private String income_id;//获利ID
    private double income_amount;//获利金额
    private String income_date;//获利时间
    private String income_content;//获利内容

    public String getIncome_id() {
        return income_id;
    }

    public void setIncome_id(String income_id) {
        this.income_id = income_id;
    }

    public double getIncome_amount() {
        return income_amount;
    }

    public void setIncome_amount(double income_amount) {
        this.income_amount = income_amount;
    }

    public String getIncome_date() {
        return income_date;
    }

    public void setIncome_date(String income_date) {
        this.income_date = income_date;
    }

    public String getIncome_content() {
        return income_content;
    }

    public void setIncome_content(String income_content) {
        this.income_content = income_content;
    }
}
