package com.ilingtong.junan.chouke.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.entity.EmploymentInfo;
import com.ilingtong.junan.chouke.utils.ImageLoaderOptionsUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * @Title: EmploymentCenterListAdapter
 * @Package: com.ilingtong.junan.chouke.adapter
 * @Description: 就业创业信息列表 Adapter
 * @author: liuting
 * @Date: 2017/4/26 15:47
 */

public class EmploymentCenterListAdapter extends BaseAdapter {
    private Context context;//Context
    private List<EmploymentInfo> list;//列表
    private IOnItemClickListener listener;//点击事件监听

    public interface IOnItemClickListener{
        void onItemClick(View view,String post_id);
    }
    public EmploymentCenterListAdapter(Context context, List<EmploymentInfo> list,IOnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        viewHolder holder;
        if (convertView == null) {
            holder = new viewHolder();

            convertView = LayoutInflater.from(context).inflate(R.layout.ja_list_item_employment_center_layout, null);
            holder.imgLogo = (ImageView) convertView.findViewById(R.id.employment_center_img_logo);
            holder.tvName = (TextView) convertView.findViewById(R.id.employment_center_tv_name);
            convertView.setTag(holder);
        }else {
            holder = (viewHolder) convertView.getTag();
        }

        ImageLoader.getInstance().displayImage(list.get(position).center_pic_url, holder.imgLogo, ImageLoaderOptionsUtils.employmentCenterLogoOptions());
        holder.tvName.setText(list.get(position).center_name);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null){
                    listener.onItemClick(view,list.get(position).post_id);
                }
            }
        });

        return convertView;
    }

    class viewHolder {
        ImageView imgLogo;//图标
        TextView tvName;//名称
    }
}

