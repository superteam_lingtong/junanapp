package com.ilingtong.junan.chouke.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.Intents;
import com.ilingtong.junan.chouke.CKConstant;
import com.ilingtong.junan.chouke.CKServiceManager;
import com.ilingtong.junan.chouke.ChouKeApplication;
import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.activity.MyChoukeFansFragmentActivity;
import com.ilingtong.junan.chouke.activity.MyOrganizationActivity;
import com.ilingtong.junan.chouke.activity.MyTaskActivity;
import com.ilingtong.junan.chouke.activity.OrganizationDetailActivity;
import com.ilingtong.junan.chouke.entity.CKOrgQrCodeResult;
import com.ilingtong.junan.chouke.entity.CKPersonalInfoResult;
import com.ilingtong.junan.chouke.entity.CKUserInfo;
import com.ilingtong.junan.chouke.utils.ImageLoaderOptionsUtils;
import com.ilingtong.junan.chouke.widget.CanMonitorScrollView;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectExpertDetailActivity;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.activity.CollectMStoreDetailActivity;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.activity.FindListenActivity;
import com.ilingtong.library.tongle.activity.ProdIntegralActivity;
import com.ilingtong.library.tongle.activity.ProductTicketDetailActivity;
import com.ilingtong.library.tongle.activity.SettingMyBoxAPPActivity;
import com.ilingtong.library.tongle.activity.ShowQRCodeActivity;
import com.ilingtong.library.tongle.adapter.MstoreDetaliGridAdapter;
import com.ilingtong.library.tongle.fragment.BaseFragment;
import com.ilingtong.library.tongle.protocol.ExpertBaseResult;
import com.ilingtong.library.tongle.protocol.ExpertProductResult;
import com.ilingtong.library.tongle.protocol.ProductListItemData;
import com.ilingtong.library.tongle.protocol.QRData;
import com.ilingtong.library.tongle.protocol.ScanDataResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.RequestPermissionUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.SelectDialog;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

import static android.app.Activity.RESULT_OK;

/**
 * Created by wuqian on 2016/5/27.
 * mail: wuqian@ilingtong.com
 * Description:酬客首页fragment，现作为酬板fragment
 */
public class CKCollectFragment extends BaseFragment implements View.OnClickListener, CanMonitorScrollView.onScrollListener {
    private CanMonitorScrollView scrollView;
    private TextView fragmentHomeTxtTodayIncome;  //今日收益
    private TextView txt_month_inccome;  //月收益
    private TextView txt_adll_income;   //总收益
    private ImageView fragmentHomeImgHeadPhoto;   //头像
    private TextView fragmentHomeTxtNickname;  //昵称
    private TextView fragmentHomeTxtOrgname;   //组织名称
    private ImageView fragmentHomeImgUserQr;  //会员二维码
    private RelativeLayout framentHomeRlChouke;  //点击去酬脉 -- 酬客
    private RelativeLayout fragmentHomeRlDirectFans;  //点击去酬脉 -- 直粉
    private RelativeLayout fragmentHomeRlTongleFans;  //点击去酬脉  -- 通粉
    private LinearLayout fragmenHomeLlScan;  //点击去扫一扫
    private LinearLayout fragmenHomeLlListen;  //点击去听一听
    private LinearLayout fragmenHomeLlAppQr;  //点击去APP推广二维码
    private LinearLayout fragmenHomeLlMyIncome;  //点击去我的收益
    private LinearLayout fragmenHomeLlMyPost;  //点击去我的帖子
    private LinearLayout fragmenHomeLlMyOrg;  //点击去我的组织
    private ImageView homeUserinfoLayoutFloatImgHeadPhoto;  //顶端悬浮 会员头像
    private TextView homeUserinfoLayoutFloatTxtNickname;  //订单悬浮 会员昵称
    private ImageView homeUserinfoLayoutFloatImgUserQr;  //订单悬浮  会员二维码
   // private LinearLayout fragment_home_ll_org;  //默认组织名称一块。。当前用户未加入组织时该view隐藏
    private TextView txt_my_chouke;  //我的酬客数量
    private TextView txt_my_direct_fans;  //我的直粉数量
    private TextView txt_my_tongle_fans;  //我的通粉数量
    private PtrClassicFrameLayout ptrClassicFrameLayout;  //下拉刷新view
    private boolean babyListClearFlag = true;
    private boolean isLoadingMore;  //是否加载更多

    private Dialog progressDialog;
    View shouyi, info, floatView;
    private CKUserInfo userInfo;   //个人信息
    final int SCAN_CODE_REQUEST = 10000;
    final int LISTEN_CODE_REQUEST = 10001;

    private GridView gridview_baby_product;  //我的宝贝列表
    private MstoreDetaliGridAdapter productListAdaper;
    public ArrayList<ProductListItemData> prod_list = new ArrayList<ProductListItemData>();

    private String[] needPermissions = {Manifest.permission.CAMERA};//相机权限，扫码所需
    private String[] listenPermissions = {Manifest.permission.RECORD_AUDIO};//麦克风权限，听一听所需
    private RequestPermissionUtils requestPermissionUtils;//请求权限类
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    productListAdaper.notifyDataSetChanged();
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ck_fragment_collect_layout, null);
        init(view);
        getUserInfo();
        getMyBaby("", true);
        return view;
    }

    /**
     * 初始化界面
     *
     * @param view
     */
    private void init(View view) {
        gridview_baby_product = (GridView) view.findViewById(R.id.fragmen_home_gv_my_baby);
        progressDialog = DialogUtils.createLoadingDialog(getActivity());
        progressDialog.setCancelable(false);
        shouyi = view.findViewById(R.id.layout_shouyi);
        info = view.findViewById(R.id.layout_info);
        floatView = view.findViewById(R.id.home_userinfo_layout_float);

        scrollView = (CanMonitorScrollView) view.findViewById(R.id.fragment_home_scrollView);
        fragmentHomeTxtTodayIncome = (TextView) view.findViewById(R.id.fragment_home_txt_today_income);
        fragmentHomeImgHeadPhoto = (ImageView) view.findViewById(R.id.fragment_home_img_head_photo);
        fragmentHomeTxtNickname = (TextView) view.findViewById(R.id.fragment_home_txt_nickname);
        fragmentHomeTxtOrgname = (TextView) view.findViewById(R.id.fragment_home_txt_orgname);
        fragmentHomeImgUserQr = (ImageView) view.findViewById(R.id.fragment_home_img_user_qr);
        framentHomeRlChouke = (RelativeLayout) view.findViewById(R.id.frament_home_rl_chouke);
        fragmentHomeRlDirectFans = (RelativeLayout) view.findViewById(R.id.fragment_home_rl_direct_fans);
        fragmentHomeRlTongleFans = (RelativeLayout) view.findViewById(R.id.fragment_home_rl_tongle_fans);
        fragmenHomeLlScan = (LinearLayout) view.findViewById(R.id.fragmen_home_ll_scan);
        fragmenHomeLlListen = (LinearLayout) view.findViewById(R.id.fragmen_home_ll_listen);
        fragmenHomeLlAppQr = (LinearLayout) view.findViewById(R.id.fragmen_home_ll_app_qr);
        fragmenHomeLlMyIncome = (LinearLayout) view.findViewById(R.id.fragmen_home_ll_my_income);
        fragmenHomeLlMyPost = (LinearLayout) view.findViewById(R.id.fragmen_home_ll_my_post);
        fragmenHomeLlMyOrg = (LinearLayout) view.findViewById(R.id.fragmen_home_ll_my_org);
        homeUserinfoLayoutFloatImgHeadPhoto = (ImageView) view.findViewById(R.id.home_userinfo_layout_float_img_head_photo);
        homeUserinfoLayoutFloatTxtNickname = (TextView) view.findViewById(R.id.home_userinfo_layout_float_txt_nickname);
        homeUserinfoLayoutFloatImgUserQr = (ImageView) view.findViewById(R.id.home_userinfo_layout_float_img_user_qr);
        txt_adll_income = (TextView) view.findViewById(R.id.fragment_home_txt_all_income);
        txt_month_inccome = (TextView) view.findViewById(R.id.fragment_home_txt_month_income);
       // fragment_home_ll_org = (LinearLayout) view.findViewById(R.id.fragment_home_ll_org_name);
        fragmentHomeImgHeadPhoto = (ImageView) view.findViewById(R.id.fragment_home_img_head_photo);
        txt_my_chouke = (TextView) view.findViewById(R.id.fragment_home_txt_my_chouke);
        txt_my_direct_fans = (TextView) view.findViewById(R.id.fragment_home_txt_my_direct_fans);
        txt_my_tongle_fans = (TextView) view.findViewById(R.id.fragment_home_txt_my_tongle_fans);

        ptrClassicFrameLayout = (PtrClassicFrameLayout) view.findViewById(R.id.fragment_home_ptrframelayout);
        ptrClassicFrameLayout.setLastUpdateTimeRelateObject(this);
        ptrClassicFrameLayout.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, scrollView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                getUserInfo();
                getMyBaby("", true);
            }
        });

        productListAdaper = new MstoreDetaliGridAdapter(getActivity(), prod_list);
        gridview_baby_product.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!TextUtils.isEmpty(prod_list.get(position).prod_id)) {
                    if (prod_list.get(position).coupon_flag.equals(TongleAppConst.YES)) {//团购商品
                        ProductTicketDetailActivity.launchForResult(getActivity(), prod_list.get(position).prod_id, TongleAppConst.ACTIONID_MY_BABY, prod_list.get(position).relation_id, "", "", TongleAppConst.MYBABY_INTO, 10001);
                    } else {
                        //普通商品
                        CollectProductDetailActivity.launchForResult(getActivity(), prod_list.get(position).prod_id, TongleAppConst.ACTIONID_MY_BABY, prod_list.get(position).relation_id, "", "", TongleAppConst.MYBABY_INTO, 10001);
                    }
                }
            }
        });
        gridview_baby_product.setAdapter(productListAdaper);

    }

    /**
     * 调用7001接口获取个人信息
     */
    private void getUserInfo() {
        progressDialog.show();
        CKServiceManager.getMyPersonalInfo(personalInfoSuccessListener(), errorListener);
    }

    /**
     * 个人信息接口成功回调
     *
     * @return
     */
    private Response.Listener<CKPersonalInfoResult> personalInfoSuccessListener() {
        return new Response.Listener<CKPersonalInfoResult>() {
            @Override
            public void onResponse(CKPersonalInfoResult ckPersonalInfoResult) {

                progressDialog.dismiss();
                ptrClassicFrameLayout.refreshComplete();
                if (TongleAppConst.SUCCESS.equals(ckPersonalInfoResult.getHead().getReturn_flag())) {
                    userInfo = ckPersonalInfoResult.getBody().getUser_info();
                    initUserInfo(ckPersonalInfoResult.getBody().getUser_info());
                } else {
                    ToastUtils.toastLong(R.string.para_exception + ckPersonalInfoResult.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 获取我的宝贝商品
     *
     * @param prod_id   翻页用
     * @param clearFlag 是否需要清空之前数据标志。刷新时应为true，加载更多时应未false
     */
    private void getMyBaby(String prod_id, boolean clearFlag) {
        babyListClearFlag = clearFlag;
        ServiceManager.doExpertProductRequest(TongleAppInstance.getInstance().getUserID(), prod_id, TongleAppConst.FORWORD_DONW, TongleAppConst.FETCH_COUNT, babyProductSuccessListener(), errorListener);
    }

    private Response.Listener<ExpertProductResult> babyProductSuccessListener() {
        return new Response.Listener<ExpertProductResult>() {
            @Override
            public void onResponse(ExpertProductResult expertProductResult) {
                if (TongleAppConst.SUCCESS.equals(expertProductResult.getHead().getReturn_flag())) {
                    if (babyListClearFlag) {
                        prod_list.clear();
                    }
                    prod_list.addAll(expertProductResult.getBody().getMy_prod_list());
                    isLoadingMore = Integer.parseInt(expertProductResult.getBody().getData_total_count()) > prod_list.size() ? true : false;
                    mHandler.sendEmptyMessage(0);
                } else {
                    ToastUtils.toastLong(R.string.para_exception + expertProductResult.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 接口调用失败回调
     */
    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            progressDialog.dismiss();
            ptrClassicFrameLayout.refreshComplete();
            isLoadingMore = true;
            ToastUtils.toastLong(R.string.sys_exception);
        }
    };

    /**
     * 7001接口 获取我的信息之后填充数据。
     *
     * @param userInfo
     */
    private void initUserInfo(CKUserInfo userInfo) {

        fragmentHomeTxtTodayIncome.setText(userInfo.getIncome_info().getToday_income() + "");
        txt_month_inccome.setText(userInfo.getIncome_info().getMonth_income() + "");
        txt_adll_income.setText(userInfo.getIncome_info().getTotal_income() + "");
        fragmentHomeTxtNickname.setText(userInfo.getBase_info().getUser_nick_name() + "");
        if (userInfo.getOrganization_list() == null || userInfo.getOrganization_list().size() < 1) {
            fragmentHomeTxtOrgname.setVisibility(View.INVISIBLE);
        } else {
            fragmentHomeTxtOrgname.setText(userInfo.getOrganization_list().get(0).getOrg_name() + "");
            ChouKeApplication.ckInstance.setMyDefaultOrg(userInfo.getOrganization_list().get(0).getOrg_id());
        }
        ImageLoader.getInstance().displayImage(userInfo.getBase_info().getUser_photo_url(), fragmentHomeImgHeadPhoto, ImageLoaderOptionsUtils.homeCircleHeadOption());
        ImageLoader.getInstance().displayImage(userInfo.getBase_info().getUser_photo_url(), homeUserinfoLayoutFloatImgHeadPhoto, ImageLoaderOptionsUtils.homeCircleHeadOption());
        homeUserinfoLayoutFloatTxtNickname.setText(userInfo.getBase_info().getUser_nick_name() + "");
        txt_my_chouke.setText(userInfo.getFans_info().getPayoff_count() + "");
        txt_my_direct_fans.setText(userInfo.getFans_info().getFans_direct_count() + "");
        txt_my_tongle_fans.setText(userInfo.getFans_info().getFans_tl_count() + "");

        fragmentHomeImgUserQr.setOnClickListener(this);
        framentHomeRlChouke.setOnClickListener(this);
        fragmentHomeRlDirectFans.setOnClickListener(this);
        fragmentHomeRlTongleFans.setOnClickListener(this);
        fragmenHomeLlScan.setOnClickListener(this);
        fragmenHomeLlListen.setOnClickListener(this);
        fragmenHomeLlAppQr.setOnClickListener(this);
        fragmenHomeLlMyIncome.setOnClickListener(this);
        fragmenHomeLlMyPost.setOnClickListener(this);
        fragmenHomeLlMyOrg.setOnClickListener(this);
        homeUserinfoLayoutFloatImgUserQr.setOnClickListener(this);
        scrollView.setVisibility(View.VISIBLE);
        scrollView.setScrollListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.frament_home_rl_chouke:  //我的酬脉 -- 酬客
                MyChoukeFansFragmentActivity.luanch(getActivity(), CKConstant.FRAGMENT_TYPE_CHOUKE);
                break;
            case R.id.fragment_home_rl_direct_fans:  //我的酬脉 -- 直粉
                MyChoukeFansFragmentActivity.luanch(getActivity(), CKConstant.FRAGMENT_TYPE_DERICT_FANS);
                break;
            case R.id.fragment_home_rl_tongle_fans:   //我的酬脉 -- 通粉
                MyChoukeFansFragmentActivity.luanch(getActivity(), CKConstant.FRAGMENT_TYPE_TONGLE_FANS);
                break;
            case R.id.fragmen_home_ll_scan:   //扫一扫
                requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(getActivity(), needPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                            @Override
                            public void requestSuccess() {
                                //跳转到扫码
                                Intent intent = new Intent(getActivity(), CaptureActivity.class);
                                intent.setAction(Intents.Scan.ACTION);
                                intent.putExtra(Intents.Scan.MODE, Intents.Scan.QR_CODE_MODE);
                                intent.putExtra(Intents.Scan.RESULT_DISPLAY_DURATION_MS, 0L);
                                intent.putExtra("TopName", "返回");
                                startActivityForResult(intent, SCAN_CODE_REQUEST);
                            }

                            @Override
                            public void requestFail() {
                                SelectDialog dialog = new SelectDialog(getActivity(), getString(com.ilingtong.library.tongle.R.string.please_open_camera_permission));
                                dialog.show();
                            }
                        });
                requestPermissionUtils.checkPermissions(CKCollectFragment.this);

                break;
            case R.id.fragmen_home_ll_listen:   // 听一听
                requestPermissionUtils = RequestPermissionUtils.getRequestPermissionUtils(getActivity(), listenPermissions, new RequestPermissionUtils.IRequestPermissionsListener() {
                            @Override
                            public void requestSuccess() {
                                //跳转到听一听
                                Intent intentListen = new Intent(getActivity(), FindListenActivity.class);
                                startActivityForResult(intentListen, LISTEN_CODE_REQUEST);
                            }

                            @Override
                            public void requestFail() {
                                SelectDialog dialog = new SelectDialog(getActivity(), getString(com.ilingtong.library.tongle.R.string.please_open_audio_permission));
                                dialog.show();
                            }
                        });
                requestPermissionUtils.checkPermissions(CKCollectFragment.this);

                break;
            case R.id.fragmen_home_ll_app_qr:   //app推广码
                Intent intentAPPQR = new Intent(getActivity(), SettingMyBoxAPPActivity.class);
                startActivity(intentAPPQR);
                break;
            case R.id.fragmen_home_ll_my_income:   //我的收益
                String myurl = userInfo.getIncome_info().getIncome_url() + "?user_id=" + TongleAppInstance.getInstance().getUserID() + "&user_token=" + TongleAppInstance.getInstance().getToken() + "&app_inner_no=" + TongleAppInstance.getInstance().getApp_inner_no();
                Intent expertIntent = new Intent(getActivity(), ProdIntegralActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("prod_url", myurl);
                bundle.putString("myTitle", getString(R.string.my_income));
                expertIntent.putExtras(bundle);
                startActivity(expertIntent);
                Log.e("TAG", myurl);
                break;
            case R.id.fragmen_home_ll_my_post:   //我的帖子
//                Intent postIntent = new Intent(getActivity(), CollectExpertDetailActivity.class);
//                Bundle postbundle = new Bundle();
//                postbundle.putString("user_id", "31600000213");
//                postbundle.putString("magazine_type","1");
//                postIntent.putExtras(postbundle);
                Intent postIntent = new Intent(getActivity(), MyTaskActivity.class);
                startActivity(postIntent);
                break;
            case R.id.fragmen_home_ll_my_org:   //我的组织
                MyOrganizationActivity.luanch(getActivity());
                break;
            case R.id.fragment_home_img_user_qr:   //个人二维码
                showQRCode();
                break;
            case R.id.home_userinfo_layout_float_img_user_qr:   //顶部个人二维码
                showQRCode();
                break;
        }
    }

    /**
     * 点击二维码图标响应事件。
     */
    private void showQRCode() {
        if (TextUtils.isEmpty(ChouKeApplication.ckInstance.getMyDefaultOrg())) {
            ToastUtils.toastShort(getString(R.string.ck_home_qr_empty_txt));
        } else {
            progressDialog.show();
            CKServiceManager.getOrgQRCode(ChouKeApplication.ckInstance.getMyDefaultOrg(), qrCodeSuccessListener(), errorListener);
        }
    }

    /**
     * 7007接口请求二维码 成功回调
     *
     * @return
     */
    private Response.Listener<CKOrgQrCodeResult> qrCodeSuccessListener() {
        return new Response.Listener<CKOrgQrCodeResult>() {
            @Override
            public void onResponse(CKOrgQrCodeResult ckOrgQrCodeResult) {
                progressDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(ckOrgQrCodeResult.getHead().getReturn_flag())) {
//                    ShowQRCodeDialog dialog = new ShowQRCodeDialog(getActivity(), ckOrgQrCodeResult.getBody().getOrg_qr_code_url(),ChouKeApplication.ckInstance.getMyDefaultOrg(),TongleAppConst.STRORGANIZATION);
//                    dialog.show();
                    ShowQRCodeActivity.launcher(CKCollectFragment.this,ckOrgQrCodeResult.getBody().getOrg_qr_code_url(),ChouKeApplication.ckInstance.getMyDefaultOrg(),TongleAppConst.STRORGANIZATION);
                } else {
                    ToastUtils.toastLong(getString(R.string.para_exception) + ckOrgQrCodeResult.getHead().getReturn_message());
                }
            }
        };
    }

    @Override
    public void onScroll(int left, int top, int oldLeft, int oldTop) {
        Log.e("TAG", "shouyi.getMeasureHeigh=" + shouyi.getMeasuredHeight() + ",info.height=" + info.getMeasuredHeight());
        Log.e("TAG", "mainActivity onScroll top=" + top);
        float alpha = (top - shouyi.getMeasuredHeight()) /
                ((info.getMeasuredHeight() - floatView.getMeasuredHeight()) * 1.0f);
        alpha = alpha > 1 ? 1 : alpha;
        floatView.setAlpha(alpha);
    }

    @Override
    public void isScrollTop() {

    }

    @Override
    public void isScrollBottom() {
        if (isLoadingMore) {
            isLoadingMore = false;
            getMyBaby(prod_list.get(prod_list.size() - 1).prod_id, false);
        }
    }

    /**
     * 处理带返回结果的activity方法
     *
     * @param requestCode 开发者定义的唯一请求码
     * @param resultCode  activity返回的结果码
     * @param data        activity返回的数据
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (SCAN_CODE_REQUEST == requestCode) {
            if (resultCode == RESULT_OK) {
                Bundle b = data.getExtras();
                String qrcode = b.getString("SCAN_RESULT");
                if (!TextUtils.isEmpty(qrcode)) {
                    progressDialog.show();
                    ServiceManager.doScanDataRequest(TongleAppInstance.getInstance().getUserID(), qrcode, successListener(), errorListener);
                }
            }
        } else if (LISTEN_CODE_REQUEST == requestCode) { //听一听返回
            if (resultCode == RESULT_OK) {
                Bundle b = data.getExtras();
                String qrcode = b.getString("LISTEN_RESULT");
                if (!TextUtils.isEmpty(qrcode)) {
                    ServiceManager.doListennDataRequest(TongleAppInstance.getInstance().getUserID(), qrcode, successListener(), errorListener);
                }
            }
        }
    }

    /**
     * 功能：扫码解析成功回调
     */
    private Response.Listener<ScanDataResult> successListener() {
        return new Response.Listener<ScanDataResult>() {
            @Override
            public void onResponse(ScanDataResult response) {
                progressDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    QRData qrData = response.getBody().getQRData();
                    codeJump(qrData);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
            }


        };
    }

    /**
     * 二维码跳转后根据类别跳转
     */
    private void codeJump(QRData qrData) {
        if (!TextUtils.isEmpty(qrData.code_type)) {
            //达人详情
            if (TongleAppConst.STREXPERT.equals(qrData.code_type)) {
                ServiceManager.doExpertBaseRequest(qrData.user_id, ExpertBaseListener(), errorListener);
                //帖子详情
            } else if (TongleAppConst.STRFORUM.equals(qrData.code_type)) {
                Intent forumIntent = new Intent(getActivity(), CollectForumDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("post_id", qrData.post_id);
                forumIntent.putExtras(bundle);
                startActivity(forumIntent);
                //魔店详情
            } else if (TongleAppConst.STRSTORE.equals(qrData.code_type)) {
                Intent intent = new Intent(getActivity(), CollectMStoreDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("mstore_id", qrData.store_id);
                intent.putExtras(bundle);
                startActivity(intent);
                //产品详情
            } else if (TongleAppConst.STRPRODUCT.equals(qrData.code_type)) {
                CollectProductDetailActivity.launch(getActivity(), qrData.prod_id, TongleAppConst.ACTIONID_SCAN_CODE, qrData.relation_id, "", "");
                //团购商品详情
            } else if (TongleAppConst.STRGROUP_PRODUCT.equals(qrData.code_type)) {
                ProductTicketDetailActivity.launch(getActivity(), qrData.prod_id, TongleAppConst.ACTIONID_SCAN_CODE, qrData.relation_id, "", "");
            } else if (CKConstant.QRCODE_TYPE_ORG.equals(qrData.code_type)) {
                //去组织详情
                OrganizationDetailActivity.launcher(this, qrData.org_id);
            }
        }
    }

    /**
     * 功能：会员详情，返回数据
     */
    private Response.Listener<ExpertBaseResult> ExpertBaseListener() {
        return new Response.Listener<ExpertBaseResult>() {
            @Override
            public void onResponse(ExpertBaseResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    Intent expertIntent = new Intent(getActivity(), CollectExpertDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("user_id", response.getBody().user_id);
                    bundle.putString("user_favorited_by_me", response.getBody().user_favorited_by_me);
                    expertIntent.putExtras(bundle);
                    startActivity(expertIntent);
                } else {
                    ToastUtils.toastLong(getString(com.ilingtong.library.tongle.R.string.para_exception) + response.getHead().getReturn_message());
                }
            }


        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        requestPermissionUtils.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
    }
}
