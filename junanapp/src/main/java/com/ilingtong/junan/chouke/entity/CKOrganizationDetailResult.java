package com.ilingtong.junan.chouke.entity;

import com.ilingtong.library.tongle.protocol.BaseResult;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/5/17
 * Time: 15:33
 * Email: liuting@ilingtong.com
 * Desc:7003接口(获取指定组织的详情)返回json结构
 */
public class CKOrganizationDetailResult extends BaseResult implements Serializable {
    private CKOrganizationDetail body;//指定组织的详情

    public CKOrganizationDetail getBody() {
        return body;
    }

    public void setBody(CKOrganizationDetail body) {
        this.body = body;
    }
}
