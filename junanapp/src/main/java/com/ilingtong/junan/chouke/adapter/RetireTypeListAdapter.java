package com.ilingtong.junan.chouke.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.junan.chouke.R;
import com.ilingtong.junan.chouke.entity.ArmInfo;
import com.ilingtong.junan.chouke.entity.OfficerInfo;

import java.util.List;

/**
 * Package:com.ilingtong.junan.chouke.adapter
 * author:liuting
 * Date:2017/4/13
 * Desc:军种类型列表Adapter
 */

public class RetireTypeListAdapter extends BaseAdapter {
    private Context context;//Context
    private IOnItemClickListener listener;//事件监听
    private List<OfficerInfo> listOfficer;//类型列表
    private List<ArmInfo> listArm;//兵种列表

    public interface IOnItemClickListener{
        void onItemClick(View view,int position);
    }

    public RetireTypeListAdapter(Context context, IOnItemClickListener listener,List<ArmInfo> listArm,List<OfficerInfo> listOfficer) {
        this.context = context;
        this.listener = listener;
        this.listArm = listArm;
        this.listOfficer = listOfficer;
    }

    @Override
    public int getCount() {
        if(listArm!=null&&listArm.size()>0){
            return listArm.size();
        }else if(listOfficer!=null&&listOfficer.size()>0){
            return listOfficer.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(listArm!=null&&listArm.size()>0){
            return listArm.get(position);
        }else if(listOfficer!=null&&listOfficer.size()>0){
            return listOfficer.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
       final viewHolder holder;
        if (convertView == null) {
            holder = new viewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.ja_list_item_retire_type_layout, null);
            holder.tvName = (TextView) convertView.findViewById(R.id.retire_type_tv_name);
            holder.imgCheck = (ImageView) convertView.findViewById(R.id.retire_type_img_check);
            convertView.setTag(holder);
        }else {
            holder = (viewHolder) convertView.getTag();
        }

        if(listArm!=null&&listArm.size()>0){
            holder.tvName.setText(listArm.get(position).arm);
        }else if(listOfficer!=null&&listOfficer.size()>0){
            holder.tvName.setText(listOfficer.get(position).applicant_type);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null){
                    listener.onItemClick(view,position);
                }
            }
        });
        return convertView;
    }

    class viewHolder {
        TextView tvName;//类型名称
        ImageView imgCheck;//选择图标
    }
}
