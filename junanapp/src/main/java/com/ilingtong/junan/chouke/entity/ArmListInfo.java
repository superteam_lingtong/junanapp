package com.ilingtong.junan.chouke.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Package:com.ilingtong.junan.chouke.entity
 * author:liuting
 * Date:2017/4/13
 * Desc:兵种列表类
 */

public class ArmListInfo implements Serializable{
    public List<ArmInfo> arm_list;
}
